/*
	Controller for button on programs list view "Assign to Scholarship"
	to mass assign a Scholarship to several programs at a time

	Author: Flor Montanari
	Date: July 1, 2018
*/

public class ProgramOfferToScholarshipsController {

	public ApexPages.StandardSetController cont {get; private set;}
	public Sch_in_Program__c dummyScholToProgramOffer {get; set;}


	public ProgramOfferToScholarshipsController(ApexPages.StandardSetController c){
		cont = c;
		dummyScholToProgramOffer = new Sch_in_Program__c();
	}


	public boolean validScholarshipSelected(){
		if (null != dummyScholToProgramOffer.Scholarship__c)
			return true;
		return false;
	}


	public PageReference massAssignScholarships(){
		if (!validScholarshipSelected()){
			addMessage('Please select a valid Scholarship','ERROR');
			return null;
		}

		if (cont.getSelected().isEmpty()){
			string m = 'Please select at least one record on the list view.';
			addMessage(m,'ERROR');
			return null;
		}
		List<Sch_in_Program__c> newScholToProgramOffers = new List<Sch_in_Program__c>();

		Id selectedScholarship = dummyScholToProgramOffer.Scholarship__c;
		for (sObject rSel: cont.getSelected()){
			Sch_in_Program__c schToProg = new Sch_in_Program__c(
				Scholarship__c = selectedScholarship
			);
			schToProg.Program_Offer__c = (Id) rSel.get('id');
			newScholToProgramOffers.add(schToProg);
		}

		insertJunctionRecords(newScholToProgramOffers);
		return null;
	}


	private void insertJunctionRecords(List<Sch_in_Program__c> l){
		l = removeExistentOnes(l);
		if (!l.isEmpty()){
			try{
				insert l;
			}catch(Exception e){
				system.debug('+-+ error creating records: '+e.getMessage());
				addMessage(e.getMessage(), 'ERROR');
				return;
			}
		}
		string m = 'Scholarships linked succesfully. <a href="/a0n"> Go Back To List </a>';
		addMessage(m, 'INFO');
		//dummyScholToProgramOffer.Scholarship__c = null;
	}


	public PageReference goBack(){
		PageReference pr = new PageReference('/a0n');
		pr.setRedirect(true);
		return pr;
	}


	private List<Sch_in_Program__c> removeExistentOnes(List<Sch_in_Program__c> l){
		if (l.isEmpty())
			return l;
		Id selectedScholarship = l[0].Scholarship__c;
		Map<Id, Sch_in_Program__c> newRecByProgramId = new Map<Id, Sch_in_Program__c>();
		for (Sch_in_Program__c schInProg: l){
			newRecByProgramId.put(schInProg.Program_Offer__c, schInProg);
		}

		// get existing programs already related to that scholarship
		List<Sch_in_Program__c> exisSchProgram = new List<Sch_in_Program__c>();
		try{
			exisSchProgram = [
				SELECT Program_Offer__c
				FROM Sch_in_Program__c
				WHERE Scholarship__c = :selectedScholarship
			];
		}catch(Exception e){
			system.debug('+-+ error querying scholarship to program records: '+e.getMessage());
			return l;
		}


		// if the program is already linked to that scholarship
		// remove it from the list
		for (Sch_in_Program__c existing: exisSchProgram){
			if (newRecByProgramId.containsKey(existing.Program_Offer__c))
				newRecByProgramId.remove(existing.Program_Offer__c);
		}

		// return a list of programs not yet related to that scholarship
		return newRecByProgramId.values();
	}


	public void addMessage(string msg, string severity){
		ApexPages.getMessages().clear();
		ApexPages.Message myMsg;
		if (severity == 'INFO')
		 	myMsg = new ApexPages.Message(ApexPages.Severity.INFO, msg);
		else
			myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
		ApexPages.addMessage(myMsg);
	}
}