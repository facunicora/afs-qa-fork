/**
 * Description : Helper class of Content Document Link Trigger
 * 				 Business Logic to share files to all user for 
 * 				 displaying images in community
 */
public without sharing class Afs_ContentDocumentLinkTriggerHelper {
    
    public static void beforeInsert(list<ContentDocumentLink> newContentDocumentList){
		String applicationPrefix = Application__c.sobjecttype.getDescribe().getKeyPrefix();
        String contactPrefix = Contact.sobjecttype.getDescribe().getKeyPrefix();
        String programOfferedPrefix = Program_Offer__c.sobjecttype.getDescribe().getKeyPrefix();
        for (ContentDocumentLink so : newContentDocumentList) {
            if(String.valueOf(so.LinkedEntityId).startsWith(applicationPrefix) || 
               String.valueOf(so.LinkedEntityId).startsWith(contactPrefix) || 
               String.valueOf(so.LinkedEntityId).startsWith(programOfferedPrefix)){
                so.ShareType = 'I';
            	so.Visibility = 'AllUsers';
            }
            
        }
    }
}