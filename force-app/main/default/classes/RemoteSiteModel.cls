public with sharing class RemoteSiteModel {
    public class Response{
    	public String itemsTotal {get; set;}
    	public String pageIndex {get; set;}
    	public String pageSize {get; set;}
    	public String pagesTotal {get; set;}
    	public String statusCode {get; set;}
    }
    
    public class Matrix{
    	public String send_ioc {get; set;}
    	public String program_code {get; set;}
    	public String host_ioc {get; set;}
    	public String host_planned_number {get; set;}
    	public String send_projected_actual {get; set;}
    }
    
    public class HostingFactSheets{
    	public String host_ioc {get; set;}
    	public String program_code {get; set;}
    	public String program_title {get; set;}
    	public String program_duration {get; set;}
    	public String program_cycle {get; set;}
    	public String program_content {get; set;}
    	public String program_type {get; set;}
    	public String program_description {get; set;}
    	public String program_language {get; set;}
    	public String program_year {get; set;}
    	public String age_range_year_start {get; set;}
    	public String age_range_year_end {get; set;}
    	public String age_range_month_start {get; set;}
    	public String age_range_month_end {get; set;}
    	public String app_received_start {get; set;}
    	public String app_received_end {get; set;}
    	public String from_departure_date {get; set;}
    	public String to_departure_date {get; set;}
    	public String from_arrival_date {get; set;}
    	public String to_arrival_date {get; set;}
    	public String from_arrival_date2 {get; set;}
    	public String to_arrival_date2 {get; set;}
    	public String from_arrival_date3 {get; set;}
    	public String from_departure_date3 {get; set;}
		public String graduate_accept {get; set;}
    }

    public class MTPDates{
        public String program_code{get; set;}
        public String host_ioc {get; set;}
        public String send_ioc {get; set;}
        public String from_arrival_date {get; set;}
        public String from_arrival_date2 {get; set;}
        public String from_arrival_date3 {get; set;}
        public String to_departure_date {get; set;}
    }

    public class Organization{
        public String sf_org_id{get; set;}
        public String id{get; set;}
        public String owner_ioc{get; set;}
        public String ioc_code{get; set;}
        public String organization_name{get; set;}
        public String native_name{get; set;}
        public String organization_ref{get; set;}
        public String org_type{get; set;}
        public String org_sub_type{get; set;}
        public String org_status{get; set;}
        public String chapter_id{get; set;}
        public String area_team_id{get; set;}
        public String region_id{get; set;}
        public String web_site{get; set;}
        public String english_address1{get; set;}
        public String english_address2{get; set;}
        public String english_city{get; set;}
        public String english_state{get; set;}
        public String english_zip{get; set;}
        public String native_address1{get; set;}
        public String native_address2{get; set;}
        public String native_city{get; set;}
        public String native_state{get; set;}
        public String native_zip{get; set;}
        public String country{get; set;}
        public String telnum1{get; set;}
        public String faxnum{get; set;}
    }    
    
    public class ServiceAndOA{
        public String id{get; set;}
        public String sf_service_id{get; set;}
        public String person_id{get; set;}
        public String service_ref{get; set;}
        public String ioc_code{get; set;}
        public String owner_ioc{get; set;}
        public String selected_partner_ioc_code{get; set;}
        public String host_country{get; set;}
        public String stage{get; set;}
        public String status{get; set;}
        public String local_program_id{get; set;}
        public String best_time_to_contact{get; set;}
        public String selected_program_code{get; set;}
        public String form_status{get; set;}
        public String preapp_fullapp_ind{get; set;}
        public String assigned_org_id{get; set;}
        public String interview_date{get; set;}
        public String program_name{get; set;}
        public String program_name_native{get; set;}
        public String program_type{get; set;}
        public String program_start_date{get; set;}
        public String school{get; set;}
        public String school_id{get; set;}
        public String other_school_city{get; set;}
        public String other_school_state{get; set;}
        public String other_school_zip{get; set;}
        public String graduation_year{get; set;}
        public String language_desc_memo{get; set;}
        public String fam_participated_on_afs_prg{get; set;}
        public String fam_hosted_on_afs{get; set;}
        public String living_abroad{get; set;}
        public String graduate_year{get; set;}
        public String self_description{get; set;}
        public String cs_skill{get; set;}
        public String medication_description{get; set;}
        public String program_expectation{get; set;}
        public String program_content{get; set;}
        public String program_duration{get; set;}
        public String preferred_year{get; set;}
        public String countries_preference1{get; set;}
        public String countries_preference2{get; set;}
        public String countries_preference3{get; set;}
        public String preferred_country{get; set;}
        public String medical_review_status{get; set;}
        public String dietary_restrictions{get; set;}
        public String dietary_restrictions_ind{get; set;}
        public String smoker_ind{get; set;}
        public String practice_religion_ind{get; set;}
        public String physical_restrictions_ind{get; set;}
        public String physical_restrictions{get; set;}
        public String emergency_contact{get; set;}
        public String fee_pay_type{get; set;}
        public String fee_pay_code{get; set;}
        public String fee_admission{get; set;}
        public String interview_result_ind{get; set;}
        public String parent_legal_guardian_other{get; set;}
        public String what_school_do_you_attend{get; set;}
        public String no_restrictions_or_allergies{get; set;}
        public String limited_in_the_activities_i_can_do{get; set;}
        public String have_allergies{get; set;}
        public String take_medications{get; set;}
        public String can_t_live_with_a_smoker{get; set;}
        public String have_food_allergies{get; set;}
        public String vegetarian{get; set;}
        public String vegan{get; set;}
        public String kosher{get; set;}
        public String halal{get; set;}
        public String celiac{get; set;}
        public String dietary_comments{get; set;}
        public String willing_to_change_diet_during_program{get; set;}
        public String treated_for_any_health_issues_physical{get; set;}
        public String health_issue_description{get; set;}
        public String what_are_you_looking_for{get; set;}
        public String what_do_you_want_out_of_this_experience{get; set;}
        public String how_would_friends_family_describe_you{get; set;}
        public String tell_us_about_yourself{get; set;}
        public String program_you_participated{get; set;}
        public String can_t_live_with_pets_s{get; set;}
        public String this_is_your_emergency_contact{get; set;}
        public String video_for_host_family{get; set;}
        public String pets{get; set;}
        public String dual_citizenship{get; set;}
        public String second_citizenship{get; set;}
        public String home_mother{get; set;}
        public String home_father{get; set;}
        public String home_stepmother{get; set;}
        public String home_stepfather{get; set;}
        public String home_sister_stepsister{get; set;}
        public String home_brother_stepbrother{get; set;}
        public String home_sister_stepsisters_age{get; set;}
        public String home_brother_stepbrothers_age{get; set;}
        public String home_grandmother{get; set;}
        public String home_grandfather{get; set;}
        public String home_other{get; set;}
        public String home_other_relationship{get; set;}
        public String instagram{get; set;}
        public String linkedin{get; set;}
        public String facebook{get; set;}
        public String twitter{get; set;}
        public String snapchat{get; set;}
        public String youtube{get; set;}
        public String are_you_attending_university_college{get; set;}
        public String describe_yourself_as_a_student{get; set;}
        public String name_of_university_college_school{get; set;}
        public String criminal_convictions{get; set;}
        public String open_to_same_sex_host_parents{get; set;}
        public String open_to_single_host_parent{get; set;}
        public String open_to_sharing_a_host_family{get; set;}
        public String lgbtq_member{get; set;}
        public String tshirt_info{get; set;}
        public String bv_record_locator{get; set;}
        public String bv_airline{get; set;}
        public String bv_flight_number{get; set;}
        public String bv_departure_time{get; set;}
        public String bv_arrival_time{get; set;}
        public String bv_departure_airport{get; set;}
        public String bv_arrival_airport{get; set;}
        public String bv_domestic_travel_arrangements{get; set;}
        public String bv_arrival_to_gateway_city_with_who{get; set;}
        public String bv_gateway_city{get; set;}
        public String bv_arrive_in_prior_to_the_start{get; set;}
        public String bv_additional_comments{get; set;}
        public String bv_contact_first_name{get; set;}
        public String bv_contact_last_name{get; set;}
        public String bv_contact_phone{get; set;}
        public String bv_contact_phone_type{get; set;}
        public String bv_contact_relationship{get; set;}
        public String cbh_record_locator{get; set;}
        public String cbh_airline{get; set;}
        public String cbh_flight_number{get; set;}
        public String cbh_departure_time{get; set;}
        public String cbh_arrival_time{get; set;}
        public String cbh_departure_airport{get; set;}
        public String cbh_arrival_airport{get; set;}
        public String cbh_domestic_travel_arrangements{get; set;}
        public String cbh_arrival_to_gateway_city_with_who{get; set;}
        public String cbh_gateway_city{get; set;}
        public String cbh_arrive_in_prior_to_the_start{get; set;}
        public String cbh_additional_comments{get; set;}
        public String cbh_contact_first_name{get; set;}
        public String cbh_contact_last_name{get; set;}
        public String cbh_contact_phone{get; set;}
        public String cbh_contact_phone_type{get; set;}
        public String cbh_contact_relationship{get; set;}
        public String things_to_share{get; set;}
        public String describe_a_normal_day_in_your_life{get; set;}
        public String what_you_bring_to_your_afs_experience{get; set;}
        public String food_thoughts{get; set;}
        public String favorite_travel_destinations_books{get; set;}
        public String why_is_global_competence_important_for_students{get; set;}
        public String how_to_be_a_global_citizen{get; set;}
        public String medical_condition_comments{get; set;}
        public String i_ve_traveled_abroad{get; set;}
        public String i_ve_lived_abroad{get; set;}
        public String none_of_the_above{get; set;}
        public String option_1_interview_date{get; set;}
        public String option_2_interview_date{get; set;}
        public String option_3_interview_date{get; set;}
        public String option_1_time_preference{get; set;}
        public String option_2_time_preference{get; set;}
        public String option_3_time_preference{get; set;}
        public String created_date{get; set;}
        public String created_by{get; set;}
        public String last_modified_date{get; set;}
        public String last_modified_by{get; set;}
        public String source{get; set;}
        public String agreement_text{get; set;}
        public String agreement_ind{get; set;}
    }
    
    public class ZipcodeAssignments{
        public String id{get; set;}
        public String ioc_code{get; set;}
        public String zip_from{get; set;}
        public String zip_to{get; set;}
        public String assigned_org_id{get; set;}
        //public String assigned_org_english_name{get; set;}
        //public String assigned_org_native_name{get; set;}
        //public String assigned_org_level{get; set;}
        //public String city{get; set;} //using by ITA partner only
    }
    
    public List<Response> response;
    
    public List<Matrix> matrix;
    
    public List<HostingFactSheets> hostingFactSheets;
    
    public List<MTPDates> mtpDates;
    
    public List<Organization> organizations;
    
    public List<ZipcodeAssignments> zipcodeAssignments;
    
    public List<ServiceAndOA> serviceAndOAList;
}