@isTest(SeeAllData=false)
global class Afs_SettingControllerTest {
	
    @isTest
    static void getPicklistValuesTest() {  
        Test.startTest();
        	system.assertEquals(true,Afs_SettingController.getPicklistValues('Contact','Gender__c,How_did_you_hear_about_AFS__c,Country_of_Legal_Residence__c,NationalityCitizenship__c').get('NationalityCitizenship__c').Size() > 0);
        Test.stopTest();
    }
    
    @isTest
    static void saveRecordTest() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createUserWithContact();
        Test.startTest();
        	list<User> us = [SELECT id,ContactId FROM User WHERE ContactId != null AND isActive = true AND ContactId IN: [SELECT id,FirstName,LastName,Email FROM Contact]];
            System.runAs(us[0]){
                Contact  con = [SELECT id,FirstName,LastName,Email FROM Contact];
                con.FirstName = 'New New';
                con.LastName = 'New LastName';
                con.Email = 'user@appohm.com';
                Afs_SettingController.saveRecord(con);
            }
        try{
            Afs_SettingController.saveRecord(null);
        }catch(Exception e){
            system.assertEquals(true, e != null);
        }
        Test.stopTest();
        system.assertEquals(true,[SELECT id,email FROM User][0].email != null);
    }
    
    @isTest
    static void changePasswordTest() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createUserWithContact();
        Test.startTest();
        	list<User> us = [SELECT id,ContactId FROM User WHERE ContactId != null AND isActive = true AND ContactId IN: [SELECT id,FirstName,LastName,Email FROM Contact]];
            System.runAs(us[0]){
                try{
                    String passWrd = 'Welcome#12';
                Afs_SettingController.changePassword(passWrd);
                }catch(Exception e){
                    system.assertEquals(true,e != null);
                }
                
            }
        Test.stopTest();
        system.assertEquals(false,[SELECT id,ContactId FROM User][0].ContactId != null);
    }
    
    @isTest
    static void changePasswordExceptionTest() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createUserWithContact();
        Test.startTest();
        	list<User> us = [SELECT id,ContactId FROM User WHERE ContactId != null AND isActive = true AND ContactId IN: [SELECT id,FirstName,LastName,Email FROM Contact]];
            System.runAs(us[0]){
                try{
                    String passWrd = 'Wel';
                    Afs_SettingController.changePassword(passWrd);
                }catch(Exception e){
                    system.assertEquals(true,e != null);
                }
            }
        Test.stopTest();
    }
    
}