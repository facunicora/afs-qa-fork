/** 
 *  Description : Controller class for getting list of program offered
 *                /additional destination and commit of the result 
 */
global without sharing class Afs_DisplayProgramSearchResultController {
    
    @AuraEnabled
    global static List <DisplayPicklistValueWrapper > getDestinations (sObject objObject, string fld){
        String communityId =  Network.getNetworkId(); 
        String queryStr = 'SELECT id, Name, Afs_SendingPartner__c';
        if(!Test.isRunningTest()){
            queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
        }else{
            queryStr += ' FROM AFS_Community_Configuration__c';
        }
        list<AFS_Community_Configuration__c>  listCommunityConfig;
        if(String.isNotBlank(communityId) || Test.isRunningTest()){
            
            listCommunityConfig  =  Database.query(queryStr);
            
        }
        list<DisplayPicklistValueWrapper> wrapperList = new list<DisplayPicklistValueWrapper>();
                    
        //list < Schema.PicklistEntry > values = Afs_UtilityApex.getPicklistVal(objObject,fld);
        set<String> fieldsSet = new set<String>();
        fieldsSet.add('Destinations__c');fieldsSet.add('Show_in_portal__c');
        Afs_SecurityEnforceUtility.checkObjFieldsAccess(fieldsSet,'Program_Offer__c',false);
        Date todayDt = Date.today(); 
        // GET ALL DESTINATIONS VALUE FROM EXISTING PROGRAM OFFERED
        set<String> destinationsSet = new set<String> ();
        for(Program_Offer__c prgOffr : [SELECT id,Destinations__c,Show_in_portal__c FROM Program_Offer__c 
                                        WHERE Sending_Partner__c =: listCommunityConfig[0].Afs_SendingPartner__c
                                        AND Show_in_portal__c = true 
                                        AND Applications_Received_From_local__c <=: todayDt
                                        AND Applications_Received_To_local__c >=: todayDt
                                        AND Destinations__c != NULL 
                                        ORDER BY Destinations__c]){
            destinationsSet.add(prgOffr.Destinations__c);
        }
        integer counterNum = 1;
        map<String,String> valueMap = Afs_UtilityApex.getAccountCountryLabel();
        for (String destinationName: destinationsSet) { 
            wrapperList.add(new DisplayPicklistValueWrapper(fld , destinationName , false ,counterNum,valueMap.get(destinationName)));
            counterNum++;            
        }         
        
        return wrapperList;
        
    }
    
    @AuraEnabled
    global static list<String> getCurrentUserName(){
        list<String> contactDetails =  new list<String>();
        String ContactId = [SELECT id,ContactId FROM USER WHERE id =: UserInfo.getUserId()].ContactId;
        Contact contact  = [SELECT id,FirstName FROM Contact WHERE id =:ContactId];
        contactDetails.add(contact.FirstName);
        Application__c app = [SELECT id,Additional_destination_1__c, Additional_destination_2__c, Additional_destination_3__c
                           FROM Application__c WHERE Applicant__c =: ContactId];
        contactDetails.add((String.isNotBlank(app.Additional_destination_1__c) ? app.Additional_destination_1__c : ''));
        contactDetails.add((String.isNotBlank(app.Additional_destination_2__c) ? app.Additional_destination_2__c : ''));
        contactDetails.add((String.isNotBlank(app.Additional_destination_3__c) ? app.Additional_destination_3__c : ''));
        return contactDetails; 
    }
    
    @AuraEnabled
    global static list<DisplayProgramWrapper> getProgramWrapper (map<String,List<Object>> mapStringyfied, String preSelectedProgram,Boolean isSearched){
        // Code added to get destinations translation
        map<String,String> valueMap = Afs_UtilityApex.getAccountCountryLabel();
        String communityId =  Network.getNetworkId(); 
        // ADd code to make Scholarship icon visible
        map <id,boolean> mapPrgmOffrvsIsScholarship = new map <id,boolean> ();
            
        String queryStr = 'SELECT id, Name, Afs_SendingPartner__c';
        if(!Test.isRunningTest()){
            queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
        }else{
            queryStr += ' FROM AFS_Community_Configuration__c';
        }
        list<AFS_Community_Configuration__c>  listCommunityConfig;
        String sendingPartner;
        if(String.isNotBlank(communityId)  || Test.isRunningTest()){
            
            listCommunityConfig  =  Database.query(queryStr);
            sendingPartner = listCommunityConfig[0].Afs_SendingPartner__c;
        }
        Date todayDt = Date.today(); 
        list<DisplayProgramWrapper> wrapperList = new list<DisplayProgramWrapper>();
        set<String> fieldsSet = new set<String>();
        fieldsSet.add('Destinations__c');fieldsSet.add('Show_in_portal__c');
        fieldsSet.add('To__c');fieldsSet.add('From__c');
        fieldsSet.add('Length__c');fieldsSet.add('Program_Type__c');
        fieldsSet.add('Total_Cost_numeric__c');
        Afs_SecurityEnforceUtility.checkObjFieldsAccess(fieldsSet,'Program_Offer__c',false);
        String query = 'SELECT id,Name,To__c,From__c,Program_Website_URL__c,Destinations__c,Tolabel(Length__c),Program_Type__c,Total_Cost_numeric__c,Program_Title__c,Currency__c FROM Program_Offer__c WHERE Sending_Partner__c =: sendingPartner AND Show_in_portal__c = true AND Applications_Received_From_local__c <: todayDt AND Applications_Received_To_local__c >=: todayDt';
        String queryPrexist = '';
        Boolean noExecute = false;
        system.debug(mapStringyfied); 
        boolean isValidId = true;
        /*try{
            Id prId = Id.valueOf(preSelectedProgram);
        }catch(Exception e){
            isValidId = false;
        }*/
        if(mapStringyfied != null && !mapStringyfied.isEmpty()){
            if(query.indexOfIgnoreCase('WHERE') < 0) {
                query += ' WHERE ';
            }else{
                query += ' AND ';
            }
           if( mapStringyfied.keySet().size() > 0) query += '('; 
            for(String key : mapStringyfied.keySet()){
                if(mapStringyfied.get(key) != null && !mapStringyfied.get(key).isEmpty()){
                    if(key == 'Area_Of_Interest__c'){
                        query += '( ' + key + ' includes (';
                        String valSet = '';
                        for(Object val : mapStringyfied.get(key)){
                            valSet +=  '\'' + (String.valueOf(val)) + '\',';
                        }
                        valSet = valSet.removeEnd(',');
                        
                        query += valSet + ' )';
                        query.trim();
                        query += ') OR '; 
                    }else{
                        query += '(';
                        for(Object val : mapStringyfied.get(key)){
                            query +=  key + ' =\'' + (String.valueOf(val)) + '\' OR ';
                        }
                        query.trim();
                        if(query.indexOfIgnoreCase('OR') > 0) 
                            query = query.substring(0, query.lastIndexOf('OR')); 
                        query += ') OR ';  
                    }
                    
                }
                
            }
            if(query.indexOfIgnoreCase('OR') > 0) 
                query = query.trim().substring(0, query.lastIndexOf('OR'));
                query += ')';
            // Create Where Clause String
            if(String.isNotBlank(preSelectedProgram) && isValidId){
                queryPrexist =query + ' AND Name =:preSelectedProgram';
                query += ' AND Name !=:preSelectedProgram';  
            }
        }else{
            // Create Where Clause String
            noExecute  = (!isSearched);
            if(String.isNotBlank(preSelectedProgram)){
                
                query += ' AND Name !=:preSelectedProgram';  
            }
        }
        
        
        query += ' ORDER BY CreatedDate';
        
        
        if(String.isNotBlank(preSelectedProgram)){
            queryPrexist = 'SELECT id,Name,To__c,From__c,Destinations__c,Program_Website_URL__c,Tolabel(Length__c),Program_Type__c,Total_Cost_numeric__c,Program_Title__c,Currency__c  FROM Program_Offer__c WHERE Name =:preSelectedProgram AND Sending_Partner__c =: sendingPartner AND Show_in_portal__c = true AND Applications_Received_From_local__c <=: todayDt AND Applications_Received_To_local__c >=: todayDt';
            list<Program_Offer__c> programOfferedListOne = Database.query(queryPrexist);
            
            // ADd code to make Scholarship icon visible
            if(!programOfferedListOne.isEmpty()){
                for(Sch_in_Program__c objSchProgram : [SELECT id,Scholarship__c,Program_Offer__c FROM Sch_in_Program__c WHERE Program_Offer__c =: programOfferedListOne[0].id]){
                    mapPrgmOffrvsIsScholarship.put(objSchProgram.Program_Offer__c,true);
                }
                list<ContentDocumentLink> documentLinkList = [SELECT id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: programOfferedListOne[0].id ORDER BY ContentDocument.Title];
                list<ContentVersion> cv;
                if(documentLinkList != null && !documentLinkList.isEmpty()){
                    cv = [SELECT id,VersionData FROM ContentVersion WHERE ContentDocumentId =: documentLinkList[0].ContentDocumentId];
                }
                for(Program_Offer__c prgramObj : programOfferedListOne){
                    
                    if(cv != null){
                        DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,EncodingUtil.base64Encode(cv[0].VersionData),true,0,true,false);
                        objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                        objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                        wrapperList.add(objWrapper);
                    }else{
                        DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,'',true,0,true,false);
                        objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                        objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                        wrapperList.add(objWrapper);
                    }
                }
            }
            
        }
        //get records of program interest corresponding to the contact object
        //and if interest record present prepopulate them in the wrapper list
        set<id> prgrmOfferdSet = new set<id>();
        String ContactId = [SELECT id,ContactId FROM USER WHERE id =: UserInfo.getUserId()].ContactId;
        if(String.isNotBlank(ContactId)){
            //GET ALL INTEREST RECORDS FOR THIS ContactId
            List<Program_Offer_in_App__c> interestList = [SELECT id,Program_Offer__c,Program_Offer__r.Name FROM Program_Offer_in_App__c  WHERE Contact__c =:ContactId];
            
            if(!interestList.isEmpty()){
                for(Program_Offer_in_App__c interestObj : interestList){
                    if(String.isNotBlank(preSelectedProgram) && isValidId){
                        if(interestObj.Program_Offer__r.Name != preSelectedProgram){
                            prgrmOfferdSet.add(interestObj.Program_Offer__c);
                        }else{
                            wrapperList[0].isSavedRecord = true;
                        }
                    }else{
                        prgrmOfferdSet.add(interestObj.Program_Offer__c);
                    }
                    
                } 
                
                String queryPrexist1 = 'SELECT id,Name,To__c,From__c,Program_Website_URL__c,Destinations__c,Tolabel(Length__c),Program_Type__c,Total_Cost_numeric__c,Program_Title__c,Currency__c, (SELECT id FROM Attachments) FROM Program_Offer__c WHERE id IN:prgrmOfferdSet AND Sending_Partner__c =: sendingPartner AND Show_in_portal__c = true AND Applications_Received_From_local__c <=: todayDt AND Applications_Received_To_local__c >=: todayDt ORDER BY CreatedDate';
                list<Program_Offer__c> programOfferedList = Database.query(queryPrexist1);
                set<id> ContentDocumentIdSet = new set<id>();
                map<id,id> mapContentDocumentIdandLink = new map<id,id>();
                map<id,ContentVersion> mapContentDocumentIdandVersion = new map<id,ContentVersion>();
                if(!prgrmOfferdSet.isEmpty()){
                    // ADd code to make Scholarship icon visible
                    for(Sch_in_Program__c objSchProgram : [SELECT id,Scholarship__c,Program_Offer__c FROM Sch_in_Program__c WHERE Program_Offer__c IN : prgrmOfferdSet]){
                        mapPrgmOffrvsIsScholarship.put(objSchProgram.Program_Offer__c,true);
                    }
                    for(ContentDocumentLink documentLink: [SELECT id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN: prgrmOfferdSet ORDER BY ContentDocument.Title]){
                        if(!mapContentDocumentIdandLink.containsKey(documentLink.LinkedEntityId))
                            mapContentDocumentIdandLink.put(documentLink.LinkedEntityId,documentLink.ContentDocumentId);
                        ContentDocumentIdSet.add(documentLink.ContentDocumentId);
                    }
                    
                    for(ContentVersion cv : [SELECT id,FileType,VersionData,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: ContentDocumentIdSet]){
                        if((cv.FileType.equalsIgnoreCase('PNG') || cv.FileType.equalsIgnoreCase('JPG') || cv.FileType.equalsIgnoreCase('JPEG'))){
                            mapContentDocumentIdandVersion.put(cv.ContentDocumentId,cv);
                        }
                        
                    }
                    
                }
                
                integer counterNum = 1;
                
                for(Program_Offer__c prgramObj : programOfferedList){
                    if(mapContentDocumentIdandLink.containsKey(prgramObj.id)){
                        if(mapContentDocumentIdandVersion.containsKey(mapContentDocumentIdandLink.get(prgramObj.id))){
                            //wrapperList.add(new DisplayProgramWrapper(prgramObj,EncodingUtil.base64Encode(mapContentDocumentIdandVersion.get(mapContentDocumentIdandLink.get(prgramObj.id)).VersionData),true,counterNum,false,true));                    
                        	DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,EncodingUtil.base64Encode(mapContentDocumentIdandVersion.get(mapContentDocumentIdandLink.get(prgramObj.id)).VersionData),true,counterNum,false,true);
                            objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                            objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                            wrapperList.add(objWrapper);
                        }else{
                            DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,'',true,counterNum,false,true);
                            objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                            objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                            wrapperList.add(objWrapper);
                            //wrapperList.add(new DisplayProgramWrapper(prgramObj,'',true,counterNum,false,true));  
                        }
                    }else{
                        DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,'',true,counterNum,false,true);
                        objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                        objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                        wrapperList.add(objWrapper);
                        //wrapperList.add(new DisplayProgramWrapper(prgramObj,'',true,counterNum,false,true));
                    }
                    counterNum++;
                }
            }
        }
        
        if(!noExecute){
            list<Program_Offer__c> programOfferedList = Database.query(query);
            //variables used for accessing content version 
            set<id> programOfferedSet = new set<id>();
            set<id> ContentDocumentIdSet = new set<id>();
            map<id,id> mapContentDocumentIdandLink = new map<id,id>();
            map<id,ContentVersion> mapContentDocumentIdandVersion = new map<id,ContentVersion>();
            for(Program_Offer__c progrmOffrd : programOfferedList){
                if(prgrmOfferdSet != null && !prgrmOfferdSet.contains(progrmOffrd.id))
                    programOfferedSet.add(progrmOffrd.id);
            }
            
            // ADd code to make Scholarship icon visible
            for(Sch_in_Program__c objSchProgram : [SELECT id,Scholarship__c,Program_Offer__c FROM Sch_in_Program__c WHERE Program_Offer__c IN : programOfferedSet]){
              	mapPrgmOffrvsIsScholarship.put(objSchProgram.Program_Offer__c,true);
            } 
            
            if(!programOfferedSet.isEmpty()){

                for(ContentDocumentLink documentLink: [SELECT id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN: programOfferedSet  ORDER BY ContentDocument.Title]){
                    if(!mapContentDocumentIdandLink.containsKey(documentLink.LinkedEntityId))
                        mapContentDocumentIdandLink.put(documentLink.LinkedEntityId,documentLink.ContentDocumentId);
                    ContentDocumentIdSet.add(documentLink.ContentDocumentId);
                }
                
                for(ContentVersion cv : [SELECT id,FileType,VersionData,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: ContentDocumentIdSet]){
                    if((cv.FileType.equalsIgnoreCase('PNG') || cv.FileType.equalsIgnoreCase('JPG') || cv.FileType.equalsIgnoreCase('JPEG'))){
                        mapContentDocumentIdandVersion.put(cv.ContentDocumentId,cv);
                    }
                }
                
            }
            
            integer counterNum = 1;
            for(Program_Offer__c prgramObj : programOfferedList){
                if(prgrmOfferdSet != null && !prgrmOfferdSet.contains(prgramObj.id)){
                    if(mapContentDocumentIdandLink.containsKey(prgramObj.id)){
                        if(mapContentDocumentIdandVersion.containsKey(mapContentDocumentIdandLink.get(prgramObj.id))){
                            DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,EncodingUtil.base64Encode(mapContentDocumentIdandVersion.get(mapContentDocumentIdandLink.get(prgramObj.id)).VersionData),false,counterNum,false,false);
                            objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                            objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                            wrapperList.add(objWrapper);
                            //wrapperList.add(new DisplayProgramWrapper(prgramObj,EncodingUtil.base64Encode(mapContentDocumentIdandVersion.get(mapContentDocumentIdandLink.get(prgramObj.id)).VersionData),false,counterNum,false,false));                    
                        }else{
                            DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,'',false,counterNum,false,false);
                            objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                            objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                            wrapperList.add(objWrapper);
                            //wrapperList.add(new DisplayProgramWrapper(prgramObj,'',false,counterNum,false,false));  
                        }
                    }else{
                        DisplayProgramWrapper objWrapper = new DisplayProgramWrapper(prgramObj,'',false,counterNum,false,false);
                        objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(prgramObj.id) ? true : false;
                        objWrapper.programObjDestination = (prgramObj.Destinations__c != null ? valueMap.get(prgramObj.Destinations__c) : '');
                        wrapperList.add(objWrapper);
                        //wrapperList.add(new DisplayProgramWrapper(prgramObj,'',false,counterNum,false,false));
                    }
                    counterNum++;
                }
            }
            
        }
        
        return wrapperList;
        
    }

    @AuraEnabled
    global static Application__c saveAndContinueProgram(list<String> offerIdsToDeleteInterest,list<String> offerIdsToSave,String additionalVal){
        // UPDATE ADDITIONAL DESTINATIONS
        // On Contact
        set<String> fieldsSet = new set<String>();
        fieldsSet.add('Three_Additional_Destination__c');
        Afs_SecurityEnforceUtility.checkObjFieldsAccess(fieldsSet,'Contact',false);
        String ContactId = [SELECT id,ContactId FROM USER WHERE id =: UserInfo.getUserId()].ContactId;
        //Contact contact  = [SELECT id,Three_Additional_Destination__c FROM Contact WHERE id =:ContactId];
        //contact.Three_Additional_Destination__c = additionalVal;
        //update contact;
        //Get Active Application Record
            Application__c appContact;
        Boolean isAppCreated = false;
        //create application
        list<Application__c> applications = [SELECT id,Status__c,Additional_destination_1__c, Additional_destination_2__c, 
                                             Additional_destination_3__c FROM Application__c 
                                             WHERE Applicant__c =: ContactId ORDER BY CreatedDate LIMIT 1];
        if(applications.size() > 0){
            appContact = applications[0];
            appContact.Status__c  = 'Product Desire';
            list<String> addVals = additionalVal.split(';');
            
            appContact.Additional_destination_1__c  = ((String.isNotBlank(addVals[0]) || addVals[0] != 'undefined') ? addVals[0] : null);
            appContact.Additional_destination_2__c  = ((String.isNotBlank(addVals[1]) || addVals[1] != 'undefined') ? addVals[1] : null);
            appContact.Additional_destination_3__c  = ((String.isNotBlank(addVals[2]) || addVals[2] != 'undefined') ? addVals[2] : null);
            update appContact;
        }else{
            //set contact and first program
            appContact = new Application__c(Applicant__c = ContactId,Status__c = 'Product Desire');
            insert appContact;
            isAppCreated = true;
        }
        
        //DELETE
        //Interest records of the deselected Prgram Offered must be deleted
        fieldsSet = new set<String>();
        fieldsSet.add('Contact__c');fieldsSet.add('Program_Offer__c');
        Afs_SecurityEnforceUtility.checkObjFieldsAccess(fieldsSet,'Program_Offer_in_App__c',false);
        if(!offerIdsToDeleteInterest.isEmpty()){
            set<String> prgrmOfferdSet = new set<String>();
            prgrmOfferdSet.addAll(offerIdsToDeleteInterest);
            list<Program_Offer_in_App__c> interestList = [SELECT id FROM Program_Offer_in_App__c WHERE Contact__c =: ContactId AND Program_Offer__c IN: prgrmOfferdSet];
            if(!interestList.isEmpty()){
                Afs_App_Opportunity_SyncTrigger_Helper.isTriggerRunning = false;
                DELETE interestList;
            }            
        }
        
        //INSERT 
        //Create new Interest records 
        // For the Program Offered Selected
        fieldsSet = new set<String>();
        fieldsSet.add('Applicant__c');
        Afs_SecurityEnforceUtility.checkObjFieldsAccess(fieldsSet,'Application__c',false);
        
        if(!offerIdsToSave.isEmpty()){
            
            list<Program_Offer_in_App__c> interestList = new list<Program_Offer_in_App__c> ();
            set<String> prgrmOfferdSet = new set<String>(offerIdsToSave);
            
            
            //create application programs
            for(String offerObj : prgrmOfferdSet){
                Program_Offer_in_App__c interestObj = new Program_Offer_in_App__c();
                interestObj.Contact__c = ContactId;
                interestObj.Program_Offer__c = offerObj;
                interestObj.Application__c = appContact.id;   
                // Checkbox add to field new requirement
                interestObj.Selected__c = true; 
                interestList.add(interestObj); 
            }
            Afs_App_Opportunity_SyncTrigger_Helper.isTriggerRunning = false;
            insert interestList;
            
            if(isAppCreated){
                String queryStr = 'SELECT id,Name, ';
                for(String fieldAPIName : Afs_UtilityApex.getFields('Application__c',true)){
                    queryStr += fieldAPIName + ' ,';
                }
                queryStr = queryStr.removeEnd(',');
                queryStr += ' FROM Application__c WHERE Applicant__c =: ContactId';
                list<Application__c>  listApplication =  Database.query(queryStr);
                appContact = listApplication[0];
            }
        }
        
        return appContact;
    }
        
    
    global class DisplayProgramWrapper {
        
        @auraEnabled
        global Boolean isSelected {get;set;}
        @auraEnabled
        global String imageUrl {get;set;}
        @auraEnabled
        global Program_Offer__c programObj {get;set;}
        @auraEnabled
        global Integer indexId {get;set;}
        @auraEnabled
        global Boolean isSelectedRecord {get;set;}
        @auraEnabled
        global Boolean isSavedRecord {get;set;}
        @auraEnabled
        global Boolean isScholarshipPresent {get;set;}
        @auraEnabled
        global String programObjDestination {get;set;}
        
        global DisplayProgramWrapper(Program_Offer__c prog, String base64Str, Boolean selected, Integer num, Boolean isSelectedRecordURL,Boolean isSavedRecordVal){
            programObj = prog;
            imageUrl = (String.isBlank(base64Str) ? '' : 'data:image/jpeg;base64,' + base64Str);
            isSelected = selected;
            indexId = num;
            isSelectedRecord = isSelectedRecordURL;
            isSavedRecord = isSavedRecordVal;
        }
    }
    
    global class DisplayPicklistValueWrapper {
    
        @auraEnabled
        global Boolean isSelected {get;set;}
        @auraEnabled
        global String Name{get;set;}
        @auraEnabled
        global String FieldName {get;set;}
        @auraEnabled
        global String FieldLbl {get;set;}
        @auraEnabled
        global Integer indexId {get;set;}  
        
        global DisplayPicklistValueWrapper(String FldName, String Nam, Boolean selected, Integer num,String fieldLbla){
            FieldName = FldName;
            Name = Nam;
            isSelected = selected;
            indexId = num;
            FieldLbl = fieldLbla;
        }    
      
    }
    
}