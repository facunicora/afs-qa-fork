public with sharing class RemoteSiteConnection implements Database.AllowsCallouts {
    public static HttpResponse pullObjects(String endPoint){
        String jsonResponse = '';

        //Construct HTTP request and response
        HttpRequest httpRequest = new HttpRequest();
        GlobalAPI__c integrationCustomSettings = GlobalAPI__c.getInstance();
        
        httpRequest.setHeader('authenticationToken', integrationCustomSettings.AdminKey__c + ':' + integrationCustomSettings.SecretKey__c);
        HttpResponse httpResponse = new HttpResponse();
        Http http = new Http();
     
        //Set Method and Endpoint and Body
        httpRequest.setMethod('GET');
        httpRequest.setEndpoint(endPoint);
        httpRequest.setTimeout(120000);
        
        try {
           //Send endPoint
            httpResponse = http.send(httpRequest);
            
            system.debug('status: ' + httpResponse.getStatus());
            system.debug('statusCode: ' + httpResponse.getStatusCode());
            if(httpResponse.getStatusCode() == 200){
                jsonResponse = httpResponse.getBody();   
            }else{
                jsonResponse = '';
            }
            
            system.debug('jsonResponse: ' + jsonResponse);
                     
        } catch(System.CalloutException e) {
            System.debug(httpResponse.toString());
        }

        return httpResponse;
    }
    
    @future(callout=true)
    public static void postObject(String endPoint, String jsonToPost){
      String jsonResponse = '';
      String httpResponseString = '';

        //Construct HTTP request and response
        HttpRequest httpRequest = new HttpRequest();
        GlobalAPI__c integrationCustomSettings = GlobalAPI__c.getInstance();
        
        httpRequest.setHeader('authenticationToken', integrationCustomSettings.AdminKey__c + ':' + integrationCustomSettings.SecretKey__c);
        HttpResponse httpResponse = new HttpResponse();
        Http http = new Http();
     
        //Set Method and Endpoint and Body
        httpRequest.setMethod('POST');
        httpRequest.setEndpoint(endPoint);
        httpRequest.setTimeout(120000);
        httpRequest.setBody(jsonToPost);
        httpRequest.setHeader('apiAction', 'upsert');
        httpRequest.setHeader('Content-Type', 'application/json');
        System.debug('jsonToPost: ' + jsonToPost);
        
        
        try {
           if(!Test.isRunningTest()){
               
            //Send endPoint
                httpResponse = http.send(httpRequest);
                
                system.debug('status: ' + httpResponse.getStatus());
                system.debug('statusCode: ' + httpResponse.getStatusCode());
                
                if(httpResponse.getStatusCode() == 200){
                    jsonResponse = httpResponse.getBody();   
                }else{
                    jsonResponse = '';
                }
                
                system.debug('jsonResponse: ' + jsonResponse);
           }
                     
        } catch(System.CalloutException e) {
          System.debug('-----exception-----');
          System.debug(e);
            System.debug(httpResponse.toString());
        }

        //return jsonResponse; //httpResponse;
    }
}