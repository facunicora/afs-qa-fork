public class RemoteSiteManager implements Database.AllowsCallouts{
	public String urlToMakeCall {get; set;}
	public String httpResponseBody {get; set;}
	public HttpResponse httpResponse {get; set;}
	public String httpRequestBody {get; set;}
	public List<RemoteSiteModel.response> responseList {get; set;}
	public List<String> genericList {get; set;}
    public Map<string,sObject> mapObjects {get; set;}
    public Map<String, Account> mapAccountsByIOCCode {get; set;}
       	
	public RemoteSiteModel remoteSiteModel {get; set;}
	
	public GlobalAPI__c integrationCustomSettings;
	
	public RemoteSiteManager(){
		initialize();
	}
	
	private void initialize(){
		integrationCustomSettings = GlobalAPI__c.getInstance();
    	responseList = new List<RemoteSiteModel.response>();
    	genericList = new List<String>();
        mapObjects = new Map<String,sObject>();
        mapAccountsByIOCCode = new Map<String, Account>();
        for(Account acc : [Select Id, Name, IOC_Code__c From Account where recordtype.name = 'AFS Partner']){
            mapAccountsByIOCCode.put(acc.IOC_Code__c,acc);
        }
	}
	
	public void sendAttachmentToGlobalLink(Id attachmentId){
        Attachment attachToSend = [select Id, ContentType, Body from Attachment where Id=:attachmentId];
        
        String attachmentJson = RemoteSiteParser.AttachmentToAttachmentRawData(attachToSend);
        this.httpRequestBody = attachmentJson;
        
        this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/attachmentRawData';
        this.postUrl();
        System.debug('-----------');
        
        /*
        callout=true
        
        try{
            RemoteSiteParser.ApiResponse responseParsed = RemoteSiteParser.DeserializeResponse(this.httpResponseBody);
            System.debug(responseParsed);
        }
        catch(Exception e){
            system.debug(e.getMessage());
            
        }*/
    }
	
    public Static void sendApplicationToGlobalLinkFuture(Id applicationId){
        RemoteSiteManager rsm = new RemoteSiteManager();
        rsm.sendApplicationToGlobalLink(applicationId);
    }
    
	public void sendApplicationToGlobalLink(Id applicationId){
               Application__c applicationToSend = [select Id, Applicant__r.AAre_you_attending_University_College_o__c, Additional_destination_1__c, Additional_destination_2__c, Additional_destination_3__c, Applicant__c, Applicant__r.AccountId, Applicant__r.Account.Global_Link_Family_ID__c, Applicant__r.Account.IOC_Code__c, Applicant__r.Describe_yourself_as_a_student__c, 
                                            Applicant__r.Global_Link_Person_ID__c, Applicant__r.How_did_you_hear_about_AFS__c, Applicant__r.Keep_me_informed_Opt_in__c, Applicant__r.MailingPostalCode, Applicant__r.MobilePhone,
                                            Applicant__r.Parent_Legal_Guardian_Other__c, Applicant__r.SecondCitizenship__c, Applicant__r.Terms_Service_Agreement__c, Applicant__r.What_school_do_you_attend__c, Applicant__r.When_would_did_you_graduate__c,  Global_Link_Participant_App_ID__c, Applicant__r.Name_of_University_College_School__c,
                                            Applicant__r.Language_1__c,Applicant__r.Language_1_Proficiency__c,Applicant__r.Language_2__c,Applicant__r.Language_2_Proficiency__c,Applicant__r.Language_3__c,Applicant__r.Language_3_Proficiency__c,Applicant__r.Language_4__c,Applicant__r.Language_4_Proficiency__c,
                                            Applicant__r.Criminal_Convictions__c,Applicant__r.No_restrictions_or_allergies__c,Applicant__r.Limited_in_the_activities_I_can_do__c,Applicant__r.Have_allergies__c,Applicant__r.Take_medications__c,Applicant__r.Can_t_live_with_pets_s__c,Applicant__r.Can_t_live_with_a_smoker__c,
                                            Applicant__r.Medical_Condition_Comments__c,Applicant__r.No_dietary_restrictions__c,Applicant__r.Have_food_allergies__c,Applicant__r.Vegetarian__c,Applicant__r.Vegan__c,Applicant__r.Kosher__c,Applicant__r.Halal__c,Applicant__r.Celiac__c,Applicant__r.Dietary_Comments__c,
                                            Applicant__r.Willing_to_change_diet_during_program__c,Applicant__r.treated_for_any_health_issues_physical__c,Applicant__r.Health_Issue_Description__c,What_are_you_looking_for__c,What_do_you_want_out_of_this_experience__c,How_would_friends_family_describe_you__c,Tell_us_about_yourself__c,
                                            Program_you_participated__c,Applicant__r.Dual_citizenship__c,Applicant__r.Home_Mother__c,Applicant__r.Home_Father__c,Applicant__r.Home_Stepmother__c,Applicant__r.Home_Stepfather__c,Applicant__r.Home_Sister_Stepsister__c,Applicant__r.Home_Brother_Stepbrother__c,
                                            Applicant__r.Home_Sister_Stepsisters_Age__c,Applicant__r.Home_Brother_Stepbrothers_Age__c,Applicant__r.Home_Grandmother__c,Applicant__r.Home_Grandfather__c,Applicant__r.Home_Other__c,Applicant__r.Home_Other_Relationship__c,Applicant__r.Instagram__c,Applicant__r.LinkedIn__c,Applicant__r.Facebook__c,
                                            Applicant__r.Twitter__c,Applicant__r.Snapchat__c,Applicant__r.YouTube__c,Access_to_religious_services__c,Ability_to_live_with_household_pets__c,Open_to_Same_Sex_Host_Parents__c,Open_to_Single_Host_Parent__c,Open_to_Sharing_a_host_family__c,Applicant__r.LGBTQ_Member__c,Why_is_Global_Competence_important_for_s__c,
                                            How_to_be_a_Global_Citizen__c,Things_to_share__c,Describe_a_normal_day_in_your_life__c,What_you_bring_to_your_AFS_experience__c,Food_thoughts__c,Favorite_travel_destinations_books__c,Video_for_Host_Family__c,Applicant__r.Tshirt__c,BV_Record_Locator__c,BV_Airline__c,BV_Flight_Number__c,BV_Departure_Time__c,
                                            BV_Arrival_Time__c,BV_Departure_Airport__c,BV_Arrival_Airport__c,BV_Domestic_Travel_Arrangements__c,BV_Arrival_to_gateway_city_who_accompany__c,BV_Gateway_City__c,BV_Arrive_in_prior_to_the_start__c,BV_Additional_Comments__c,BV_Contact_First_Name__c,BV_Contact_Last_Name__c,BV_Contact_Phone__c,
                                            BV_Contact_Relationship__c,CBH_Record_Locator__c,CBH_Airline__c,CBH_Flight_Number__c,CBH_Departure_Time__c,CBH_Arrival_Time__c,CBH_Departure_Airport__c,CBH_Arrival_Airport__c,CBH_Domestic_Travel_Arrangements__c,CBH_Arrival_to_gateway_city_with_who__c,CBH_Gateway_City__c,
                                            CBH_Arrive_in_prior_to_the_start__c,CBH_Additional_Comments__c,CBH_Contact_First_Name__c,CBH_Contact_Last_Name__c,CBH_Contact_Phone__c,CBH_Contact_Relationship__c
                                            from Application__c where Id=:applicationId];
        
        //mover a trigger de insert
        if(applicationToSend.Global_Link_Participant_App_ID__c == null){
            applicationToSend.Global_Link_Participant_App_ID__c = RemoteSiteParser.generateNewUUID();
            
            update applicationToSend;
        }
        
        //si entra en el if, es necesario ejecutar de nuevo
        
        String participantAppJson = RemoteSiteParser.ApplicationToParticipantApp(applicationToSend);
        this.httpRequestBody = participantAppJson;
        
        this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/participantApps';
        this.postUrl();
        System.debug('-----------');
        /*
        callout=true
        try{
            RemoteSiteParser.ApiResponse responseParsed = RemoteSiteParser.DeserializeResponse(this.httpResponseBody);
            System.debug(responseParsed);
        }
        catch(Exception e){
            system.debug(e.getMessage());
            
        }
        */
    }
    
    public Static void sendPersonToGlobalLinkFuture(Id contactId){
        RemoteSiteManager rsm = new RemoteSiteManager();
        rsm.sendPersonToGlobalLink(contactId);
    }
    
    public void sendPersonToGlobalLink(Id contactId){
        Contact contactToSend = [select Id, AccountId, Account.Global_Link_Family_ID__c, Account.Id, Account.IOC_Code__c, BirthDate, Email, FirstName, Gender__c, Gender_Other__c, Global_Link_Person_ID__c, LastName, Title From Contact where Id=:contactId];
        
        //mover a trigger de insert
        if(contactToSend.Global_Link_Person_ID__c == null){
            contactToSend.Global_Link_Person_ID__c = RemoteSiteParser.generateNewUUID();
            
            update contactToSend;
        }
        
        //mover a trigger de insert
        if(contactToSend.AccountId != null && (contactToSend.Account.Global_Link_Family_ID__c == null || contactToSend.Account.Global_Link_Family_ID__c == '')){
            contactToSend.Account.Global_Link_Family_ID__c = RemoteSiteParser.generateNewUUID();
            
            update contactToSend.Account;
        }
        
        //si entra en alguno de los 2 if, es necesario ejecutar de nuevo
        
        String personJson = RemoteSiteParser.ContactToPerson(contactToSend);
        this.httpRequestBody = personJson;
        
        this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/persons';
        this.postUrl();
        System.debug('-----------');
        
        /*
        callout=true
        try{
            RemoteSiteParser.ApiResponse responseParsed = RemoteSiteParser.DeserializeResponse(this.httpResponseBody);
            System.debug(responseParsed);
        }
        catch(Exception e){
            system.debug(e.getMessage());
            
        }
        */
    }
	
	public void createMatrix(String pageIndex, String pageSize, String programYear){
		this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/matrix?apiPageIndex='+ pageIndex +'&apiPageSize='+ pageSize +'&host_ioc=&id=&program_code=&program_year='+programYear+'&send_ioc=';
    	this.callUrl();
    	
    	tryToDeserializeResponse();
        processMatrix();
    	
    	System.debug('-----------');
    	System.debug(remoteSiteModel);
	}
	
	public void createHostingFactSheets(String pageIndex, String pageSize,String programYear){
		this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/hostingFactSheet?apiPageIndex='+ pageIndex +'&apiPageSize='+ pageSize +'&host_ioc=&id=&program_code=&program_year='+programYear;
    	this.callUrl();
    	
    	tryToDeserializeResponse();
        processHostingFactSheets();
    	
    	System.debug('-----------');
    	System.debug(remoteSiteModel);
	}
    
    public void createAfs_Chapters(String pageIndex, String pageSize){
        this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/organizations?apiPageIndex='+ pageIndex +'&apiPageSize='+ pageSize +'&id=&ioc_code=&org_status=&org_type=AFS+Chapter&source=';
    	this.callUrl();
    	
    	tryToDeserializeResponse();
        processAfs_Chapters();
    	
    	System.debug('-----------');
    	System.debug(remoteSiteModel);
    }
    
    public void createZipcodeAssignments(String iocCode){
        this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/zipcodeAssignments?ioc_code='+ iocCode;//+'&zip_code='+ zipCode;
        this.callUrl();
    	
    	tryToDeserializeResponse();
    	
    	System.debug('-----------');
    	System.debug(remoteSiteModel);
	}
    
    public void createServiceAndOA(List<Application__c> lstApplication){
        List<Application__c> lstApplicationToUpdate = new List<Application__c>();
        for(Application__c app : lstApplication){
            this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/serviceAndOA?apiPageSize=&id='+app.Global_Link_Participant_App_ID__c+'&ioc_code=&person_id=&selected_partner_ioc_code=&selected_program_code=&service_ref=&source=&stage=&status=';
            this.callUrl();

                
            tryToDeserializeResponse();
            System.debug('-----------');
            System.debug(remoteSiteModel);
            
            if(remoteSiteModel != null && remoteSiteModel.serviceAndOAList != null){
               lstApplicationToUpdate.add(processServiceAndOA(app));  
            }                       
    	}
 
        
		if(!lstApplication.isEmpty()){
           List<Database.SaveResult> lstSR = Database.Update(lstApplicationToUpdate,false);
           for(Integer i = 0;i < lstSR.size() ; i++){
               Database.SaveResult sr = lstSR[i];
               if(!sr.isSuccess()){
                   System.debug(logginglevel.error,lstApplicationToUpdate[i].Id + ' --> ' + String.join(sr.getErrors(),' / '));
               }
           }
       }
	}
    
	public void createMTPDates(String pageIndex, String pageSize){
		this.urlToMakeCall = integrationCustomSettings.EndPoint__c + '/afsglobal/api/MTPDate?apiPageIndex='+ pageIndex +'&apiPageSize='+ pageSize;
    	this.callUrl();
    	
    	tryToDeserializeResponse();
        //processMTPDates();
    	
    	System.debug('-----------');
    	System.debug(remoteSiteModel);
	}
    
	public HttpResponse callUrl(){
    	httpResponse = RemoteSiteConnection.pullObjects(this.urlToMakeCall);
    	httpResponseBody = httpResponse.getBody();
    	return httpResponse;	
    }
    
    public void postUrl(){
    	
    	RemoteSiteConnection.postObject(this.urlToMakeCall, this.httpRequestBody);
    	 
        /*
        callout=true
        httpResponse = RemoteSiteConnection.postObject(this.urlToMakeCall, this.httpRequestBody);
        httpResponseBody = httpResponse.getBody();
        return httpResponse;   */ 
    }
	
    public boolean tryToDeserializeResponse(){
    	try{
           
    		//httpBody = '[' + httpBody + ']';
    		//httpBody = '{"response":[{"statusCode":"200","pagesTotal":"1300","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}]}';
    		//responseList = (List<RemoteSiteModel.response>) JSON.deserialize(httpBody, List<RemoteSiteModel.response>.class);
    		system.debug(httpResponseBody.length());
            system.debug(httpResponseBody);
    		remoteSiteModel = (RemoteSiteModel) System.JSON.deserialize(httpResponseBody, RemoteSiteModel.class);
    		return true;
    	}
    	catch(Exception e){
    		system.debug(e.getMessage());
    		return false;
    	}
    }
    
    public void processMatrix(){
        for(remoteSiteModel.Matrix mat : remoteSiteModel.matrix){
            
            Program_Offer__c po = new Program_Offer__c();
            po.Name = mat.send_ioc + '-' + mat.program_code + '-' + mat.host_ioc;
            po.Name_External_Id__c = mat.send_ioc + '-' + mat.program_code + '-' + mat.host_ioc;
            try{
            	po.Projected_Sending_Number__c = Decimal.valueOf(mat.send_projected_actual);
            }catch(Exception ex){
                system.debug(ex.getMessage());
            }
            if(mapAccountsByIOCCode.containsKey(mat.send_ioc)){
                po.Sending_Partner__c = mapAccountsByIOCCode.get(mat.send_ioc).Id;
            }            
            mapObjects.put(po.Name_External_Id__c,po);
       	}        

    }
    
    public void processHostingFactSheets(){
        for(remoteSiteModel.HostingFactSheets hfs : remoteSiteModel.hostingFactSheets){
            if(!mapObjects.containsKey(hfs.program_code + '-' + hfs.host_ioc)){
                Hosting_Program__c hp = new Hosting_Program__c();
                hp.Name = hfs.program_code + '-' + hfs.host_ioc;
                hp.Name_External_Id__c = hfs.program_code + '-' + hfs.host_ioc;
                if(mapAccountsByIOCCode.containsKey(hfs.host_ioc)){
                	hp.Host_Partner__c = mapAccountsByIOCCode.get(hfs.host_ioc).Id;
				}
                hp.Duration__c = hfs.program_duration;
                hp.Program_Content__c = hfs.program_content;
                hp.App_Received_From__c = retrieveDate(hfs.app_received_start);
                hp.App_Received_To__c = retrieveDate(hfs.app_received_end);
                hp.Program_Title__c = hfs.program_title;
				hp.Program_Description__c = hfs.program_description;
                hp.Language_of_Placement__c = retrieveLanguages(hfs.program_language);
                hp.Hemisphere__c = hfs.program_cycle;
                hp.Year__c =hfs.program_year;
                mapObjects.put(hp.Name_External_Id__c,hp);
            }
        }
        
    }

    public void processMTPDates(){
        for(remoteSiteModel.MTPDates mtp : remoteSiteModel.mtpDates){
            
            /*Program_Offer__c po = new Program_Offer__c();
            po.Name = mat.send_ioc + '-' + mat.program_code + '-' + mat.host_ioc;
            po.Name_External_Id__c = mat.send_ioc + '-' + mat.program_code + '-' + mat.host_ioc;
            try{
            	po.Projected_Sending_Number__c = Decimal.valueOf(mat.send_projected_actual);
            }catch(Exception ex){
                system.debug(ex.getMessage());
            }
            if(mapAccountsByIOCCode.containsKey(mat.send_ioc)){
                po.Sending_Partner__c = mapAccountsByIOCCode.get(mat.send_ioc).Id;
            }            
            mapObjects.put(po.Name_External_Id__c,po);*/
       	}        

    }
    
    public void processAfs_Chapters(){
        try{
            List<Account> lstAccount = new List<Account>();
            Id recordTypeID =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Chapters').getRecordTypeId();
            for(remoteSiteModel.Organization chap : remoteSiteModel.organizations){
                if(chap.ioc_code != 'USA'){
                    Account acc = new Account();
                    acc.Global_Link_External_Id__c = chap.Id;
                    acc.IOC_Code__c = chap.ioc_code;
                    acc.Name = chap.organization_name;
                    acc.Status__c = chap.org_status;
                    acc.Country__c = chap.Country;
                    acc.RecordTypeId = recordTypeID;
                    lstAccount.add(acc);
                }
            }
            
            if(lstAccount.size() > 0){
                List<Database.UpsertResult> lstUR = Database.Upsert(lstAccount,Schema.Account.Global_Link_External_Id__c,false);
                
                for(Integer i = 0;i < lstUR.size();i++){
                Database.UpsertResult ur = lstUR.get(i);
                if(!ur.isSuccess()){
                    system.debug(lstAccount.get(i).Global_Link_External_Id__c + ' --> ' + ur.getErrors());
                }
            }
        }
        }catch(Exception exc){
            system.debug(exc.getMessage());
        }
    }
    
//Method to update Applicaction with Travel Data From GL
    public Application__c processServiceAndOA(Application__c applic){
        Application__c app = new Application__c(Id=applic.Id);
        app.BV_Record_Locator__c = remoteSiteModel.serviceAndOAList[0].BV_RECORD_LOCATOR;
        app.BV_Airline__c = remoteSiteModel.serviceAndOAList[0].BV_AIRLINE;
        app.BV_Flight_Number__c = remoteSiteModel.serviceAndOAList[0].BV_FLIGHT_NUMBER;
        //app.BV_Departure_Time__c = remoteSiteModel.serviceAndOAList[0].BV_DEPARTURE_TIME;
        //app.BV_Arrival_Time__c = remoteSiteModel.serviceAndOAList[0].BV_ARRIVAL_TIME;        app.BV_Departure_Airport__c = remoteSiteModel.serviceAndOAList[0].BV_DEPARTURE_AIRPORT;
        app.BV_Arrival_Airport__c = remoteSiteModel.serviceAndOAList[0].BV_ARRIVAL_AIRPORT;
        app.BV_Domestic_Travel_Arrangements__c = remoteSiteModel.serviceAndOAList[0].BV_DOMESTIC_TRAVEL_ARRANGEMENTS;
        //app.BV_Arrival_to_gateway_city_with_who__c = remoteSiteModel.serviceAndOAList[0].BV_ARRIVAL_TO_GATEWAY_CITY_WITH_WHO;
        app.BV_Gateway_City__c = remoteSiteModel.serviceAndOAList[0].BV_GATEWAY_CITY;
        app.BV_Arrive_in_prior_to_the_start__c = remoteSiteModel.serviceAndOAList[0].BV_ARRIVE_IN_PRIOR_TO_THE_START;
        app.BV_Additional_Comments__c = remoteSiteModel.serviceAndOAList[0].BV_ADDITIONAL_COMMENTS;
        app.BV_Contact_First_Name__c = remoteSiteModel.serviceAndOAList[0].BV_CONTACT_FIRST_NAME;
        app.BV_Contact_Last_Name__c = remoteSiteModel.serviceAndOAList[0].BV_CONTACT_LAST_NAME;
        app.BV_Contact_Phone__c = remoteSiteModel.serviceAndOAList[0].BV_CONTACT_PHONE;
        app.BV_Contact_Phone__c = remoteSiteModel.serviceAndOAList[0].BV_CONTACT_PHONE_TYPE;
        app.BV_Contact_Relationship__c = remoteSiteModel.serviceAndOAList[0].BV_CONTACT_RELATIONSHIP;
        app.CBH_Record_Locator__c = remoteSiteModel.serviceAndOAList[0].CBH_RECORD_LOCATOR;
        app.CBH_Airline__c = remoteSiteModel.serviceAndOAList[0].CBH_AIRLINE;
        app.CBH_Flight_Number__c = remoteSiteModel.serviceAndOAList[0].CBH_FLIGHT_NUMBER;
        //app.CBH_Departure_Time__c = remoteSiteModel.serviceAndOAList[0].CBH_DEPARTURE_TIME;
        //app.CBH_Arrival_Time__c = remoteSiteModel.serviceAndOAList[0].CBH_ARRIVAL_TIME;
        app.CBH_Departure_Airport__c = remoteSiteModel.serviceAndOAList[0].CBH_DEPARTURE_AIRPORT;
        app.CBH_Arrival_Airport__c = remoteSiteModel.serviceAndOAList[0].CBH_ARRIVAL_AIRPORT;
        app.CBH_Domestic_Travel_Arrangements__c = remoteSiteModel.serviceAndOAList[0].CBH_DOMESTIC_TRAVEL_ARRANGEMENTS;
        app.CBH_Arrival_to_gateway_city_with_who__c = remoteSiteModel.serviceAndOAList[0].CBH_ARRIVAL_TO_GATEWAY_CITY_WITH_WHO;
        app.CBH_Gateway_City__c = remoteSiteModel.serviceAndOAList[0].CBH_GATEWAY_CITY;
        app.CBH_Arrive_in_prior_to_the_start__c = remoteSiteModel.serviceAndOAList[0].CBH_ARRIVE_IN_PRIOR_TO_THE_START;
        app.CBH_Additional_Comments__c = remoteSiteModel.serviceAndOAList[0].CBH_ADDITIONAL_COMMENTS;
        app.CBH_Contact_First_Name__c = remoteSiteModel.serviceAndOAList[0].CBH_CONTACT_FIRST_NAME;
        app.CBH_Contact_Last_Name__c = remoteSiteModel.serviceAndOAList[0].CBH_CONTACT_LAST_NAME;
        app.CBH_Contact_Phone__c = remoteSiteModel.serviceAndOAList[0].CBH_CONTACT_PHONE;
        app.CBH_Contact_Phone__c = remoteSiteModel.serviceAndOAList[0].CBH_CONTACT_PHONE_TYPE;
        app.CBH_Contact_Relationship__c = remoteSiteModel.serviceAndOAList[0].CBH_CONTACT_RELATIONSHIP;
		app.Get_Travel_Data_From_Global_Link__c = false;
        
        return app;
    }
    
    //YYYYMMDD String to Date
    public Date retrieveDate(String strDate){
        Date finalDate;
        
        if(strDate != null && strDate.length() == 8){
            Integer year = Integer.valueOf(strDate.left(4));
            Integer month = Integer.valueOf(strDate.substring(4, 5));
            Integer day = Integer.valueOf(strDate.right(2));
            finalDate = Date.newInstance(year, month, day);
        }        
        
        Return finalDate;   
    }
    
    /*
     * Method to modified the language to expected value
	*/    
    public static String retrieveLanguages(String progLanguage){
        String finalLanguage;
        if(progLanguage != null){
            String auxStr;
			List<String> lstFinalLanguage = new List<String>();            
            if(progLanguage.contains('Swiss German')){
                auxStr = progLanguage.remove('Swiss German').normalizeSpace();
                lstFinalLanguage.add('Swiss German');
            }else{
                auxStr = progLanguage.normalizeSpace();
            }
            lstFinalLanguage.addAll(auxStr.split(' '));
            finalLanguage = String.join(lstFinalLanguage,';');
        }
        return finalLanguage;
    }
}