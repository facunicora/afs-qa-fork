@IsTest
private class MasterToDoItemTriggerTest {
    @TestSetup public static void testSetup(){
        
    }
    @IsTest static void insertUpdateTest(){
        Account acc = Util_Test.create_AFS_Partner();
        Scholarship__c sch =  Util_Test.create_Scholarship(acc);
        Master_To_Do_Template__c toDo = new Master_To_Do_Template__c(Name = 'Test', To_use_on__c = 'Program Offer', 
                                                                     Type__c = 'Standard for Portal');
        insert todo;
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c prog = Util_Test.create_Program_Offer(acc, hp);
        
        Test.startTest();
        Master_To_Do__c master = new Master_To_Do__c();
        master.name = 'Test Master To Do';
        master.Scholarship__c = sch.Id;
        master.Program_Offer__c = prog.Id;
        master.Template__c = toDo.Id;
        master.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Standard' AND SobjectType = 'Master_To_Do__c' LIMIT 1].Id;
        insert master;
        
        master.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Standard for Program Offer' AND SobjectType = 'Master_To_Do__c' LIMIT 1].Id;
        update master;
        Test.stopTest();
    }
}