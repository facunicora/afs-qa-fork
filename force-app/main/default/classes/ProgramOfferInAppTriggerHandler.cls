public class ProgramOfferInAppTriggerHandler {
    public static void afterInsert(List<Program_Offer_in_App__c> lstNew){
        recalculateAppScore(lstNew,null,false,false);
        populateProgramEligibilityStatusOfApplication(lstNew,null,false,false);
    }
    
    public static void afterUpdate(Map<Id,Program_Offer_in_App__c> mapNew, Map<Id,Program_Offer_in_App__c> mapOld){
        recalculateAppScore(mapNew.values(),mapOld,true,false);
        populateProgramEligibilityStatusOfApplication(mapNew.values(),mapOld,true,false);
    }
    
    public static void afterDelete(Map<Id,Program_Offer_in_App__c> mapOld){
        recalculateAppScore(null,mapOld,false,true);
        populateProgramEligibilityStatusOfApplication(null,mapOld,false,true);
    }
    
    public static List<Application__c> recalculateAppScore(List<Program_Offer_in_App__c> lstNew, Map<Id,Program_Offer_in_App__c> mapOld, Boolean isUpdate, Boolean isDelete){
        Set<Id> setAppIds = new Set<Id>();
        List<Application__c> lstApplications = new List<Application__c>();
        List<Program_Offer_in_App__c> lstAppToLoop = !isDelete ? lstNew : mapOld.Values();
        
        for(Program_Offer_in_App__c poa: lstAppToLoop){
            if(!isUpdate || poa.Application__c !=  mapOld.get(poa.Id).Application__c){
                setAppIds.add(poa.Application__c);
            }
        }
        
        if(!setAppIds.isEmpty()){
            List<String> lstApplicationFields = ScoringHandler.getFieldsOnScoringSettingByObjectName('Application__c');
            if(!lstApplicationFields.isEmpty()){
                String queryApplications = 'SELECT Id, ' + String.join(lstApplicationFields,',') + ' FROM Application__c WHERE Id in :setAppIds';
                lstApplications = Database.query(queryApplications);
                
                List<Scoring_Settings__c> lstApplicationScoringSettings = ScoringHandler.getListScoringSettingByObject('Application__c'); 
                for(Application__c app : lstApplications){
                    app.Score__c = ScoringHandler.recalculateScore(app, lstApplicationScoringSettings,100,'Application__c',null);
                }
                
                Database.Update(lstApplications);
            }            
            
        }
        
        return lstApplications;
    }
    
    public static void populateProgramEligibilityStatusOfApplication(List<Program_Offer_in_App__c> lstNew, Map<Id,Program_Offer_in_App__c> mapOld, Boolean isUpdate, Boolean isDelete){
        Set<Id> setAppIds = new Set<Id>();
        List<Application__c> lstApplications = new List<Application__c>();
        List<Program_Offer_in_App__c> lstAppToLoop = !isDelete ? lstNew : mapOld.Values();
        
        for(Program_Offer_in_App__c poa: lstAppToLoop){
            if(!isUpdate || poa.Is_the_applicant_Eligible__c != mapOld.get(poa.Id).Is_the_applicant_Eligible__c || poa.Application__c != mapOld.get(poa.Id).Application__c){
                setAppIds.add(poa.Application__c);
            }
        }
        
        for(Application__c app : [SELECT Id, Program_Eligibility_Status__c ,(SELECT Id, Is_the_applicant_Eligible__c FROM Interests__r) FROM Application__c WHERE Id IN :setAppIds]){
            Integer countIneligible = 0;
            Boolean eligible = false;
            Boolean notDecided = false;
            
            for(Program_Offer_in_App__c poa : app.Interests__r){
                if(poa.Is_the_applicant_Eligible__c == null){
                    notDecided = true;
                    Break;
                }else if(poa.Is_the_applicant_Eligible__c == 'Eligible'){
                    eligible = true;
                }else if(poa.Is_the_applicant_Eligible__c == 'Ineligible'){
                    countIneligible++;
                }
            }
            
            if(notDecided && app.Program_Eligibility_Status__c != 'Not decided'){
                app.Program_Eligibility_Status__c = 'Not decided';
                lstApplications.add(app);
            }else if(!notDecided && eligible && app.Program_Eligibility_Status__c != 'Eligible'){
            	app.Program_Eligibility_Status__c = 'Eligible';
                lstApplications.add(app);
            }else if(!notDecided &&  app.Interests__r.size() == countIneligible && app.Program_Eligibility_Status__c != 'Ineligible'){
                app.Program_Eligibility_Status__c = 'Ineligible';
                lstApplications.add(app);
            }
        }
        
        if(!lstApplications.isEmpty()){
        	List<Database.SaveResult> lstSR = Database.Update(lstApplications,false);
            for(Integer i = 0;i < lstSR.Size(); i++){                
            Database.SaveResult sr = lstSR[i];
            if(!sr.isSuccess()){
                system.debug(logginglevel.error,lstApplications[i] + ' --> ' + String.join(sr.getErrors(),' / '));
            }
        } 
        }
    }
}