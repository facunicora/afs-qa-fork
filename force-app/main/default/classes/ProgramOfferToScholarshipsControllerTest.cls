@IsTest
private class ProgramOfferToScholarshipsControllerTest {
	@TestSetup
    public static void Test_Setup(){
        Account acc = Util_Test.create_HouseHoldAcc();
        Scholarship__c scho = Util_test.create_Scholarship(acc);
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        List<Program_Offer__c> pos = new List<Program_Offer__c>();
        pos.add(new Program_Offer__c(
            name = 'Test Program Offer',
            Hosting_Program__c = hp.Id,
            Sending_Partner__c = acc.Id,
            Applications_Received_To_local__c = System.today() + 30,
            RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']));
        
        acc = Util_Test.create_HouseHoldAcc();
        scho = Util_test.create_Scholarship(acc);
        hp = Util_Test.create_Hosting_Program(acc);
        pos.add(new Program_Offer__c(
            name = 'Test Program Offer',
            Hosting_Program__c = hp.Id,
            Sending_Partner__c = acc.Id,
            Applications_Received_To_local__c = System.today() + 30,
            RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']));
        insert pos;
        
        Util_Test.create_ScholarshipProgram(scho, pos[0]);
        Util_Test.create_ScholarshipProgram(scho, pos[1]);
    }
    @IsTest public static void insertJunctionRecordsException(){
		List<Sch_in_Program__c> schList = [SELECT Id, Program_Offer__c, Scholarship__c FROM Sch_in_Program__c];
        ProgramOfferToScholarshipsController c = new ProgramOfferToScholarshipsController(new ApexPages.StandardSetController(schList));
        Test.startTest();
        PageReference ret = c.goBack();
        c.massAssignScholarships();
        c.addMessage('Test', 'INFO');
        
        c.dummyScholToProgramOffer = [SELECT Id, Program_Offer__c, Scholarship__c FROM Sch_in_Program__c LIMIT 1];
        c.massAssignScholarships();
        
        c.cont.setSelected(schList);
        c.massAssignScholarships();
		Test.stopTest();        
    }
    @IsTest public static void insertJunctionRecords(){
        List<Sch_in_Program__c> schList = [SELECT Id, Program_Offer__c, Scholarship__c FROM Sch_in_Program__c];
        ProgramOfferToScholarshipsController c = new ProgramOfferToScholarshipsController(new ApexPages.StandardSetController(schList));
        c.dummyScholToProgramOffer = [SELECT Id, Program_Offer__c, Scholarship__c FROM Sch_in_Program__c LIMIT 1];
        c.cont.setSelected([SELECT Id FROM Program_Offer__c]);
        
        Test.startTest();
        c.massAssignScholarships();
        Test.stopTest(); 
    }
}