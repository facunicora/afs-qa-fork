global without sharing class Afs_SavePersonalInformation {
  
    @AuraEnabled
    global static void savePersonalInfo (Contact contactInfo){
        try{
           
          update contactInfo; 
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
        }
    }
    
    @AuraEnabled
    global static map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> getPicklistValues(String objObject, string fld) {
        system.debug(' --- >' + fld);
        list<String> fieldAPINames = fld.split(',');
        system.debug(' --- >' + fieldAPINames);
        map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> mapFieldPicklistVal = new map<String,list<Afs_UtilityApex.PicklistWrapperUtil>>();
        
        for(String fieldAPIName : fieldAPINames){
            list<Afs_UtilityApex.PicklistWrapperUtil> picklistValues = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            picklistValues.addAll(Afs_UtilityApex.getPicklistWrapper(objObject, fieldAPIName.trim()));
            picklistValues.sort();
            mapFieldPicklistVal.put(fieldAPIName.trim(), picklistValues);
        }
        
        return mapFieldPicklistVal;
    }
    @AuraEnabled
    global static map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> getPicklistValuesNoSort(String objObject, string fld){
        system.debug(' --- >' + fld);
        list<String> fieldAPINames = fld.split(',');
        system.debug(' --- >' + fieldAPINames);
        map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> mapFieldPicklistVal = new map<String,list<Afs_UtilityApex.PicklistWrapperUtil>>();
        
        for(String fieldAPIName : fieldAPINames){
            list<Afs_UtilityApex.PicklistWrapperUtil> picklistValues = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            picklistValues.addAll(Afs_UtilityApex.getPicklistWrapper(objObject, fieldAPIName.trim()));
            mapFieldPicklistVal.put(fieldAPIName.trim(), picklistValues);
        }
        
        return mapFieldPicklistVal;
    }
    
    @AuraEnabled
    global static list<schoolWrapper> getSchools() {
        String communityId =  Network.getNetworkId();
        String sendingPartner;
        String queryStr = 'SELECT id, Name, Afs_SendingPartner__c';
        if(!Test.isRunningTest()){
            queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
        }else{
            queryStr += ' FROM AFS_Community_Configuration__c';
        }
        list<AFS_Community_Configuration__c>  listCommunityConfig;
        if(String.isNotBlank(communityId)){
            
            listCommunityConfig  =  Database.query(queryStr);
            sendingPartner = listCommunityConfig[0].Afs_SendingPartner__c;
        }
        list<schoolWrapper> wrapperList = new list<schoolWrapper>();
        wrapperList.add(new schoolWrapper(null));
        Id recordTypeId = [SELECT id,Name FROM RecordType WHERE SObjectType = 'Account' AND Name= 'School'].id;
        for(Account acc : [SELECT id,Name,BillingCity,BillingPostalCode FROM Account WHERE RecordTypeId =: recordTypeId AND Sending_Partner__c =: sendingPartner ORDER BY Name]){
            wrapperList.add(new schoolWrapper(acc));
        }
        return wrapperList;
    }
    
    global class schoolWrapper {
        @AuraEnabled
        global String schoolName {get;set;} 
        @AuraEnabled
        global String schoolId {get;set;}
        @AuraEnabled
        global String schoolCity {get;set;}
        @AuraEnabled
        global String schoolZip {get;set;}
        
        global schoolWrapper(Account schoolObj){
            schoolName = (schoolObj != null ? schoolObj.Name : System.label.AfsLbl_General_None);
            schoolId = (schoolObj != null ? String.valueOf(schoolObj.id) : '');
            schoolCity = (schoolObj != null ? schoolObj.BillingCity : System.label.AfsLbl_General_None);
            schoolZip = (schoolObj != null ? schoolObj.BillingPostalCode : System.label.AfsLbl_General_None);
        }
    }
    
    @AuraEnabled
    global static void saveCompleteProfile (Contact contactInfo,application__c applicationObj,String schoolAppObj, Account school, string parentGuardianWrapperJSON){
       system.debug(schoolAppObj);
        try{
            String communityId =  Network.getNetworkId();
            list<Account> insertAccount = new list<Account>();
            String queryStr = 'SELECT id, Name, CRMUserOwner__c';
            if(!Test.isRunningTest()){
                queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
            }else{
                queryStr += ' FROM AFS_Community_Configuration__c';
            }
            list<AFS_Community_Configuration__c>  listCommunityConfig;
            if(String.isNotBlank(communityId) || Test.isRunningTest()){
                
               listCommunityConfig  =  Database.query(queryStr);
                  
            }
            // Create account of type house hold
            Id recordTypeId = [SELECT id,Name FROM RecordType WHERE SObjectType = 'Account' AND Name= 'Household Account'].id;
                        
            List<Afs_ParentGuardianCtrl.ParentGuardianWrapper>  parentGuardianWrapperList = (List<Afs_ParentGuardianCtrl.ParentGuardianWrapper>)JSON.deserialize(parentGuardianWrapperJSON, List<Afs_ParentGuardianCtrl.ParentGuardianWrapper>.class);
            map<integer,npe4__Relationship__c>  localIdToRelation = new map<integer,npe4__Relationship__c>();
            map<integer,contact>   localIdToContact = new  map<integer,contact> ();
            List<npe4__Relationship__c> relationListToInsert = new List<npe4__Relationship__c>();
            integer localID = 0 ; 
            //map <integer,Account> mapIndexVsAccount = new map <integer,Account> ();
            RecordType rcdType =  [Select Id From RecordType Where DeveloperName = 'General']; 
            /*for(Afs_ParentGuardianCtrl.ParentGuardianWrapper parentRec : parentGuardianWrapperList){
                Account accs = new Account();
                accs.RecordTypeId = recordTypeId;
                if(parentRec.relatedContact.AccountId == null){
                   if(String.isNotBlank(parentRec.relatedContact.LastName))
                   {
                       if(String.isNotBlank(parentRec.relatedContact.FirstName))
                       {
                           accs.Name = parentRec.relatedContact.FirstName + System.label.AfsLbl_General_Account;
                       }else{
                           accs.Name = parentRec.relatedContact.LastName + System.label.AfsLbl_General_Account;
                       }                    
                   } 
                   mapIndexVsAccount.put(localID,accs);
                }
                localID ++ ;
            }
                   
            if(!mapIndexVsAccount.isEmpty()){
               insert mapIndexVsAccount.values();
            }   */    
            localID = 0 ;
            for(Afs_ParentGuardianCtrl.ParentGuardianWrapper parentRec : parentGuardianWrapperList){
                parentRec.relatedContact.RecordtypeId  = rcdType.Id;
                if(!listCommunityConfig.isEmpty()){
                    parentRec.relatedContact.OwnerId  = listCommunityConfig[0].CRMUserOwner__c;
                }
                //if(mapIndexVsAccount.containsKey(localID)){
                    parentRec.relatedContact.AccountId = contactInfo.AccountId;
                //}
                if(String.isNotBlank(parentRec.relatedContact.LastName)){
                    localIdToRelation.put(localID,parentRec.relation);
                  localIdToContact.put(localID,parentRec.relatedContact);
                }
                
                localID ++ ;
            }
            
            if(localIdToContact.values().size() > 0){
                upsert localIdToContact.values();
            }
            
            for( integer localIdKey : localIdToContact.keyset()){
                string relatedContactID  = localIdToContact.get(localIdKey).Id;
                npe4__Relationship__c relationRec =localIdToRelation.get(localIdKey);
                relationRec.npe4__RelatedContact__c  = relatedContactID;
                relationListToInsert.add(relationRec);
            }
            
            if(relationListToInsert.size() > 0){
                upsert relationListToInsert;
            }
            
           
            if(applicationObj != null){
                // To set blank value of Religious_affiliation__c
                // because od restricted picklist
                if(applicationObj.Religious_affiliation__c == '-None-'){
                    applicationObj.Religious_affiliation__c = null;
                }
                
              update applicationObj;
            }
            
            system.debug(school);
          if(school.Name != null) {
              //create School if doesn't exist in SF
              String schoolId = getSchoolIdwithValidation(school, contactInfo.OwnerId);
              system.debug(schoolId);
              //create school/contact object and save
        Applicant_School_Association__c schoolApplicant = parse(schoolAppObj);
        schoolApplicant.School__c = schoolId;
              //if(schoolApplicant.Applicant__c == null) {
        //  schoolApplicant.Applicant__c = contactInfo.Id;
              //}
              //add school rel to contactInfo
              upsert schoolApplicant;
        //add school/app object to applicantionObj
              applicationObj.Applicant_School_Association__c = schoolApplicant.Id;
              update applicationObj;
            }
            
            
            if(contactInfo != null){
               if(contactInfo.LGBTQ_Member__c == 'Not Selected'){
                    contactInfo.LGBTQ_Member__c = null;
                }
                 if(contactInfo.Ethnicity__c == 'Not Selected'){
                    contactInfo.Ethnicity__c = null;
                }
                update contactInfo;
                
                if(applicationObj != null){
                    List<To_Do_Item__c> itemList = [Select Id, Status__c from To_Do_Item__c where Application__c =:applicationObj.Id and COMPLETE_YOUR_PROFILE_BY__c = true];
                    
                    if(itemList.size() > 0){
                        itemList.get(0).Status__c = 'In Review';
                        update itemList;
                    }
                }
                
            }
           
        }catch(Exception e){
            System.debug('e.getMessage(): ' + e.getMessage());
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
        }
    }
    
    global static String getSchoolIdwithValidation(Account school, String ownerId) {
        if(school.Id == null) {
               
              school.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('School').getRecordTypeId();
                 school.OwnerId = ownerId;
                //validate School to not be already in DB
                Double radio = 0.001;
                Double latitudePlusFifty = school.BillingLatitude + radio;
                Double latitudeLessFifty = school.BillingLatitude - radio;
                Double longitudePlusFity = school.BillingLongitude + radio;
                Double longitudeLessFity = school.BillingLongitude - radio;
                List<Account> accs = [SELECT Id, Name, GMPlace_Id__c FROM Account WHERE BillingLatitude <= :latitudePlusFifty AND BillingLatitude >= :latitudeLessFifty AND BillingLongitude <= :longitudePlusFity AND BillingLongitude >= : longitudeLessFity];
                if(accs.size() > 0) {
                  List<List<SObject>> similarAcc = [FIND :school.Name IN NAME FIELDS RETURNING Account(Id, Name WHERE RecordType.Name = 'School') ];
                     List<Account> schools = ((List<Account>) similarAcc[0]);
                    if(schools.size() > 0) {
                      for (Account acc : accs) {
                          for(Account sch : schools) { 
                            if(acc.Id == sch.Id) {
                              school.Id = sch.Id;
                            }
                          }
                      } 
                    }
                       
                }
                if(school.Id == null){
          upsert school;
                }     
            }
            
            return school.Id;
    }
    
    @AuraEnabled
    global static Applicant_School_Association__c getSchoolApplication(String applicantId, String schoolId) {
      Applicant_School_Association__c applicantSchool;
      try {
      
      if(schoolId.length() > 0) {
        applicantSchool = [SELECT Id, Grade_Average__c, School__c, Graduation_Date__c, Applicant__c FROM Applicant_School_Association__c WHERE Id = :schoolId];
      } else {
        applicantSchool = new Applicant_School_Association__c(Applicant__c=applicantId);
      }
      
      } catch (Exception e){ 
        System.debug('e.getMessage(): ' + e.getMessage());
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
      }
      return applicantSchool;
    }
    
  
	@AuraEnabled
    global static List<String> getStatesPreFill(Id afsCommunityId){
        List<String> states = new List<String>();
        for(State_to_Portal__c state :[SELECT Name FROM State_to_Portal__c 
                                       WHERE AFS_Community_Configuration__c =: afsCommunityId AND Active__c = true
                                       ORDER BY Name]){
            states.add(state.name);
        }
        return states;
    }
  

  
  public static Applicant_School_Association__c parse(String json) {
    Applicant_School_Association__c resp = (Applicant_School_Association__c) System.JSON.deserialize(json, Applicant_School_Association__c.class); 
    return resp;
  }
}