@isTest(SeeAllData = false)
public class Afs_UtilityApex_Test {
	@isTest
    static void getPicklistValTest() { 
        Test.startTest();
            system.assertEquals(true,Afs_UtilityApex.getPicklistVal('AFS_Community_Configuration__c'  , 'LoginComp_Fld_Facebook__c') != null);
        Test.stopTest();
    }
    
    @isTest
    static void getPicklistValMapTest() { 
        Test.startTest();
            system.assertEquals(true,Afs_UtilityApex.getPicklistValMap('AFS_Community_Configuration__c'  , 'LoginComp_Fld_Facebook__c') != null);
        Test.stopTest();
    }
    
    @isTest
    static void getPicklistWrapperTest() { 
        Test.startTest();
            system.assertEquals(true,Afs_UtilityApex.getPicklistWrapper('AFS_Community_Configuration__c'  , 'LoginComp_Fld_Facebook__c') != null);
        Test.stopTest();
    }
    
    @isTest
    static void getFieldsTest() { 
        Test.startTest();
            system.assertEquals(true,Afs_UtilityApex.getFields('AFS_Community_Configuration__c'  , true) != null);
        	Afs_UtilityApex.logger('Test');
        Test.stopTest();
    }
    
    @isTest
    static void getAccountCountryLabel(){
        Afs_UtilityApex.getAccountCountryLabel();
    }
    
}