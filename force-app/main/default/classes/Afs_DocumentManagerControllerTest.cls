@isTest(SeeAllData=false)
public class Afs_DocumentManagerControllerTest {
    @testSetup
    public static void setup_test(){
       /*Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Afs_TestDataFactory.createProfilePhoto(1);*/
    }
    
    public static testMethod void getDownloadFormTest(){        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        List<ContentVersion>  contentVersionList  = [SELECT id,VersionData,ContentDocumentId,FileType,Title  FROM ContentVersion] ; 
        Afs_DocumentManagerController.getDownloadForm(contentVersionList[0].ContentDocumentId);
        System.assertEquals(true,contentVersionList != null);
        
        Test.stopTest(); 
    }
    
    public static testMethod void getFilesTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1); 
        List<Contact> ContactList = [SELECT id FROM Contact] ; 
        Afs_DocumentManagerController.getFiles(ContactList[0].id);
        System.assertEquals(true,ContactList != null);
        Test.stopTest(); 
    }
    
    public static testmethod void completeDueDateTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        To_Do_Item__c tdi = new To_Do_Item__c(When__c = 'After Entering Stage of Portal', 
                                              Days__c=5,
                                              application__c = [SELECT id FROM Application__c LIMIT 1].Id);
        insert tdi;
        
        Afs_DocumentManagerController.completeDueDate(new list<To_Do_Item__c>{tdi});
        test.stopTest();
    }
    
    public static testMethod void deleteFileSFTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        List<Application__c> application = [SELECT id FROM Application__c] ; 
        
        List<ContentVersion> ContactList = [SELECT id,VersionData,ContentDocumentId,FileType,Title  FROM ContentVersion] ; 
        
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Filling Field'; 
        obj.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        obj.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY__c = true;
        obj.File_link__c = ContactList[0].ContentDocumentId;
        insert obj;
        
        Afs_DocumentManagerController.deleteFileSF(ContactList[0].ContentDocumentId,obj);
        System.assertEquals(true,ContactList != null);
        Test.stopTest(); 
    }
    
    public static testMethod void updateParentTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1); 
        List<Application__c> application = [SELECT id FROM Application__c] ; 
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Filling Field'; 
        obj.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        insert obj;
        
        Afs_DocumentManagerController.updateParent(obj);
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void completeTaskTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1); 
        List<Application__c> application = [SELECT id FROM Application__c] ; 
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Filling Field'; 
        obj.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        obj.SUBMIT_YOUR_ACADEMIC_HISTORY__c = true;
        insert obj;
        
        Afs_DocumentManagerController.completeTask(obj);
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void getDocumentListBlock1Test(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1); 
        List<Application__c> application = [SELECT id,Program_Offer__c,Status__c,Status_App__c FROM Application__c] ; 
        Program_Offer__c prOffer = [SELECT Id FROM Program_Offer__c LIMIT 1];
        application[0].Program_Offer__c = prOffer.id;
        application[0].Status__c = 'Decision';
        update application;
        
        Id RecordTypeIdStr = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_for_Portal' AND SObjectType = 'To_Do_Item__c' LIMIT 1].id;
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Task'; 
        obj.RecordTypeId = RecordTypeIdStr;
        obj.Stage_of_portal__c = '1. Stage: Pre-application'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        obj.SUBMIT_YOUR_HEALTH_UPDATE_FORM__c = true;
        insert obj;
        
        Afs_DocumentManagerController.getDocumentList(application[0].id,'');
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void getDocumentListBlock2Test(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1); 
        List<Application__c> application = [SELECT id,Program_Offer__c,Status__c,Status_App__c FROM Application__c] ; 
        Program_Offer__c prOffer = [SELECT Id FROM Program_Offer__c LIMIT 1];
        application[0].Program_Offer__c = prOffer.id;
        application[0].Status__c = 'Confirmation';
        update application;
        
        Id RecordTypeIdStr = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_for_Portal' AND SObjectType = 'To_Do_Item__c' LIMIT 1].id;
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Task'; 
        obj.RecordTypeId = RecordTypeIdStr;
        obj.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        obj.SIGN_PARTICIPATION_AGREEMENT_BY__c = true;
        insert obj;
        
        Afs_DocumentManagerController.getDocumentList(application[0].id,'');
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void getDocumentListBlock2ExtraTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1); 
        List<Application__c> application = [SELECT id,Program_Offer__c,Status__c,Status_App__c FROM Application__c] ; 
        Program_Offer__c prOffer = [SELECT Id FROM Program_Offer__c LIMIT 1];
        application[0].Program_Offer__c = prOffer.id;
        application[0].Status__c = 'Confirmation';
        update application;
        
        Id RecordTypeIdStr = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_for_Portal' AND SObjectType = 'To_Do_Item__c' LIMIT 1].id;
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Task'; 
        obj.RecordTypeId = RecordTypeIdStr;
        obj.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        insert obj;
        
        Afs_DocumentManagerController.getDocumentList(application[0].id,'');
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void getDocumentListBlock21ExtraTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        List<Application__c> application = [SELECT id,Program_Offer__c,Status__c,Status_App__c FROM Application__c] ; 
        Program_Offer__c prOffer = [SELECT Id FROM Program_Offer__c LIMIT 1];
        application[0].Program_Offer__c = prOffer.id;
        application[0].Status__c = 'Decision';
        update application;
        
        Id RecordTypeIdStr = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_for_Portal' AND SObjectType = 'To_Do_Item__c' LIMIT 1].id;
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Task'; 
        obj.RecordTypeId = RecordTypeIdStr;
        obj.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        insert obj;
        
        Afs_DocumentManagerController.getDocumentList(application[0].id,'Test');
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    
    public static testMethod void getDocumentListBlock3Test(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        List<Application__c> application = [SELECT id,Program_Offer__c,Status__c,Status_App__c,Confirmed__c FROM Application__c] ; 
        Program_Offer__c prOffer = [SELECT Id FROM Program_Offer__c LIMIT 1];
        application[0].Program_Offer__c = prOffer.id;
        
        application[0].Status__c = 'Confirmation';
        application[0].Confirmed__c = true;
        update application;
        application[0].Status__c = 'Purchase';
        update application;
        
        Id RecordTypeIdStr = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_for_Portal' AND SObjectType = 'To_Do_Item__c' LIMIT 1].id;
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Task'; 
        obj.RecordTypeId = RecordTypeIdStr;
        obj.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        insert obj;
        
        Afs_DocumentManagerController.getDocumentList(application[0].id,'');
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void getDocumentListBlock3ExtraTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
          Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1); 
        List<Application__c> application = [SELECT id,Program_Offer__c,Status__c,Status_App__c,Confirmed__c FROM Application__c] ; 
        Program_Offer__c prOffer = [SELECT Id FROM Program_Offer__c LIMIT 1];
        application[0].Program_Offer__c = prOffer.id;
        
        application[0].Status__c = 'Confirmation';
        application[0].Confirmed__c = true;
        update application;
        
        application[0].Status__c = 'Purchase';
        update application;
        Id RecordTypeIdStr = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_for_Portal' AND SObjectType = 'To_Do_Item__c' LIMIT 1].id;
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Task'; 
        obj.RecordTypeId = RecordTypeIdStr;
        obj.Stage_of_portal__c = '3. Stage: Accepted'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        insert obj;
        
        Afs_DocumentManagerController.getDocumentList(application[0].id,'');
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void getDocumentListBlock4Test(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        List<Application__c> application = [SELECT id,Program_Offer__c,Status__c,Status_App__c,Confirmed__c FROM Application__c] ; 
        Program_Offer__c prOffer = [SELECT Id FROM Program_Offer__c LIMIT 1];
        application[0].Program_Offer__c = prOffer.id;       
        application[0].Status__c = 'Confirmation';
        application[0].Confirmed__c = true;
        update application;
        application[0].Status__c = 'Onboarding';
        update application;
        Id RecordTypeIdStr = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_for_Portal' AND SObjectType = 'To_Do_Item__c' LIMIT 1].id;
        
        List<To_Do_Item__c> listObj = new List<To_Do_Item__c>();      
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sbd'; 
        obj.To_Do_Label__c = 'COMPLETE YOUR TRAVEL INFORMATION BY';     
        obj.Type__c = 'Task'; 
        obj.RecordTypeId = RecordTypeIdStr;
        obj.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10);
        listObj.add(obj);
        
        To_Do_Item__c obj1 = new To_Do_Item__c();
        obj1.Name = 'sad1'; 
        obj1.To_Do_Label__c = 'INTRODUCE YOURSELF BY';       
        obj1.Type__c = 'Task'; 
        obj1.RecordTypeId = RecordTypeIdStr;
        obj1.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj1.Application__c  = application[0].id; 
        obj1.Status__c = 'Pending'; 
        obj1.Due_Date__c = Date.today().addDays(10);
        listObj.add(obj1);
        
        To_Do_Item__c obj2 = new To_Do_Item__c();
        obj2.Name = 'asd';
        obj2.To_Do_Label__c = 'COMPLETE YOUR PROFILE BY';       
        obj2.Type__c = 'Task'; 
        obj2.RecordTypeId = RecordTypeIdStr;
        obj2.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj2.Application__c  = application[0].id; 
        obj2.Status__c = 'Pending'; 
        obj2.Due_Date__c = Date.today().addDays(10);
        listObj.add(obj2);
        
        To_Do_Item__c obj3 = new To_Do_Item__c();
        obj3.Name = 'csd';
        obj3.To_Do_Label__c = 'SUBMIT YOUR HEALTH CERTIFICATE BY ';
        obj3.Type__c = 'Task'; 
        obj3.RecordTypeId = RecordTypeIdStr;
        obj3.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj3.Application__c  = application[0].id; 
        obj3.Status__c = 'Pending'; 
        obj3.Due_Date__c = Date.today().addDays(10);
        listObj.add(obj3);
        
        To_Do_Item__c obj4 = new To_Do_Item__c();
        obj4.Name = 'csd';
        obj4.To_Do_Label__c = 'SUBMIT YOUR HEALTH CERTIFICATE BY ';
        obj4.Type__c = 'Task'; 
        obj4.RecordTypeId = RecordTypeIdStr;
        obj4.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj4.Application__c  = application[0].id; 
        obj4.Status__c = 'Pending'; 
        listObj.add(obj4);
        
        To_Do_Item__c obj5 = new To_Do_Item__c();
        obj5.Name = 'csd';
        obj5.To_Do_Label__c = 'COMPLETE YOUR PROFILE BY';
        obj5.Type__c = 'Task'; 
        obj5.RecordTypeId = RecordTypeIdStr;
        obj5.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj5.Application__c  = application[0].id; 
        obj5.Status__c = 'Pending';        
        listObj.add(obj5);
        
        To_Do_Item__c obj6 = new To_Do_Item__c();
        obj6.Name = 'csd';
        obj6.To_Do_Label__c = 'SUBMIT YOUR HEALTH CERTIFICATE BY ';
        obj6.Type__c = 'Task'; 
        obj6.RecordTypeId = RecordTypeIdStr;
        obj6.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj6.Application__c  = application[0].id; 
        obj6.Status__c = 'Pending';        
        listObj.add(obj6);
        
        To_Do_Item__c obj7 = new To_Do_Item__c();
        obj7.Name = 'csd';
        obj7.To_Do_Label__c = 'SUBMIT YOUR HEALTH CERTIFICATE BY ';
        obj7.Type__c = 'Task'; 
        obj7.RecordTypeId = RecordTypeIdStr;
        obj7.Stage_of_portal__c = '4. Stage: Get Ready'; 
        obj7.Application__c  = application[0].id; 
        obj7.Status__c = 'Pending'; 
        obj7.Due_Date__c = Date.today().addDays(11);
        listObj.add(obj7);
        insert listObj;
        
        Afs_DocumentManagerController.getDocumentList(application[0].id,'');
        System.assertEquals(true,true);
        Test.stopTest(); 
    }
    
    public static testMethod void updateDocumentLinkTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);        
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        List<Application__c> application = [SELECT id FROM Application__c] ; 
        List<String> cdList = new list<String> ();
        for(ContentVersion cdObj : [SELECT id,ContentDocumentId FROM ContentVersion]){
            cdList.add(cdObj.ContentDocumentId);
        }
        
        List<To_Do_Item__c> todos = new List<To_Do_Item__c>();
        To_Do_Item__c obj1 = new To_Do_Item__c();
        obj1.Name = 'sd'; 
        obj1.Type__c = 'Filling Field'; 
        obj1.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj1.Application__c  = application[0].id; 
        obj1.Status__c = 'Pending'; 
        obj1.Due_Date__c = Date.today().addDays(10); 
        obj1.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY__c = true;
        todos.add(obj1);
        To_Do_Item__c obj2 = new To_Do_Item__c();
        obj2.Name = 'sd'; 
        obj2.Type__c = 'Filling Field'; 
        obj2.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj2.Application__c  = application[0].id; 
        obj2.Status__c = 'Pending'; 
        obj2.Due_Date__c = Date.today().addDays(10); 
        obj2.SUBMIT_YOUR_ACADEMIC_HISTORY__c = true;
        todos.add(obj2);
        To_Do_Item__c obj3 = new To_Do_Item__c();
        obj3.Name = 'sd'; 
        obj3.Type__c = 'Filling Field'; 
        obj3.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj3.Application__c  = application[0].id; 
        obj3.Status__c = 'Pending'; 
        obj3.Due_Date__c = Date.today().addDays(10); 
        obj3.SUBMIT_YOUR_HEALTH_UPDATE_FORM__c = true;
        todos.add(obj3);
        To_Do_Item__c obj4 = new To_Do_Item__c();
        obj4.Name = 'sd'; 
        obj4.Type__c = 'Filling Field'; 
        obj4.Stage_of_portal__c = '2. Stage: Pre-selected'; 
        obj4.Application__c  = application[0].id; 
        obj4.Status__c = 'Pending'; 
        obj4.Due_Date__c = Date.today().addDays(10); 
        obj4.SIGN_PARTICIPATION_AGREEMENT_BY__c = true;
        todos.add(obj4);
        insert toDos;
        
        ContentDocumentLink cdl = new ContentDocumentLink(LinkedEntityId = application[0].Id,
                                       ContentDocumentId = [SELECT id FROM ContentDocument LIMIT 1].Id);
        insert cdl;
        
        Afs_DocumentManagerController.updateDocumentLink(cdList,obj1);
        System.assertEquals(true,application != null);
        Afs_DocumentManagerController.updateDocumentLink(cdList,obj2);
        Afs_DocumentManagerController.updateDocumentLink(cdList,obj3);
        Afs_DocumentManagerController.updateDocumentLink(cdList,obj4);
        Test.stopTest(); 
    }
    
}