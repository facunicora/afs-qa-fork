@IsTest
public class ScholarshipToProgramOfferControllerTest {
    @testSetup 
    public static void setup_test(){
        Account acc = new Account();
        acc.name = 'Test House Hold Account';
        acc.IOC_Code__c = [SELECT IOC_Code__c FROM User WHERE Id = :UserInfo.getUserId()].IOC_Code__c;
        acc.RecordType = [Select Id From RecordType Where DeveloperName = 'HH_Account'];
        Insert acc;
        Hosting_Program__c hp = Util_test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc, hp);
        Scholarship__c scho = Util_Test.create_Scholarship(acc);
        scho.Sending_Partner__c = acc.Id;
        update scho;
        po.Sending_Partner__c = acc.Id;
        update po;
    }
    @IsTest static public void getProgram_OffersTest(){
        Scholarship__c scho = [SELECT Id, Sending_Partner__c FROM Scholarship__c LIMIT 1];
        Program_Offer__c po = [SELECT Id FROM Program_Offer__c LIMIT 1];
        Program_Offer__c po2 = Util_Test.create_Program_Offer([SELECT Id FROM Account LIMIT 1], [SELECT Id FROM Hosting_Program__c LIMIT 1]);
        po2.Sending_Partner__c = scho.Sending_Partner__c;
        po2.Applications_Received_To_local__c = Date.today() +20;
        update po2;
        system.debug('BBBBBBBBBBBBBB: '+[SELECT id, Sending_Partner__c, Applications_Received_To_local__c, Host_IOC__c FROM Program_Offer__c WHERE id=:po2.id]);
        Util_Test.create_ScholarshipProgram(scho, po);
        List<ScholarshipToProgramOfferController.wrapperProgToOffer> listWrapper = ScholarshipToProgramOfferController.getProgram_Offers(scho.Id);
    }
    @IsTest static public void getProgram_OffersTest2(){
        Scholarship__c scho = [SELECT Id FROM Scholarship__c LIMIT 1];
        List<ScholarshipToProgramOfferController.wrapperProgToOffer> listWrapper = ScholarshipToProgramOfferController.getProgram_Offers(scho.Id);
    }
    @IsTest static public void createRecordsTest(){
        Scholarship__c scho = [SELECT Id FROM Scholarship__c LIMIT 1];
        Program_Offer__c po = [SELECT Id FROM Program_Offer__c LIMIT 1];
        Set<Id> ids = new Set<Id>();
        ids.add(po.Id);
        String strJSONIds = System.JSON.serialize(ids);
        List<Sch_in_Program__c> ret = ScholarshipToProgramOfferController.createRecords(strJSONIds, scho.Id);
    }
}