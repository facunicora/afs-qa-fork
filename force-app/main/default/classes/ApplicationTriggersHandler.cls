public class ApplicationTriggersHandler {

    public static void createInstallments() {
    
        /*List<Application__c> triggerNew = [SELECT Id, RecordType.DeveloperName, Create_Installments__c, Starting_Date__c,
                                                 Ending_Date__c, Number_of_fees__c, Total_Installments_to_be_paid__c, 
                                                 Remaining_amount_of_installments_to_crea__c, Amount_of_Installments__c, Total_Paid__c, Number_of_installments__c, PP_Type__c, Payment_Plan_Status__c
                                                FROM Application__c 
                                                WHERE Id IN: trigger.new];*/
                                                
        List<Application__c> triggerNew = (List<Application__c>)trigger.new;
        Map<Id, Application__c> triggerOldMap = (Map<Id, Application__c>)trigger.oldMap;
        
        List<Payment_Installment__c> listInstallment = new List<Payment_Installment__c>();
        
        for (Application__c myApplication : triggerNew) {
        
            //if ((trigger.isInsert && myApplication.Create_Installments__c == true || trigger.isUpdate && triggerOldMap.get(myApplication.Id).Create_Installments__c == false && myApplication.Create_Installments__c == true) && myApplication.RecordType.DeveloperName == 'Dynamic' && myApplication.Starting_Date__c != null && myApplication.Ending_Date__c != null && myApplication.Remaining_amount_of_installments_to_crea__c > 0 && (myApplication.Number_of_fees__c != null || myApplication.Number_of_installments__c != null)) {
    
            if (myApplication.Create_Installments__c == true
               && myApplication.PP_Type__c == 'Dynamic' 
               && myApplication.Starting_Date__c != null && myApplication.Ending_Date__c != null 
               && myApplication.Remaining_amount_of_installments_to_crea__c > 0 
               && myApplication.Payment_Plan_Status__c == 'Active'
               && (myApplication.Number_of_fees__c != null || myApplication.Number_of_installments__c != null)) {    
   
                
                if (myApplication.Number_of_fees__c != null) {
                
                    Integer numberDaysDue = myApplication.Starting_Date__c.daysBetween(myApplication.Ending_Date__c);
                    Integer numberOfQuota = integer.valueof((numberDaysDue / myApplication.Number_of_fees__c).round(System.RoundingMode.FLOOR));
                    Decimal QuotaAmount = myApplication.Remaining_amount_of_installments_to_crea__c / numberOfQuota;
                    
                    for (Integer i = 1; i <= numberOfQuota; i++) {
                        Payment_Installment__c myInstallment = new Payment_Installment__c();
                        myInstallment.Application__c = myApplication.Id;
                        myInstallment.Amount__c = QuotaAmount;
                        myInstallment.Number__c = i + myApplication.Number_of_installments__c - 1;
                        myInstallment.Due_Date__c = date.today().addDays(integer.valueof(myApplication.Number_of_fees__c * i));
                        listInstallment.add(myInstallment);
                    }
                
                } else if (myApplication.Number_of_installments__c != null) {
                    
                    Integer numberDaysDue = myApplication.Starting_Date__c.daysBetween(myApplication.Ending_Date__c);
                    Integer CantidadDeDias = integer.valueof((numberDaysDue / myApplication.Number_of_installments__c).round(System.RoundingMode.FLOOR));
                    Decimal QuotaAmount = myApplication.Amount_of_Installments__c / myApplication.Number_of_installments__c;
                    
                    for (Integer i = 1; i <= myApplication.Number_of_installments__c; i++) {
                        Payment_Installment__c myInstallment = new Payment_Installment__c();
                        myInstallment.Application__c = myApplication.Id;
                        myInstallment.Amount__c = QuotaAmount;
                        myInstallment.Number__c = i + myApplication.Number_of_installments__c - 1;
                        myInstallment.Due_Date__c = date.today().addDays(integer.valueof(CantidadDeDias * i));
                        listInstallment.add(myInstallment);
                    }
                    
                }
            
            } 
            
        }
        
        if (!listInstallment.isEmpty()) {
            insert listInstallment;
        }
    
    
    }

    public static void dummy(){
        integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
                
    }

}