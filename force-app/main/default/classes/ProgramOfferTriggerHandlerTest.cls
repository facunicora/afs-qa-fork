@IsTest
public class ProgramOfferTriggerHandlerTest {
	@TestSetup
    public static void setup_method(){
        
    }
    
    public static testmethod void afterInsertUpdateTest(){
        Map<Id, Program_Offer__c> emptyMap = new Map<Id, Program_Offer__c>();
        ProgramOfferTriggerHandler.afterInsert(emptyMap, emptyMap);
        ProgramOfferTriggerHandler.afterUpdate(emptyMap, emptyMap);
    }
    
    public static testmethod void createMasterToDosTest(){
        Account acc = Util_Test.create_HouseHoldAcc();
        Hosting_Program__c hP = Util_Test.create_Hosting_Program(acc);
        Master_To_Do_Template__c toDoTemplate = Util_Test.createListMasterToDoTemplate(1)[0];
        insert toDoTemplate;
        Master_To_Do__c masterToDo = new Master_To_Do__c(Name = 'Test',
                                                         Days__c = 4,
                                                         Template__c = toDoTemplate.Id);
        insert masterToDo;
        
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.To_Do_Template__c = toDoTemplate.Id;
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']; //flagship
        Insert po;
        po.To_Do_Template__c = Util_Test.createListMasterToDoTemplate(1)[0].Id;
        update po;
        masterToDo.Program_Offer__c = po.Id;
        update masterToDo;
    }
}