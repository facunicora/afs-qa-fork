@isTest
private class Afs_LabelTranslatorTest {

    static testMethod void myUnitTest() {
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.Afs_LabelTranslator')); 
		System.currentPageReference().getParameters().put('label_lang', 'en');
        System.currentPageReference().getParameters().put('label', 'AfsLbl_LoginWith');
        Afs_LabelTranslator cls = new Afs_LabelTranslator();
        cls.translate('AfsLbl_LoginWith', 'en');
        Test.stopTest();
    }
}