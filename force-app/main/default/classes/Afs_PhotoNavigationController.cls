global without sharing class Afs_PhotoNavigationController {
    
    /**
     * Desc : Function return list of content document string 64 bit 
     *        corresponding to the current contact
     */ 
    @AuraEnabled
    global static list<String> getUploadedPhoto (){
        list<String> fileDataList = new list<String>();
        Id contactId = [SELECT ContactId FROM USER WHERE id =: UserInfo.getUserId()][0].ContactId;
        Set<id> contentDocumentIds = new Set<id> ();
        for(ContentDocumentLink cdLink : [SELECT id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: contactId]){
            contentDocumentIds.add(cdLink.ContentDocumentId);
        }
        for(ContentVersion fileObj : [SELECT id,VersionData,Afs_ProfilePhoto__c,ContentDocumentId FROM ContentVersion WHERE Afs_ProfilePhoto__c = false AND ContentDocumentId IN: contentDocumentIds ORDER BY CreatedDate LIMIT 4]){
            fileDataList.add('data:image/jpeg;base64,' + EncodingUtil.base64Encode(fileObj.VersionData));
            fileDataList.add(fileObj.ContentDocumentId);
        }
        return fileDataList;
    }
    
    /**
     * Desc : Function return content document string 64 bit 
     *        of profile photo i.e. content version whose
     *        profile photo checkbox is checked
     *        corresponding to the current contact
     */
    @AuraEnabled
    global static list<String> getProfilePhoto (){
        list<String> fileDataList = new list<String>();
        Id contactId = [SELECT ContactId FROM USER WHERE id =: UserInfo.getUserId()][0].ContactId;
        Set<id> contentDocumentIds = new Set<id> ();
        for(ContentDocumentLink cdLink : [SELECT id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: contactId]){
            contentDocumentIds.add(cdLink.ContentDocumentId);
        }
        list<ContentVersion> fileList = [SELECT id,VersionData,Afs_ProfilePhoto__c,ContentDocumentId FROM ContentVersion WHERE Afs_ProfilePhoto__c = true AND ContentDocumentId IN: contentDocumentIds ORDER BY CreatedDate LIMIT 1];
        fileDataList.add('NoFile');
        if(!fileList.isEmpty()){
            fileDataList.clear();
            fileDataList.add(fileList[0].ContentDocumentId);
            fileDataList.add('data:image/jpeg;base64,' + EncodingUtil.base64Encode(fileList[0].VersionData));
        }
        return fileDataList;
    }
    
    /**
     * Desc : Function return base64 encoded string used 
     *        to display image at the time of upload
     */
    @AuraEnabled
    global static list<String> getEncodedStringList (list<string> fileId, Boolean isProfilePhoto){
        list<String> fileDataList = new list<String>();
        try{            
            for(ContentVersion fileObj : [SELECT id,VersionData,Afs_ProfilePhoto__c,ContentDocumentId FROM ContentVersion WHERE Afs_ProfilePhoto__c = false AND ContentDocumentId IN: fileId ORDER BY CreatedDate DESC LIMIT 4]){
                fileDataList.add('data:image/jpeg;base64,' + EncodingUtil.base64Encode(fileObj.VersionData));
                fileDataList.add(fileObj.ContentDocumentId);
            }
        }catch(Exception ex){
            system.debug(ex + '--' + ex.getstacktracestring());
        }
        return fileDataList;
    }
    
    /**
     * Desc : Function return base64 encoded string used 
     *        to display image at the time of upload
     */
    @AuraEnabled
    global static String getEncodedString (string fileId, Boolean isProfilePhoto){
        ContentVersion file = [SELECT id,VersionData,Afs_ProfilePhoto__c FROM ContentVersion WHERE ContentDocumentId =: fileId];
        
        if(isProfilePhoto){
            file.Afs_ProfilePhoto__c = true;
        }
        
        update file;
        return 'data:image/jpeg;base64,' + EncodingUtil.base64Encode(file.VersionData);
    }
    
    /**
     * Desc : Function deletes the content document link
     *        of the document supplied as a parameter 
     *        for the current logged in contact
     */
    @AuraEnabled
    global static void deleteCDL (String documentId){
        
        try{
            Id contactId = [SELECT ContactId FROM USER WHERE id =: UserInfo.getUserId()][0].ContactId;
            DELETE [SELECT id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: contactId AND ContentDocumentId =: documentId];
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }
}