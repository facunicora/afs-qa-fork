@isTest(SeeAllData=false)
global class Afs_ParentComponentControllerTest {
    
	@isTest
    static void getConfigurationTest() {  
        Test.startTest();
             system.assertEquals(new AFS_Community_Configuration__c(),Afs_ParentComponentController.getConfiguration());
        Test.stopTest();
    }
    
    @isTest
    static void getNavigateWrapperTest() {  
        Test.startTest();
             system.assertEquals(true,Afs_ParentComponentController.getNavigateWrapper() != null);
        Test.stopTest();
    }
    
    @isTest
    static void getLoginWrapperTest() {  
        Test.startTest();
             system.assertEquals(true,Afs_ParentComponentController.getLoginWrapper() != null);
        Test.stopTest();
    }
    
    @isTest
    static void getNewLoginWrapperTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        	Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                system.assertEquals(true,Afs_ParentComponentController.getNewLoginWrapper() != null);
            }
        Test.stopTest();
    }
    
    @isTest
    static void updateUserAndContactTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        	Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id,firstname,Lastname,Email,BirthDate FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                
            
        		system.assertEquals(true,Afs_ParentComponentController.updateUserAndContact(con,'English') != null);
            }
            
            
        Test.stopTest();
    }
    
    @isTest
    static void updateUserAndContactExceptionTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
            
            try{
                Afs_ParentComponentController.updateUserAndContact(null,'');
            }catch(exception e){
                system.assertEquals(true,e != null);
            }

        Test.stopTest();
    }
    
    @isTest
    static void getLanguageListTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
            
            try{
                Afs_ParentComponentController.getLanguageList('ar;en_US;en_CA;en_IN;en_GB;mk;ms;es_US;es_VE;ur;vi');
            }catch(exception e){
                system.assertEquals(true,e != null);
            }

        Test.stopTest();
    }
    
    @isTest
    static void updateUserLanguageTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
            
            try{
                Afs_ParentComponentController.updateUserLanguage('English');
            }catch(exception e){
                system.assertEquals(true,e != null);
            }

        Test.stopTest();
    }
    
    @isTest
    static void getApplicationRecordTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id,firstname,Lastname,Email,BirthDate FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                
            	system.assertEquals(true,Afs_ParentComponentController.getApplicationRecord() != null);
        		
            }
        Test.stopTest();
    }
    
    @isTest
    static void generateSessionTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        	Profile p = [SELECT id FROM Profile WHERE Name = 'Applicant Community Member Profile'];
            List<Network> recCommunity = [SELECT Id, SelfRegProfileId, Name, OptionsSiteAsContainerEnabled FROM Network WHERE SelfRegProfileId =: p.id];
            NetworkSelfRegistration nsr = new NetworkSelfRegistration();
            nsr.AccountID = [SELECT id FROM Account LIMIT 1].id;
            //nsr.networkid = recCommunity[0].id;
            //insert nsr;
        	Afs_ParentComponentController.loginWrapper wrapperObj = new Afs_ParentComponentController.loginWrapper();
            wrapperObj.FirstName = 'Test';
            wrapperObj.LastName = 'AppOhm';
            wrapperObj.Email = 'user@appOhm.com';
            wrapperObj.Password = 'Welcome#12';
            wrapperObj.MobileNumber = '9929292929';
            wrapperObj.ZipCode = '312001';
            wrapperObj.AgreeTerms = true;
            wrapperObj.KeepMeInformed = true;      
            try{
                system.assertEquals(true,Afs_ParentComponentController.generateSession(JSON.serialize(wrapperObj),'sad', 'English', true) != null);
            }catch(Exception e){
               system.assertEquals(true,e != null);
            }
        Test.stopTest();
    }
    
    @isTest
    static void doTranslateTest(){
    	Test.startTest();
    		List<String> lstString = new List<String>();
    		lstString.add('AfsLbl_LoginWith');
    		Afs_ParentComponentController.doTranslate('English', JSON.serialize(lstString), false);
    		Afs_ParentComponentController.doTranslate('en', JSON.serialize(lstString), true);
    	Test.stopTest();	
    }
}