@isTest(SeeAllData=false)
public class Afs_ScholarshipApplyControllerTest {
	
    public static testMethod void getScholarshipWrapperTest (){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        
        Test.startTest();
        Afs_TestDataFactory.create_Scholarship();
        Afs_TestDataFactory.create_ScholarshipProgram();
        Afs_TestDataFactory.create_ScholarshipApplication();        
        list<Afs_ScholarshipApplyController.ScholarshipWrapper>  ScholarshipWrapperList;
        ScholarshipWrapperList = Afs_ScholarshipApplyController.getScholarshipWrapper([SELECT id FROM Account][0].Id,[SELECT id FROM Contact][0].Id,[SELECT id FROM Application__c][0].Id);
        Test.stopTest(); 
        System.assertEquals(true,ScholarshipWrapperList.size() != null);
    }
    
    public static testMethod void getScholarshipWrapperNewTest (){
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        
        Test.startTest(); 
        Afs_TestDataFactory.create_Scholarship();
        Afs_TestDataFactory.create_ScholarshipProgram();
        list<Afs_ScholarshipApplyController.ScholarshipWrapper>  ScholarshipWrapperList;    	
        ScholarshipWrapperList = Afs_ScholarshipApplyController.getScholarshipWrapper([SELECT id FROM Account][0].Id,[SELECT id FROM Contact][0].Id,[SELECT id FROM Application__c][0].Id);
        Test.stopTest(); 
        System.assertEquals(true,ScholarshipWrapperList.size() != null);
    }
    
    public static testMethod void saveScholarshipWrapperTest (){       
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);      
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        
        Test.startTest(); 
        Afs_TestDataFactory.create_Scholarship();
        Afs_TestDataFactory.create_ScholarshipProgram();
        list<Afs_ScholarshipApplyController.ScholarshipWrapper>  ScholarshipWrapperList;   	
        Afs_ScholarshipApplyController.saveScholarshipWrapper([SELECT id,Scholarship_Application_Deadline__c,Applicant_Eligibility_Specifications__c,Scholarship_Amount_Available__c FROM Scholarship__c],[SELECT id FROM Contact][0].Id,[SELECT id FROM Application__c][0].Id);
        Test.stopTest(); 
        System.assertEquals(true,[SELECT id FROM Scholarship_Application__c].size() > 0);
    }
    
    public static testMethod void saveScholarshipWrapperNewTest (){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
               
        Test.startTest();
        Afs_TestDataFactory.create_Scholarship();
        Afs_TestDataFactory.create_ScholarshipApplication();
        Afs_TestDataFactory.create_ScholarshipProgram();
        list<Afs_ScholarshipApplyController.ScholarshipWrapper>  ScholarshipWrapperList;    	 
        Afs_ScholarshipApplyController.saveScholarshipWrapper([SELECT id,Scholarship_Application_Deadline__c,Applicant_Eligibility_Specifications__c,Scholarship_Amount_Available__c FROM Scholarship__c],[SELECT id FROM Contact][0].Id,[SELECT id FROM Application__c][0].Id);
        Test.stopTest(); 
        System.assertEquals(true,[SELECT id FROM Scholarship_Application__c].size() > 0);
    }
    
    public static testMethod void exceptionTest (){
    	Test.startTest(); 
        try{
            Afs_ScholarshipApplyController.saveScholarshipWrapper(null,'abc','');   
        }catch(Exception e){
            System.assertEquals(true,e != null);
        }
           
        Test.stopTest(); 
        
    }
}