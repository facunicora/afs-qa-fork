@IsTest
global class RemoteSiteManagerMock2 implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"response":[{"statusCode":"200","pagesTotal":"1300","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}],'+
                         '"HostingFactSheets":[{"host_ioc":"Test","program_code":"Test","program_title":"Test","program_duration":"Test","program_cycle":"Test","program_content":"Test","program_type":"Test","program_description":"Test",'+
                         	'"program_language":"Test","program_year":"Test","age_range_year_start":"Test","age_range_year_end":"Test","age_range_month_start":"Test","age_range_month_end":"Test",'+
                         	'"app_received_start":"Test","app_received_end":"Test","from_departure_date":"Test","to_departure_date":"Test","from_arrival_date":"Test","to_arrival_date":"Test",'+
                         	'"from_arrival_date2":"Test","to_arrival_date2":"Test","from_arrival_date3":"Test","from_departure_date3":"Test","graduate_accept":"Test"}]}');
        response.setStatusCode(200);
        return response; 
    }
}