@isTest(SeeAllData=false)
public class Afs_TravelInfomationCtrlTest {
	@testSetup
    public static void setup_test(){
		Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Afs_TestDataFactory.createContentDocumentLinks(1);
    }
    
    public static testMethod void getPicklistValuesTest(){
        
        Test.startTest(); 
            map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> picklistValues = AfsComp_TravelInfomationCtrl.getPicklistValues('Contact','Gender__c,How_did_you_hear_about_AFS__c,Country_of_Legal_Residence__c,NationalityCitizenship__c');
        	system.assertEquals(true,picklistValues.get('NationalityCitizenship__c').Size() > 0);
        Test.stopTest(); 
         
    }
    
    public static testMethod void getProgramOfferDetailsTest(){
        
        Test.startTest(); 
            Program_Offer__c obj = [SELECT id FROM Program_Offer__c LIMIT 1];
            AfsComp_TravelInfomationCtrl.getProgramOfferDetails(obj.id);
            System.assertEquals(true,obj != null);
        Test.stopTest(); 
         
    }
    
    
    public static testMethod void updateTaskTest(){
        
        Test.startTest(); 
            Contact Con = [SELECT id FROM Contact LIMIT 1];
            List<application__c>  application  = [select Id from application__c limit 1] ;    
            To_Do_Item__c obj = new To_Do_Item__c();
            obj.Name = 'sd'; 
            obj.Type__c = 'Filling Field'; 
            obj.Stage_of_portal__c = '	1. Stage: Pre-application'; 
            obj.Application__c  = application[0].id; 
            obj.Status__c = 'Pending'; 
            obj.Due_Date__c = Date.today().addDays(10); 
            insert obj;
            AfsComp_TravelInfomationCtrl.saveTravelInfomation(Con,obj.id);
            System.assertEquals(1,application.size());
        Test.stopTest(); 
         
    }
    
    public static testMethod void deleteContentDocumentTest(){
        
        Test.startTest(); 
            AfsComp_TravelInfomationCtrl.deleteAttachment([SELECT id FROM ContentDocument][0].id);
            System.assertEquals(true,[SELECT id FROM ContentDocument].size() == 0);
        Test.stopTest(); 
         
    }
}