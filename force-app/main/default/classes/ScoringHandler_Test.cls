@isTest
public class ScoringHandler_Test {
	@testSetup
    public static void setup_method(){
        Contact con = Util_Test.createContact('Test Contact', null);
        Insert con;
        List<Scoring_Settings__c> lstSS = new List<Scoring_Settings__c>();
        Scoring_Settings__c ss = Util_Test.createScoringSettingsRecord('Test SS1', 'Contact', 'Do_you_smoke__c', 'Not Blank', null,null,null, -20);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS2', 'Contact', 'Applications__r', 'Child', '0',null,null, -20);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS3', 'Contact', 'Language_1_Proficiency__c,Language_2_Proficiency__c', 'Count', 'Professional working,Full professional','2',null, 10);
        lstSS.add(ss);
        Insert lstSS;
    }
    
    public static testMethod void getListScoringSettingByObject_Test(){
        Test.startTest();
		List<Scoring_Settings__c> lstSS = ScoringHandler.getListScoringSettingByObject('Contact');
        Test.stopTest();
        
        System.assertEquals(3, lstSS.size());
        System.assertEquals('Test SS1', lstSS[0].Name);
        System.assertEquals('Test SS2', lstSS[1].Name);
        System.assertEquals('Test SS3', lstSS[2].Name);
    }
    
    public static testMethod void anyFieldChange_Test(){
        Contact con = [SELECT Do_you_smoke__c FROM Contact LIMIT 1];
        Contact newCon = con.clone(true,true,true,true);
        newCon.Do_you_smoke__c = 'Yes';
        
        Test.startTest();
        System.assert(ScoringHandler.anyFieldChange(newCon,con,'Contact'));
        newCon = con.clone(true,true,true,true);
        newCon.Language_1_Proficiency__c = 'Professional working';
        System.assert(ScoringHandler.anyFieldChange(newCon,con,'Contact'));
        Test.stopTest();
    }
    
    public static testMethod void getFieldsOnScoringSettingByObjectName_Test(){
        Test.startTest();
		List<String> lstFields = ScoringHandler.getFieldsOnScoringSettingByObjectName('Contact');
        system.debug('ScoringHandler.getFieldsOnScoringSettingByObjectName(Contact):' + ScoringHandler.getFieldsOnScoringSettingByObjectName('Contact'));
        Test.stopTest();
        System.assertEquals(3, lstFields.size());
        System.assertEquals('Do_you_smoke__c', lstFields[0]);      
        System.assertEquals('Language_1_Proficiency__c', lstFields[1]);        
        System.assertEquals('Language_2_Proficiency__c', lstFields[2]);
        
    }
    
    public static testMethod void recalculateScore_Test(){
        Contact con = [SELECT Do_you_smoke__c, (SELECT Id FROM Applications__r),Language_1_Proficiency__c,Language_2_Proficiency__c FROM Contact LIMIT 1];
        con.Do_you_smoke__c = 'YES';
        con.Language_1_Proficiency__c = 'Professional working';
        con.Language_2_Proficiency__c = 'Full professional';
        List<Scoring_Settings__c> lstSS = ScoringHandler.getListScoringSettingByObject('Contact');

        Test.startTest();
        System.assertEquals(90,ScoringHandler.recalculateScore(con,lstSS,null,'Contact',null));
        Test.stopTest();
    }
}