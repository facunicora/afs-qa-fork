@isTest(SeeAllData=false)
global class AfsSocialRegistrationHandlerTest {
    
    @isTest
    static void testCreateAndUpdateUser() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createConfigurationObj();
        AfsSocialRegistrationHandler handler = new AfsSocialRegistrationHandler();
        Profile p = [SELECT id FROM Profile WHERE Name = 'Applicant Community Member Profile'];
        List<Network> recCommunity = [SELECT Id, SelfRegProfileId, Name, OptionsSiteAsContainerEnabled FROM Network];
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
            'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'en_US', 'facebook',
            null, new Map<String, String>{'language' => 'en_US','sfdc_networkid' => recCommunity[0].id});
        NetworkSelfRegistration nsr = new NetworkSelfRegistration();
        nsr.AccountID = [SELECT id FROM Account LIMIT 1].id;
        nsr.networkid = recCommunity[0].id;
        insert nsr;
        User u = handler.createUser(null, sampleData);
        u.profileId = p.id;
        System.assertEquals('testuser@example.org', u.userName);
        System.assertEquals('testuser@example.org', u.email);
        System.assertEquals('testLast', u.lastName);
        System.assertEquals('testFirst', u.firstName);
        System.assertEquals('test', u.alias);
        insert(u);
        String uid = u.id;
        
        sampleData = new Auth.UserData('testNewId', 'testNewFirst', 'testNewLast',
            'testNewFirst testNewLast', 'testnewuser@example.org', null, 'testnewuserlong', 'en_US', 'facebook',
            null, new Map<String, String>{});
        handler.updateUser(uid, null, sampleData);
        
        User updatedUser = [SELECT userName, email, firstName, lastName, alias FROM user WHERE id=:uid];
        System.assertEquals('testuser@example.org', 'testuser@example.org');

    }
    
    @isTest
    static void testCreateAndUpdateUserException() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createConfigurationObj();
        AfsSocialRegistrationHandler handler = new AfsSocialRegistrationHandler();
        List<Network> recCommunity = [SELECT Id, SelfRegProfileId, Name, OptionsSiteAsContainerEnabled FROM Network];
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
            'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'en_US', 'facebook',
            null, new Map<String, String>{'language' => 'en_US','sfdc_networkid' => recCommunity[0].id});
        
        User u = handler.createUser(null, sampleData);
        System.assertEquals(false, u == null);
        
        sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
            'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'en_US', 'facebook',
            null, new Map<String, String>{'language' => 'en_US','sfdc_networkid' => 'XXX'});
        
         u = handler.createUser(null, sampleData);
        
    }
    
    @isTest
    static void generateSessionTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
            Profile p = [SELECT id FROM Profile WHERE Name = 'Applicant Community Member Profile'];
            List<Network> recCommunity = [SELECT Id, SelfRegProfileId, Name, OptionsSiteAsContainerEnabled FROM Network];
            NetworkSelfRegistration nsr = new NetworkSelfRegistration();
            nsr.AccountID = [SELECT id FROM Account LIMIT 1].id;
            nsr.networkid = recCommunity[0].id;
            insert nsr;
            Afs_ParentComponentController.loginWrapper wrapperObj = new Afs_ParentComponentController.loginWrapper();
            wrapperObj.FirstName = 'Test';
            wrapperObj.LastName = 'AppOhm';
            wrapperObj.Email = 'user@appOhm.com';
            wrapperObj.Password = 'Welcome#12';
            wrapperObj.MobileNumber = '9929292929';
            wrapperObj.ZipCode = '312001';
            wrapperObj.AgreeTerms = true;
            wrapperObj.KeepMeInformed = true;   
            User user = AfsUserManagementController.getInMemoryUserAccount(wrapperObj.Email,wrapperObj.FirstName,wrapperObj.LastName,recCommunity[0].id,wrapperObj);
            try{
                AfsUserManagementController.createUserAndLogin(user, wrapperObj.Password, nsr.AccountID);
            }catch(Exception e){
               system.assertEquals(true,e != null);
            }
        Test.stopTest();
    }
    
    @isTest
    static void createUserAndLoginExceptionTest() {  
        
        Test.startTest();
            
            try{
                AfsUserManagementController.createUserAndLogin(null, '', '');
            }catch(Exception e){
               system.assertEquals(true,e != null);
            }
        Test.stopTest();
    }
}