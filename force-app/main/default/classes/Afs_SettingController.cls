global without sharing class Afs_SettingController {
	/**
	 * @Desc : Method to get picklist values
	 * @param objObject : Object name
	 * @param fld : field names seperated by comma
	 */
    @AuraEnabled
    global static map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> getPicklistValues(String objObject, string fld) {
        list<String> fieldAPINames = fld.split(',');
        map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> mapFieldPicklistVal = new map<String,list<Afs_UtilityApex.PicklistWrapperUtil>>();
        
        for(String fieldAPIName : fieldAPINames){
            list<Afs_UtilityApex.PicklistWrapperUtil> picklsitValues = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            if(fieldAPIName.trim() == 'Country_of_Legal_Residence__c'){
                picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_YourApplication_SelectCountry,false,System.label.AfsLbl_YourApplication_SelectCountry));
            }else if(fieldAPIName.trim() == 'NationalityCitizenship__c'){
            	picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_YourApplication_SelectNationality,false,System.label.AfsLbl_YourApplication_SelectNationality));
            }else if(fieldAPIName.trim() == 'Gender__c'){
            	picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_YourApplication_SelectGender,false,System.label.AfsLbl_YourApplication_SelectGender));
            }else{
            	picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_General_None,false,System.label.AfsLbl_General_None));
            }
            
            picklsitValues.addAll(Afs_UtilityApex.getPicklistWrapper(objObject, fieldAPIName.trim()));
            mapFieldPicklistVal.put(fieldAPIName.trim(),picklsitValues);
        }
        
        return mapFieldPicklistVal; 
        
    }
    
    /**
	 * @Desc : Method to change password for user
	 * @param password : password string 
	 */
    @AuraEnabled
    global static void changePassword(String password) {
        try{            
            Id userId = UserInfo.getUserId();
        	System.setPassword(userId, password);
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            String errMsg = e.getMessage();
            if(e.getMessage().equalsIgnoreCase(System.label.AfsLbl_Error_PasswordSystem)){
                errMsg += ' '+System.label.AfsLbl_Error_PasswordAddOn;
            }else{
                errMsg = errMsg.split(':')[1];
            }
            
            throw new AuraHandledException( errMsg );
        }
        
    }
    
    /**
	 * @Desc : Method to save contact
	 * @param contactObj : Contact object instance to update
	 */
    @AuraEnabled
    global static void saveRecord(Contact contactObj) {
        try{            
            update contactObj; 
            Afs_UtilityApex.updateUser(UserInfo.getUserId());
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
        }
    }
}