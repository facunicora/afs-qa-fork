global without sharing class Afs_AdvisorController {
    
	@AuraEnabled// Insert feed on click of save
    global static void insertFeed(Id ApplicationId, String body){
        try{
        	//old code
            /*ApplicationId = ApplicationId;
            FeedItem feed = new FeedItem();
            feed.Body = body;
            feed.ParentId = ApplicationId;
            feed.Status = 'Published';
            feed.visibility = 'AllUsers';
            insert feed;*/
            
            //new  code with mention

        	ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
			ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
			ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
			messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
			
			Application__c appObject = [Select OwnerId, Owner.Type from Application__c where Id =:ApplicationId limit 1];
			
			if(appObject.Owner.Type == 'User'){
				ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
				mentionSegmentInput.id = appObject.OwnerId;
				
				messageBodyInput.messageSegments.add(mentionSegmentInput);
			}
			
			textSegmentInput.text = ' ' + body;
			messageBodyInput.messageSegments.add(textSegmentInput);
			
			feedItemInput.body = messageBodyInput;
			feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
			feedItemInput.subjectId = ApplicationId;
			
			ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
            
        }catch(Exception e){
        	System.debug('insertFeed: ' + e.getMessage());
            throw new AuraHandledException( e.getMessage());
        }
    }
    
    @AuraEnabled//Insert Comment
    global static void insertComment(Id feedId, String body){
        try{
            FeedComment feedCommentObj = new FeedComment();
            feedCommentObj.CommentBody = body;
            feedCommentObj.FeedItemId = feedId;
            feedCommentObj.Status = 'Published';
            insert feedCommentObj;
        }catch(Exception e){
            throw new AuraHandledException( e.getMessage());
        }
    }
    
    @AuraEnabled// Fetch feed 
    global static list<feedWrapper> getFeed(String ApplicationId, String ownerApplication, String contactId){
        list<feedWrapper> feedWrapList = new list<feedWrapper> ();
        try{
            Application__c application = [SELECT id,Owner.Name FROM Application__c WHERE id =: ApplicationId];
            Id userId = UserInfo.getUserId();
            
            Set<id> groupMembersSet = new Set<id> ();
            Map<id,String> userPhotoUrl = new Map<id,String> ();
            for(GroupMember gm : [Select UserOrGroupId From GroupMember where GroupId =:ownerApplication]){
                groupMembersSet.add(gm.UserOrGroupId);
            }
            
            for(FeedItem feed : [SELECT id,Body,InsertedById,LikeCount,(SELECT id,CommentBody,InsertedById FROM FeedComments ORDER BY CreatedDate) FROM FeedItem
                                 WHERE ParentId =: ApplicationId AND
                                 (InsertedById IN: groupMembersSet OR InsertedById =: userId)])
            {
                groupMembersSet.add(feed.InsertedById);
                for(FeedComment comment : feed.FeedComments){
                    groupMembersSet.add(comment.InsertedById);
                }
            }
            
            String currentContactId = [SELECT id,ContactId FROM USER WHERE id =: UserInfo.getUserId()].ContactId;
            Set<id> contentDocumentIdSet = new Set<Id> ();
            for(ContentDocumentLink cdlink : [SELECT id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId =: currentContactId]){
                contentDocumentIdSet.add(cdlink.ContentDocumentId);
            }
            
            list<ContentVersion> cvList = [SELECT id,VersionData FROM ContentVersion WHERE ContentDocumentId IN: contentDocumentIdSet AND Afs_ProfilePhoto__c = true];
            String url = (cvList.isEmpty() ?  ''  : 'data:image/jpeg;base64,' + EncodingUtil.base64Encode(cvList[0].VersionData));
            for(User uObj : [SELECT id,SmallPhotoUrl FROM USER WHERE id IN: groupMembersSet]){
                userPhotoUrl.put(uObj.id,uObj.SmallPhotoUrl);
            }
            
            for(FeedItem feed : [SELECT id,Body,InsertedById,LikeCount,(SELECT id,CommentBody,InsertedById FROM FeedComments ORDER BY CreatedDate) FROM FeedItem
                                 WHERE ParentId =: ApplicationId AND
                                 (InsertedById IN: groupMembersSet OR InsertedById =: userId)])
            {
                if(String.isNotBlank(feed.Body)){
                    feedWrapList.add(new feedWrapper(feed,userId != feed.InsertedById,System.label.AfsLbl_Adivisor_Advisor,userPhotoUrl,url));
                }
            }
            /*if(feedWrapList.isEmpty()){
            FeedItem feedItem = new FeedItem();
            feedItem.Body = 'Welcome, I will be your advisor. Message with any questions!';
            feedWrapList.add(new feedWrapper(feedItem,true,System.label.AfsLbl_Adivisor_Advisor,userPhotoUrl.values()[0],url));
            }*/
        }catch(Exception e){
            throw new AuraHandledException( e.getMessage());
        }
        return feedWrapList;
    }
    
    @AuraEnabled // Like feed
    global static void likeFeed(Id feedId){
        try{
            ConnectApi.ChatterLike chatterLike = ConnectApi.ChatterFeeds.likeFeedElement(Network.getNetworkId(), feedId);
        }catch(Exception e){
            throw new AuraHandledException( e.getMessage());
        }
    }
    
    // Class Wrapper
    global class feedWrapper{
        @AuraEnabled
        global FeedItem feedObj {get;set;}
        @AuraEnabled
        global List<feedCommentWrapper> feedCommentWrapperList {get;set;}
        @AuraEnabled
        global boolean isAgent {get;set;}
        @AuraEnabled
        global String agentName {get;set;}
        @AuraEnabled
        global String commentBody {get;set;}
		@AuraEnabled
        global String showCommentSection {get;set;}
        @AuraEnabled
        global String smallPhotoUrl {get;set;}
        @AuraEnabled
        global String YouProfile {get;set;}
        @AuraEnabled
        global Boolean isDummy {get;set;}
        
        global feedWrapper(FeedItem feed, Boolean isAgentTrue,String agent,Map<id,String> photoUrl,String url){
            feedObj = feed;
            isDummy = (feed.id == null);
            feedCommentWrapperList = new List<feedCommentWrapper>();
            for(FeedComment  fc : feed.FeedComments){
                feedCommentWrapperList.add(new feedCommentWrapper(fc,photoUrl));
            }
            isAgent = isAgentTrue;
            agentName = agent;
            if(feed.InsertedById != UserInfo.getUserId()){
                smallPhotoUrl = photoUrl.get(feed.InsertedById);
            }else{
                smallPhotoUrl = url;
            }
            YouProfile = url;
        }        
    }
    
    global class feedCommentWrapper{
        @AuraEnabled
        global FeedComment feedObj {get;set;}
        @AuraEnabled
        global boolean isAgent {get;set;}
        @AuraEnabled
        global String smallPhotoUrl {get;set;}
        global feedCommentWrapper(FeedComment feed,Map<id,String> photoUrl){
            feedObj = feed;
            if(feed.InsertedById != UserInfo.getUserId()){
                smallPhotoUrl = photoUrl.get(feed.InsertedById);
            }
            isAgent = (UserInfo.getUserId() != feed.InsertedById);
        }   
    }
}