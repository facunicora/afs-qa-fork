@isTest(SeeAllData=false)
global class Afs_AboutYouControllerTest {
    
	@isTest
    static void getPicklistValuesTest() {  
        Test.startTest();
        	system.assertEquals(true,Afs_AboutYouController.getPicklistValues('Contact','Country_of_Legal_Residence__c,NationalityCitizenship__c').get('NationalityCitizenship__c').Size() > 0);
        Test.stopTest();
    }
    
    @isTest
    static void doSubmittApplicationTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
             system.assertEquals('Processed',Afs_AboutYouController.doSubmittApplication([SELECT ID FROM Application__c LIMIT 1],'AboutYou'));
        Test.stopTest();
    }
    
    @isTest
    static void doSubmittApplicationException() {  
        Test.startTest();
            try{
                Afs_AboutYouController.doSubmittApplication(null,'AboutYou');
            }catch(exception e){
                system.assertEquals(true,e != null);
            }
                
        Test.stopTest();
    }
}