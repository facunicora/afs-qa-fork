@isTest
private class GL_SF_Integrations_BatchTest {
    private static Hosting_Program__c Create_Hosting_Program(Account acc){
        Hosting_Program__c hp = new Hosting_Program__c();
        hp.name = 'Test Hosting Program';
        hp.Host_Partner__c = acc.Id;
        hp.Duration__c = 'YP';
        hp.Program_Content__c = 'as';
        hp.RecordType = [Select Id From RecordType Where DeveloperName = 'X18']; //flagship
        hp.App_Received_From__c = Date.today();
        hp.App_Received_To__c = date.today();
        hp.Return_Date__c = Date.today();
        hp.Date_of_Birth_From__c = Date.today();
        hp.Date_of_Birth_To__c = Date.today();
        hp.Program_Content__c = 'ag';
        hp.Program_Type__c = 'High school';
        hp.Hemisphere__c = 'SH';
        hp.Duration__c = 'YP';
        hp.Language_of_Placement__c = 'Afrikaans';
        hp.Year__c = '2006';
        return hp;
    }
    private static Application__c CreateApplication(contact aplicantContact, Program_Offer__c po, Id RecordTypeId, string status){
        Application__c application = new application__c();
        application.Applicant__c = aplicantContact.Id;
        application.Program_Offer__c = po.Id;
        application.RecordTypeId = RecordTypeId;
        application.Confirmed__c = true;
        application.Status__c = status;
        if(status == 'Cancelled'){
            application.Cancellation_Reason__c = 'Other';
            application.Cancellation_Reason_Other__c = 'Test';
        }
        return application;
    }
    private static Program_Offer__c create_Program_Offer(Account acc,Hosting_Program__c hp, string status){	
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']; //flagship
        po.Durations__c = 'YP';
        po.Con__c = 'ag';
        po.Cycle__c = 'SH';
        po.Year__c = '2006';
        po.Pre_application_Fee_numeric__c = 10;
        po.Visa_Fee_numeric__c = 10;
        po.Program_Price_numeric__c = 10;
        po.Date_of_Birth_To__c = Date.today();
        po.Flexible_Age_Range__c = 'Yes';
        po.Date_of_Birth_From__c = Date.today();
        po.Date_of_Birth_To__c = Date.today();
        po.Program_Type__c = 'High school';
        po.Year__c = '2006';        //hosting_program.app_recived_from__c        
        switch on status{
            when 'Product Desire'{
                po.Product_Desire__c = 1;
            }
            when 'Participation Desire'{
                po.Participation_Desire__c = 1;
            }
            when 'Decision'{
                po.Decision__c = 1;
            }
            when 'Confirmation'{
                po.Confirmation__c = 1;
            }
            when 'Purchase'{
                po.Purchase__c = 1;
            }
            when 'Onboarding'{
                po.Onboarding__c = 1;
            }
            when 'Participation'{
                po.Participation__c = 1;
            }
            when 'Cancelled'{
                po.Cancelled__c = 1;
            }
        }
        insert po;
        return po;
    }
    private static Program_Offer__c create_Program_Offer(Account acc,Hosting_Program__c hp){
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']; //flagship
        po.Durations__c = 'YP';
        po.Con__c = 'ag';
        po.Cycle__c = 'SH';
        po.Year__c = '2006';
        po.Pre_application_Fee_numeric__c = 10;
        po.Visa_Fee_numeric__c = 10;
        po.Program_Price_numeric__c = 10;
        po.Date_of_Birth_To__c = Date.today();
        po.Flexible_Age_Range__c = 'Yes';
        po.Date_of_Birth_From__c = Date.today();
        po.Date_of_Birth_To__c = Date.today();
        po.Program_Type__c = 'High school';
        po.Year__c = '2006';        
        insert po;
        return po;
    }
    
    @IsTest public static void HostingProgramTest(){       
        Test.setMock(HttpCalloutMock.class, new RemoteSiteManagerMock());
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        
        Test.startTest();
        GL_SF_Integrations_Batch obj = new GL_SF_Integrations_Batch(new List<Hosting_Program__c>{hp}, NULL, 2, '1');
        DataBase.executeBatch(obj);
        Test.stopTest();
        
    }
    @IsTest public static void ProgramOfferTest(){
        Test.setMock(HttpCalloutMock.class, new RemoteSiteManagerMock());
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = create_Program_Offer(acc, hp);
        hp.Name_External_Id__c = po.Name.right(12);
        update hp;
        hp = create_Hosting_Program(acc);
        hp.Name_External_Id__c = 'st-Test-Test';
        insert hp;
        
        Test.startTest();
        GL_SF_Integrations_Batch obj = new GL_SF_Integrations_Batch(new List<Program_Offer__c>{po}, 1, 2, '1');
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
}