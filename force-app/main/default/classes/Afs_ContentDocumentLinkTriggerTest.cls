@isTest
public class Afs_ContentDocumentLinkTriggerTest {
	@isTest
    static void getUploadedPhotoTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createContentDocumentLinksContact(1);
        Test.startTest();
        	Afs_TestDataFactory.createContentDocumentLinksContact(1);
        Test.stopTest();
        system.assertEquals(true,[SELECT id FROM Contentversion].size() > 0);
    }
}