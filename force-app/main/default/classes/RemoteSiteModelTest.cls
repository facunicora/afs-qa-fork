@IsTest
public class RemoteSiteModelTest {
    @IsTest static void MTPDates(){
        RemoteSiteModel.MTPDates MTP = New remoteSiteModel.MTPDates();
        MTP.program_code = 'Test';
        MTP.host_ioc = 'Test';
        MTP.send_ioc = 'Test';
        MTP.from_arrival_date = 'Test';
        MTP.from_arrival_date2 = 'Test';
        MTP.from_arrival_date3 = 'Test';
        MTP.to_departure_date = 'Test';
    }
    @IsTest static void ResponseTest(){
        RemoteSiteModel.Response res = new RemoteSiteModel.Response();
        res.itemsTotal = 'Test';
        res.pageIndex = 'Test';
        res.pageSize = 'Test';
        res.pagesTotal = 'Test';
        res.statusCode = 'Test';
    }
    @IsTest static void MatrixTest(){
        RemoteSiteModel.Matrix mat = New remoteSiteModel.matrix();
        mat.send_ioc = 'Test';
        mat.program_code = 'Test';
        mat.host_ioc = 'Test';
        mat.host_planned_number = 'Test';
        mat.send_projected_actual = 'Test';
    }
    @IsTest static void HostingFactSheetsTest(){
        RemoteSiteModel.HostingFactSheets hos = New remoteSiteModel.HostingFactSheets();
        hos.host_ioc = 'Test';
        hos.program_code = 'Test';
        hos.program_title = 'Test';
        hos.program_duration = 'Test';
        hos.program_cycle = 'Test';
        hos.program_content = 'Test';
        hos.program_type = 'Test';
        hos.program_description = 'Test';
        hos.program_language = 'Test';
        hos.program_year = 'Test';
        hos.age_range_year_start = 'Test';
        hos.age_range_year_end = 'Test';
        hos.age_range_month_start = 'Test';
        hos.age_range_month_end = 'Test';
        hos.app_received_start = '11111111';
        hos.app_received_end = 'Test';
        hos.from_departure_date = 'Test';
        hos.to_departure_date = 'Test';
        hos.from_arrival_date = 'Test';
        hos.to_arrival_date = 'Test';
        hos.from_arrival_date2 = 'Test';
        hos.to_arrival_date2 = 'Test';
        hos.from_arrival_date3 = 'Test';
        hos.from_departure_date3 = 'Test';
        hos.graduate_accept = 'Test';
    }
    @IsTest static void Organization(){
        RemoteSiteModel.Organization Org = New remoteSiteModel.Organization();
        Org.sf_org_id = 'Test';
        Org.id = 'Test';
        Org.owner_ioc = 'Test';
        Org.ioc_code = 'Test';
        Org.organization_name = 'Test';
        Org.native_name = 'Test';
        Org.organization_ref = 'Test';
        Org.org_type = 'Test';
        Org.org_sub_type = 'Test';
        Org.org_status = 'Test';
        Org.chapter_id = 'Test';
        Org.area_team_id = 'Test';
        Org.region_id = 'Test';
        Org.web_site = 'Test';
        Org.english_address1 = 'Test';
        Org.english_address2 = 'Test';
        Org.english_city = 'Test';
        Org.english_state = 'Test';
        Org.english_zip = 'Test';
        Org.native_address1 = 'Test';
        Org.native_address2 = 'Test';
        Org.native_city = 'Test';
        Org.native_state = 'Test';
        Org.native_zip = 'Test';
        Org.country = 'Test';
        Org.telnum1 = 'Test';
        Org.faxnum = 'Test';
    }
    @IsTest static void ZipcodeAssignments(){
        RemoteSiteModel.ZipcodeAssignments zip = New RemoteSiteModel.ZipcodeAssignments();
        zip.id = 'Test';
        zip.ioc_code = 'Test';
        zip.zip_from = 'Test';
        zip.zip_to = 'Test';
        zip.assigned_org_id = 'Test';
        //zip.assigned_org_english_name = 'Test';
        //zip.assigned_org_native_name = 'Test';
        //zip.assigned_org_level = 'Test';
        //zip.city = 'Test';
    }
    @IsTest static void ServiceAndOA(){
        RemoteSiteModel.ServiceAndOA SaO = New RemoteSiteModel.ServiceAndOA();
        SaO.id = 'Test'; SaO.sf_service_id = 'Test'; SaO.person_id = 'Test'; SaO.service_ref = 'Test'; SaO.ioc_code = 'Test'; SaO.owner_ioc = 'Test'; SaO.selected_partner_ioc_code = 'Test'; SaO.host_country = 'Test'; SaO.stage = 'Test'; SaO.status = 'Test';
        SaO.local_program_id = 'Test'; SaO.best_time_to_contact = 'Test'; SaO.selected_program_code = 'Test'; SaO.form_status = 'Test'; SaO.preapp_fullapp_ind = 'Test'; SaO.assigned_org_id = 'Test'; SaO.interview_date = 'Test'; SaO.program_name = 'Test';
        SaO.program_name_native = 'Test'; SaO.program_type = 'Test'; SaO.program_start_date = 'Test'; SaO.school = 'Test'; SaO.school_id = 'Test'; SaO.other_school_city = 'Test'; SaO.other_school_state = 'Test'; SaO.other_school_zip = 'Test'; SaO.graduation_year = 'Test'; SaO.language_desc_memo = 'Test';
        SaO.fam_participated_on_afs_prg = 'Test'; SaO.fam_hosted_on_afs = 'Test'; SaO.living_abroad = 'Test'; SaO.graduate_year = 'Test'; SaO.self_description = 'Test'; SaO.cs_skill = 'Test'; SaO.medication_description = 'Test'; SaO.program_expectation = 'Test'; SaO.program_content = 'Test'; SaO.program_duration = 'Test'; SaO.preferred_year = 'Test'; SaO.countries_preference1 = 'Test'; SaO.countries_preference2 = 'Test';
        SaO.countries_preference3 = 'Test'; SaO.preferred_country = 'Test'; SaO.medical_review_status = 'Test'; SaO.dietary_restrictions = 'Test'; SaO.dietary_restrictions_ind = 'Test'; SaO.smoker_ind = 'Test'; SaO.practice_religion_ind = 'Test'; SaO.physical_restrictions_ind = 'Test'; SaO.physical_restrictions = 'Test'; SaO.emergency_contact = 'Test'; SaO.fee_pay_type = 'Test';
        SaO.fee_pay_code = 'Test'; SaO.fee_admission = 'Test'; SaO.interview_result_ind = 'Test'; SaO.parent_legal_guardian_other = 'Test'; SaO.what_school_do_you_attend = 'Test'; SaO.no_restrictions_or_allergies = 'Test'; SaO.limited_in_the_activities_i_can_do = 'Test'; SaO.have_allergies = 'Test'; SaO.take_medications = 'Test'; SaO.can_t_live_with_a_smoker = 'Test'; SaO.have_food_allergies = 'Test'; SaO.vegetarian = 'Test';
        SaO.vegan = 'Test'; SaO.kosher = 'Test'; SaO.halal = 'Test'; SaO.celiac = 'Test'; SaO.dietary_comments = 'Test'; SaO.willing_to_change_diet_during_program = 'Test'; SaO.treated_for_any_health_issues_physical = 'Test'; SaO.health_issue_description = 'Test';
        SaO.what_are_you_looking_for = 'Test'; SaO.what_do_you_want_out_of_this_experience = 'Test'; SaO.how_would_friends_family_describe_you = 'Test'; SaO.tell_us_about_yourself = 'Test'; SaO.program_you_participated = 'Test'; SaO.can_t_live_with_pets_s = 'Test'; SaO.this_is_your_emergency_contact = 'Test'; SaO.video_for_host_family = 'Test'; SaO.pets = 'Test';
        SaO.dual_citizenship = 'Test'; SaO.second_citizenship = 'Test'; SaO.home_mother = 'Test'; SaO.home_father = 'Test'; SaO.home_stepmother = 'Test'; SaO.home_stepfather = 'Test'; SaO.home_sister_stepsister = 'Test'; SaO.home_brother_stepbrother = 'Test'; SaO.home_sister_stepsisters_age = 'Test'; SaO.home_brother_stepbrothers_age = 'Test'; SaO.home_grandmother = 'Test';
        SaO.home_grandfather = 'Test'; SaO.home_other = 'Test'; SaO.home_other_relationship = 'Test'; SaO.instagram = 'Test'; SaO.linkedin = 'Test'; SaO.facebook = 'Test'; SaO.twitter = 'Test'; SaO.snapchat = 'Test'; SaO.youtube = 'Test'; SaO.are_you_attending_university_college = 'Test'; SaO.describe_yourself_as_a_student = 'Test';
        SaO.name_of_university_college_school = 'Test'; SaO.criminal_convictions = 'Test'; SaO.open_to_same_sex_host_parents = 'Test'; SaO.open_to_single_host_parent = 'Test'; SaO.open_to_sharing_a_host_family = 'Test'; SaO.lgbtq_member = 'Test'; SaO.tshirt_info = 'Test'; SaO.bv_record_locator = 'Test'; SaO.bv_airline = 'Test'; SaO.bv_flight_number = 'Test';
        SaO.bv_departure_time = 'Test'; SaO.bv_arrival_time = 'Test'; SaO.bv_departure_airport = 'Test'; SaO.bv_arrival_airport = 'Test'; SaO.bv_domestic_travel_arrangements = 'Test'; SaO.bv_arrival_to_gateway_city_with_who = 'Test'; SaO.bv_gateway_city = 'Test'; SaO.bv_arrive_in_prior_to_the_start = 'Test'; SaO.bv_additional_comments = 'Test'; SaO.bv_contact_first_name = 'Test'; SaO.bv_contact_last_name = 'Test'; 
        SaO.bv_contact_phone = 'Test'; SaO.bv_contact_phone_type = 'Test'; SaO.bv_contact_relationship = 'Test'; SaO.cbh_record_locator = 'Test'; SaO.cbh_airline = 'Test'; SaO.cbh_flight_number = 'Test'; SaO.cbh_departure_time = 'Test'; SaO.cbh_arrival_time = 'Test'; SaO.cbh_departure_airport = 'Test';
        SaO.cbh_arrival_airport = 'Test'; SaO.cbh_domestic_travel_arrangements = 'Test'; SaO.cbh_arrival_to_gateway_city_with_who = 'Test'; SaO.cbh_gateway_city = 'Test'; SaO.cbh_arrive_in_prior_to_the_start = 'Test'; SaO.cbh_additional_comments = 'Test'; SaO.cbh_contact_first_name = 'Test'; SaO.cbh_contact_last_name = 'Test'; SaO.cbh_contact_phone = 'Test';
        SaO.cbh_contact_phone_type = 'Test'; SaO.cbh_contact_relationship = 'Test'; SaO.things_to_share = 'Test'; SaO.describe_a_normal_day_in_your_life = 'Test'; SaO.what_you_bring_to_your_afs_experience = 'Test'; SaO.food_thoughts = 'Test'; SaO.favorite_travel_destinations_books = 'Test'; SaO.why_is_global_competence_important_for_students = 'Test'; SaO.how_to_be_a_global_citizen = 'Test';
        SaO.medical_condition_comments = 'Test'; SaO.i_ve_traveled_abroad = 'Test'; SaO.i_ve_lived_abroad = 'Test'; SaO.none_of_the_above = 'Test'; SaO.option_1_interview_date = 'Test'; SaO.option_2_interview_date = 'Test'; SaO.option_3_interview_date = 'Test'; SaO.option_1_time_preference = 'Test'; SaO.option_2_time_preference = 'Test'; SaO.option_3_time_preference = 'Test';
        SaO.created_date = 'Test'; SaO.created_by = 'Test'; SaO.last_modified_date = 'Test'; SaO.last_modified_by = 'Test'; SaO.source = 'Test'; SaO.agreement_text = 'Test'; SaO.agreement_ind = 'Test';
    }
}