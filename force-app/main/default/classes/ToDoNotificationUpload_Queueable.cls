public class ToDoNotificationUpload_Queueable implements Queueable {
	public ToDoNotificationUpload_Queueable() {
    }
    public void execute(QueueableContext context) {
        Map<Id, Opportunity> oppMap  = new Map<Id, Opportunity>();
        Set<String> setStatus = new Set<String>{'Completed','In Review'};
        Date ThisDay = Date.today();
        for(To_Do_Item__c toDo :[SELECT Id, Name, Type__c, Help__c, Due_Date__c, Application__r.Opportunity__c, RecordTypeId,
                                 SUBMIT_YOUR_HEALTH_CERTIFICATE_BY__c, SUBMIT_YOUR_ACADEMIC_HISTORY__c, SCHOLARSHIP_DOCUMENTATION__c,
                                 INTRODUCE_YOURSELF_BY__c, START_YOUR_LANGUAGE_TRAINING__c, SIGN_PARTICIPATION_AGREEMENT_BY__c
                                 FROM To_Do_Item__c
                                 WHERE Due_Date__c <= :ThisDay + 15
                                 AND Status__c not in :setStatus]){
            string typeName = Schema.SObjectType.To_Do_Item__c.getRecordTypeInfosById().get(toDo.recordtypeid).getname();
                if((typeName == 'Custom' && toDo.Type__c == 'Upload') || toDo.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY__c || 
                   	toDo.SUBMIT_YOUR_ACADEMIC_HISTORY__c || toDo.SCHOLARSHIP_DOCUMENTATION__c || toDo.INTRODUCE_YOURSELF_BY__c || 
                   	toDo.START_YOUR_LANGUAGE_TRAINING__c || toDo.SIGN_PARTICIPATION_AGREEMENT_BY__c){
                    if(oppMap.containsKey(toDo.Application__r.Opportunity__c)){
                        Opportunity opp = oppMap.get(toDo.Application__r.Opportunity__c);
                    	opp.Pendings_ToDo_submit_documents__c += '\n' + toDo.Name;
                    }
                    else{
                    	oppMap.put(toDo.Application__r.Opportunity__c, new Opportunity(Id = toDo.Application__r.Opportunity__c,
                                                            Pendings_ToDo_submit_documents__c = toDo.Name,
                                                            Due_Date_submit_documents__c = Date.today()));
                    }
                    
                }
        }
        List<Database.SaveResult> lstSR = database.update(oppMap.values(), false);
        for(Integer i = 0;i < lstSR.Size(); i++){                
            Database.SaveResult sr = lstSR[i];
            if(!sr.isSuccess()){
                system.debug(logginglevel.error,oppMap.values()[i] + ' --> ' + String.join(sr.getErrors(),' / '));
            }
        } 
    } 
}