global class Afs_SecurityEnforceUtility {
    
    global static final string ACCESS_ERROR = System.Label.AfsLbl_SecurityEnforceUtil_AccessError;
    
	global static void checkObjFieldsAccess(Set<String> fields, String objectName, Boolean needAllFldsAccess){
        /*objectName = objectName.toLowerCase();
        List<String> accessibleFlds = new List<String>(fields);
        
         Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
         //string allObj = String.join(new List<string>(Schema.getGlobalDescribe().keyset()), ',');
         //system.debug('@@@@@@@@@@@@@@' + objectName + '##' + objType + '##' + allObj);
         Schema.DescribeSObjectResult objDescribeSObjectRes = objType.getDescribe();
         //Check: if user is allowed to see this object
         if(!objDescribeSObjectRes.isAccessible()) {
             throw new AfsException(ACCESS_ERROR + ' SObject - ' + objectName + ', please contact administrator.');
         }
         
         //check: if user has field access
         Map<String,Schema.SObjectField> mapfields = objDescribeSObjectRes.fields.getMap();
         accessibleFlds.clear();
         List<String> notAccessibleFlds = new List<String>();
         String tempFld = '';
         //System.assert(objectName != 'Campaign', fields);
         for(String fld : fields){
             fld = fld.toLowerCase();
             
             //only when field is not added
             if(!accessibleFlds.contains(fld)){
              tempFld = fld;
              if(fld.contains('__r')){
                  tempFld = fld.substringBefore('__r') + '__c';
              }
              Schema.SObjectField fldType = mapfields.get(tempFld); 
              if(fldType != null){
                  Schema.DescribeFieldResult fR = fldType.getDescribe();
                  if(fR.isAccessible()){
                      accessibleFlds.add(fld);
                  }else{
                      notAccessibleFlds.add(fld);
                  }
              }else{
               accessibleFlds.add(fld);
              }
             }
         }
         
         //throw exception: when all
         if(needAllFldsAccess && notAccessibleFlds.size() > 0){
             throw new AfsException(ACCESS_ERROR + + ' SObject - ' + objectName + ' :: Fields-' + String.join(notAccessibleFlds,',') + ', please contact administrator.'); 
         }*/
        
        
    }
    
    global class AfsException extends Exception {}
}