@isTest
public class GL_SF_Integrations_Daily_Test {
	public static testMethod void execute_test(){
        GL_SF_Integrations_Daily glSfIntDa = new GL_SF_Integrations_Daily();
        glSfIntDa.execute(null);
        List<AsyncApexJob> lstJobs = [Select Id From AsyncApexJob];
        system.Assert(lstJobs.size() == 1);
    }
}