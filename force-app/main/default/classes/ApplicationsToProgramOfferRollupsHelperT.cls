@IsTest
public class ApplicationsToProgramOfferRollupsHelperT {
    @testSetup static void testSetup(){
        Account acc = Util_Test.create_AFS_Partner();
        Contact con = Util_Test.createContact('Test', acc);
        insert con;
        Hosting_Program__c hp = Create_Hosting_Program(acc);
        Program_Offer__c po =  create_Program_Offer(acc, hp);
        insert po;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        Application__c app = Util_Test.create_Applicant(con, po);
    }
    private static Account Create_Account(Id RecordTypeid){
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.IOC_Code__c = 'Test IOC Code';
        acc.RecordTypeid = RecordTypeid;
        return acc;
    }
    private static Hosting_Program__c Create_Hosting_Program(Account acc){
        Hosting_Program__c hp = new Hosting_Program__c();
        hp.name = 'Test Hosting Program';
        hp.Host_Partner__c = acc.Id;
        hp.Duration__c = 'YP';
        hp.Program_Content__c = 'as';
        hp.RecordType = [Select Id From RecordType Where DeveloperName = 'X18']; //flagship
        hp.App_Received_From__c = Date.today();
        hp.App_Received_To__c = date.today();
        hp.Return_Date__c = Date.today();
        hp.Date_of_Birth_From__c = Date.today();
        hp.Date_of_Birth_To__c = Date.today();
        hp.Program_Content__c = 'ag';
        hp.Program_Type__c = 'High school';
        hp.Hemisphere__c = 'SH';
        hp.Duration__c = 'YP';
        hp.Language_of_Placement__c = 'Afrikaans';
        hp.Year__c = '2006';
        Insert hp;
        return hp;
    }
    private static Application__c CreateApplication(contact aplicantContact, Program_Offer__c po, Id RecordTypeId, string status){
       Application__c application = new application__c();
       application.Applicant__c = aplicantContact.Id;
       application.Program_Offer__c = po.Id;
       application.RecordTypeId = RecordTypeId;
       application.Confirmed__c = true;
       application.Status__c = status;
        if(status == 'Cancelled'){
            application.Cancellation_Reason__c = 'Other';
            application.Cancellation_Reason_Other__c = 'Test';
        }
       return application;       
    }
    private static Program_Offer__c create_Program_Offer(Account acc,Hosting_Program__c hp, string status){
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']; //flagship
        po.Durations__c = 'YP';
        po.Con__c = 'ag';
        po.Cycle__c = 'SH';
        po.Year__c = '2006';
        po.Pre_application_Fee_numeric__c = 10;
        po.Visa_Fee_numeric__c = 10;
        po.Program_Price_numeric__c = 10;
        po.Date_of_Birth_To__c = Date.today();
        po.Flexible_Age_Range__c = 'Yes';
        po.Date_of_Birth_From__c = Date.today();
        po.Date_of_Birth_To__c = Date.today();
        po.Program_Type__c = 'High school';
        po.Year__c = '2006';
        
        //hosting_program.app_recived_from__c
        
        switch on status{
            when 'Product Desire'{
                po.Product_Desire__c = 1;
            }
            when 'Participation Desire'{
                po.Participation_Desire__c = 1;
            }
            when 'Decision'{
                po.Decision__c = 1;
            }
            when 'Confirmation'{
                po.Confirmation__c = 1;
            }
            when 'Purchase'{
                po.Purchase__c = 1;
            }
            when 'Onboarding'{
                po.Onboarding__c = 1;
            }
            when 'Participation'{
                po.Participation__c = 1;
            }
            when 'Cancelled'{
                po.Cancelled__c = 1;
            }
        }
        return po;
    }
    private static Program_Offer__c create_Program_Offer(Account acc,Hosting_Program__c hp){
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']; //flagship
        po.Durations__c = 'YP';
        po.Con__c = 'ag';
        po.Cycle__c = 'SH';
        po.Year__c = '2006';
        po.Pre_application_Fee_numeric__c = 10;
        po.Visa_Fee_numeric__c = 10;
        po.Program_Price_numeric__c = 10;
        po.Date_of_Birth_To__c = Date.today();
        po.Flexible_Age_Range__c = 'Yes';
        po.Date_of_Birth_From__c = Date.today();
        po.Date_of_Birth_To__c = Date.today();
        po.Program_Type__c = 'High school';
        po.Year__c = '2006';
        
        return po;
    }

    @IsTest static void Insert_Update_Delete_Test(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        List<Application__c> apps = new List<Application__c>();
        List<Program_Offer__c> pos = new List<Program_Offer__c>();
        Account acc;
        Hosting_Program__c hp;
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Program_Offer__c po;
        Id recordTypeId = [Select Id From RecordType Where DeveloperName = 'Flagship' AND SObjectType = 'application__c'].Id;
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Product Desire');
        pos.add(po);
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Participation Desire');
        pos.add(po);
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Decision');
        pos.add(po);
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Confirmation');
        pos.add(po);
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Purchase');
        pos.add(po);
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Onboarding');
        pos.add(po);
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Participation');
        pos.add(po);
        
        acc = Util_Test.create_AFS_Partner();
        hp = Create_Hosting_Program(acc);
        po = create_Program_Offer(acc, hp, 'Cancelled');
        pos.add(po);
        
        Test.startTest();
        
        insert pos;
        
        Application__c app2 = CreateApplication(con, pos[0], recordTypeId, 'Product Desire');
        apps.add(app2);
        app2 = CreateApplication(con, pos[1], recordTypeId,'Participation Desire');
        apps.add(app2);
        app2 = CreateApplication(con, pos[2], recordTypeId, 'Decision');
        apps.add(app2);
        app2 = CreateApplication(con, pos[3], recordTypeId, 'Confirmation');
        apps.add(app2);
        app2 = CreateApplication(con, pos[4], recordTypeId, 'Purchase');
        apps.add(app2);
        app2 = CreateApplication(con, pos[5], recordTypeId, 'Onboarding');
        apps.add(app2);
        app2 = CreateApplication(con, pos[6], recordTypeId, 'Participation');
        apps.add(app2);
        app2 = CreateApplication(con, pos[7], recordTypeId, 'Cancelled');
        apps.add(app2);
        insert apps;
        
        for(Program_Offer__c p : pos){
            po.Product_Desire__c = null;
            po.Decision__c = null;
            po.Participation__c = null;
            po.Onboarding__c = null;
            po.Confirmation__c = null;
            po.Purchase__c = null;
            po.Cancelled__c = null;
            po.Participation_Desire__c = null;
        }
        update pos;
        for(Application__c app :apps){
            switch on app.Status__c{
                /*when 'Product Desire'{
                    app.Status__c = 'Participation Desire';
                }
                when 'Participation Desire'{
                    app.Status__c = 'Decision';
                }*/
                when 'Decision'{
                    app.Status__c = 'Confirmation';
                }
                when 'Confirmation'{
                    app.Status__c = 'Purchase';
                }
                when 'Purchase'{
                    app.Status__c = 'Onboarding';
                }
                when 'Onboarding'{
                    app.Status__c = 'Participation';
                }
                when 'Participation'{
                    app.Status__c = 'Cancelled';
                    app.Cancellation_Reason__c = 'Other';
                    app.Cancellation_Reason_Other__c = 'Test';
                }
                when 'Cancelled'{
                    app.Status__c = 'Product Desire';
                    app.Cancellation_Reason__c = null;
                    app.Cancellation_Reason_Other__c = null;
                }
            }
        }
        update apps;
        for(Program_Offer__c p : pos){
            po.Product_Desire__c = null;
            po.Decision__c = null;
            po.Participation__c = null;
            po.Onboarding__c = null;
            po.Confirmation__c = null;
            po.Purchase__c = null;
            po.Cancelled__c = null;
            po.Participation_Desire__c = null;
        }
        update pos;
        for(Application__c app :apps){
            switch on app.Status__c{
                when 'Product Desire'{
                    app.Status__c = 'Participation Desire';
                }
                when 'Participation Desire'{
                    app.Status__c = 'Decision';
                }
                when 'Decision'{
                    app.Status__c = 'Confirmation';
                }
                when 'Confirmation'{
                    app.Status__c = 'Purchase';
                }
                when 'Purchase'{
                    app.Status__c = 'Onboarding';
                }
                when 'Onboarding'{
                    app.Status__c = 'Participation';
                }
                when 'Participation'{
                    app.Status__c = 'Cancelled';
                    app.Cancellation_Reason__c = 'Other';
                    app.Cancellation_Reason_Other__c = 'Test';
                }
                when 'Cancelled'{
                    app.Status__c = 'Product Desire';
                    app.Cancellation_Reason__c = null;
                    app.Cancellation_Reason_Other__c = null;
                }
            }
        }
        
        Test.stopTest();
        delete apps;
    }
    /*@IsTest static void DeleteTest(){
        List<Application__c> apps = new List<Application__c>();
        List<Program_Offer__c> pos = new List<Program_Offer__c>();
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Hosting_Program__c hp = [SELECT Id FROM Hosting_Program__c LIMIT 1];
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Program_Offer__c po;
        Id recordTypeId = [Select Id From RecordType Where DeveloperName = 'Flagship' AND SObjectType = 'application__c'].Id;
        
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        Application__c app2 = CreateApplication(con, po, recordTypeId, 'Product Desire');
        apps.add(app2);
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        app2 = CreateApplication(con, po, recordTypeId,'Participation Desire');
        apps.add(app2);
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        app2 = CreateApplication(con, po, recordTypeId, 'Decision');
        apps.add(app2);
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        app2 = CreateApplication(con, po, recordTypeId, 'Confirmation');
        apps.add(app2);
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        app2 = CreateApplication(con, po, recordTypeId, 'Purchase');
        apps.add(app2);
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        app2 = CreateApplication(con, po, recordTypeId, 'Onboarding');
        apps.add(app2);
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        app2 = CreateApplication(con, po, recordTypeId, 'Participation');
        apps.add(app2);
        po = create_Program_Offer(acc, hp);
        pos.add(po);
        app2 = CreateApplication(con, po, recordTypeId, 'Cancelled');
        apps.add(app2);
        
        Test.startTest();
        insert apps;
        for(Application__c app :apps){
            switch on app.Status__c{
                when 'Product Desire'{
                    app.Status__c = 'Participation Desire';
                }
                when 'Participation Desire'{
                    app.Status__c = 'Decision';
                }
                when 'Decision'{
                    app.Status__c = 'Confirmation';
                }
                when 'Confirmation'{
                    app.Status__c = 'Purchase';
                }
                when 'Purchase'{
                    app.Status__c = 'Onboarding';
                }
                when 'Onboarding'{
                    app.Status__c = 'Participation';
                }
                when 'Participation'{
                    app.Status__c = 'Cancelled';
                    app.Cancellation_Reason__c = 'Other';
                    app.Cancellation_Reason_Other__c = 'Test';
                }
                when 'Cancelled'{
                    app.Status__c = 'Product Desire';
                    app.Cancellation_Reason__c = null;
                    app.Cancellation_Reason_Other__c = null;
                }
            }
            app.Program_Offer__c = create_Program_Offer(acc, hp).Id;
        }
        delete apps;
        Test.stopTest();
    }*/
}