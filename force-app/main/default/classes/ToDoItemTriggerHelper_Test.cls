@isTest
public class ToDoItemTriggerHelper_Test {
    @TestSetup
    public static void setup_method(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',false,false);
    }   
    
    public static testMethod void calculateOpportunityCheckboxs_Test(){
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Application__c app = Util_Test.createApplication(con,null);
        Insert app;
        List<To_Do_Item__c> lstToDoItem = new List<To_Do_Item__c>();
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '1. Stage: Pre-application'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '2. Stage: Pre-selected'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '3. Stage: Accepted'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '4. Stage: Get Ready'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '- Extra documentation needed'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '1. Stage: Pre-application'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '2. Stage: Pre-selected'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '3. Stage: Accepted'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '4. Stage: Get Ready'));
        lstToDoItem.add(new To_Do_Item__c(Application__c = app.Id,Status__c = 'In Review', Stage_of_portal__c = '- Extra documentation needed'));
        
        Test.startTest();
        Insert lstToDoItem;
        
        Opportunity opp = [SELECT Id, Pre_application_in_review__c, Pre_selected_in_review__c, Accepted_in_review__c, Get_Ready_in_review__c, Extra_documentation_needed_in_review__c
                                  FROM Opportunity WHERE Application_ID__c = :app.Id]; 
       	system.assert(opp.Pre_application_in_review__c && opp.Pre_selected_in_review__c && opp.Accepted_in_review__c && opp.Get_Ready_in_review__c && opp.Extra_documentation_needed_in_review__c);        
        
        for(To_Do_Item__c tdi : lstToDoItem){
            tdi.Status__c = 'Completed';
        }
        
        Update lstToDoItem;
        opp = [SELECT Id, Pre_application_Completed__c, Pre_selected_Completed__c, Accepted_Completed__c, Get_Ready_Completed__c, Extra_documentation_needed_Completed__c
                                  FROM Opportunity WHERE Application_ID__c = :app.Id]; 
       	system.assert(opp.Pre_application_Completed__c && opp.Pre_selected_Completed__c && opp.Accepted_Completed__c && opp.Get_Ready_Completed__c && opp.Extra_documentation_needed_Completed__c);
        Test.stopTest();
        
        Delete lstToDoItem;
        opp = [SELECT Id, Pre_application_Completed__c, Pre_selected_Completed__c, Accepted_Completed__c, Get_Ready_Completed__c, Extra_documentation_needed_Completed__c
                                  FROM Opportunity WHERE Application_ID__c = :app.Id]; 
       	system.assert(opp.Pre_application_Completed__c && opp.Pre_selected_Completed__c && opp.Accepted_Completed__c && opp.Get_Ready_Completed__c && opp.Extra_documentation_needed_Completed__c);
    }
    public static testmethod void actualizarOppCheckbox_Test(){
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;

        List<Opportunity> opps = new List<Opportunity>();
        for(Integer i=0; i<6;i++){
            opps.add(new Opportunity(Name = 'test'+i, StageName = 'Prospecting', CloseDate = System.Today() + 30));
        }
        Afs_Opportunity_Trigger_Helper.allowOppUpdate = true;
        insert opps;
        List<Application__c> apps = new List<Application__c>();
        for(Integer i = 0; i<6; i++){
            apps.add(Util_Test.createApplication(con,null));
            apps[i].Opportunity__c = opps[i].Id;
        }
        Insert apps;
        List<To_Do_Item__c> lstToDoItem = new List<To_Do_Item__c>();
        lstToDoItem.add(new To_Do_Item__c(Application__c = apps[0].Id, Status__c = 'Completed', COMPLETE_YOUR_PROFILE_BY__c = true));
        lstToDoItem.add(new To_Do_Item__c(Application__c = apps[1].Id, Status__c = 'In Review', COMPLETE_YOUR_PROFILE_BY__c = true));
        lstToDoItem.add(new To_Do_Item__c(Application__c = apps[2].Id, Status__c = 'Rejected', COMPLETE_YOUR_PROFILE_BY__c = true));
        lstToDoItem.add(new To_Do_Item__c(Application__c = apps[3].Id, Status__c = 'Completed', COMPLETE_YOUR_PROFILE_BY__c = false));
        lstToDoItem.add(new To_Do_Item__c(Application__c = apps[4].Id, Status__c = 'In Review', COMPLETE_YOUR_PROFILE_BY__c = false));
        lstToDoItem.add(new To_Do_Item__c(Application__c = apps[5].Id, Status__c = 'Rejected', COMPLETE_YOUR_PROFILE_BY__c = false));
        
        Test.startTest();
        Insert lstToDoItem;
        
        lstToDoItem[0].COMPLETE_YOUR_PROFILE_BY__c = false;
        lstToDoItem[1].Status__c = 'Rejected';
        lstToDoItem[2].Status__c = 'Completed';
        
        update lstToDoItem;
        Test.stopTest();
        
        system.assertEquals(3, [SELECT COUNT() FROM Opportunity WHERE Profile_Completed__c = true]);
    }
}