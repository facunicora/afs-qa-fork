global without sharing class Afs_PreSelectedController {
    @AuraEnabled
    global static Afs_DisplayProgramSearchResultController.DisplayProgramWrapper getProgramOffered(String prId){
        map<String,String> valueMap = Afs_UtilityApex.getAccountCountryLabel();
        list<Program_Offer__c> prList = [SELECT id,Destinations__c,Program_Type__c,Tolabel(Length__c),Total_Cost_numeric__c,From__c,Program_Title__c,Currency__c FROM Program_Offer__c WHERE id =:prId];
        Afs_DisplayProgramSearchResultController.DisplayProgramWrapper displayWrapper;
        if(String.isNotBlank(prId)){
            displayWrapper = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(prList[0],'',false,1,false,false);
            displayWrapper.programObjDestination = (prList[0].Destinations__c != null ? valueMap.get(prList[0].Destinations__c) : '');
        }else{
            displayWrapper = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(new Program_Offer__c(),'',false,1,false,false);
            displayWrapper.programObjDestination = '';
        }
        return displayWrapper;
    }
    
    @AuraEnabled
    global static Afs_DisplayProgramSearchResultController.DisplayProgramWrapper getProgramOfferedAccepted(String prId){
        // Code added to get destinations translation
        map<String,String> valueMap = Afs_UtilityApex.getAccountCountryLabel();
        set<id> ContentDocumentIdSet = new set<id>();
        for(ContentDocumentLink cdlLink : [SELECT id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId =: prId]){
            ContentDocumentIdSet.add(cdlLink.ContentDocumentId);
        }
        list<ContentVersion> cvList = [SELECT id,FileType,VersionData,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: ContentDocumentIdSet AND (FileType = 'PNG' OR FileType = 'JPG' OR FileType = 'JPEG') ORDER BY ContentDocument.Title LIMIT 1];
        Afs_DisplayProgramSearchResultController.DisplayProgramWrapper displayWrapper;
        Program_Offer__c programObj = [SELECT id,Tolabel(Length__c),Name,To__c,From__c,Destinations__c,Durations__c,Program_Type__c,Total_Cost_numeric__c,Program_Title__c FROM Program_Offer__c WHERE id =:prId];
        if(!cvList.isEmpty()){
            displayWrapper = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(programObj,EncodingUtil.base64Encode(cvList[0].VersionData),false,1,false,false);
            displayWrapper.programObjDestination = (programObj.Destinations__c != null ? valueMap.get(programObj.Destinations__c) : '');
        }else{
            displayWrapper = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(programObj,'',false,1,false,false);
            displayWrapper.programObjDestination = (programObj.Destinations__c != null ? valueMap.get(programObj.Destinations__c) : '');
        }
        return displayWrapper;
    }
    
	@AuraEnabled
    global static Boolean isAppProcessed(String contactId, String applicationId){
        list<Program_Offer_in_App__c> programOfferList = [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c  FROM Program_Offer_in_App__c WHERE Application__c =: applicationId AND Contact__c =: contactId AND Is_the_applicant_Eligible__c = null];
		return programOfferList.isEmpty();
    }
    
    @AuraEnabled
    global static list<Afs_DisplayProgramSearchResultController.DisplayProgramWrapper> getProgramWrapper (String contactId, String applicationId){
        // Code added to get destinations translation
        map<String,String> valueMap = Afs_UtilityApex.getAccountCountryLabel();
        set<id> idSet = new set<id>();
        // ADd code to make Scholarship icon visible
        map <id,boolean> mapPrgmOffrvsIsScholarship = new map <id,boolean> ();
        
        list<Afs_DisplayProgramSearchResultController.DisplayProgramWrapper> wrapperList = new list<Afs_DisplayProgramSearchResultController.DisplayProgramWrapper> ();
        list<Application__c> appList = [SELECT id,Program_Offer__c,Status_App__c FROM Application__c WHERE id =: applicationId];
        String eligibility = System.label.AfsLbl_Application_Eligible;
        for(Program_Offer_in_App__c progrmDesired : [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c  FROM Program_Offer_in_App__c WHERE Application__c =: applicationId AND Contact__c =: contactId AND Is_the_applicant_Eligible__c =: eligibility]){
            idSet.add(progrmDesired.Program_Offer__c);
        }
        // ADd code to make Scholarship icon visible
        for(Sch_in_Program__c objSchProgram : [SELECT id,Scholarship__c,Program_Offer__c FROM Sch_in_Program__c WHERE Program_Offer__c IN: idSet]){
            mapPrgmOffrvsIsScholarship.put(objSchProgram.Program_Offer__c,true);
        } 
        
        set<id> ContentDocumentIdSet = new set<id>();
        map<id,id> mapContentDocumentIdandLink = new map<id,id>();
        map<id,ContentVersion> mapContentDocumentIdandVersion = new map<id,ContentVersion>();
        if(!idSet.isEmpty()){
            for(ContentDocumentLink documentLink: [SELECT id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN: idSet ORDER BY ContentDocument.Title]){
                if(!mapContentDocumentIdandLink.containsKey(documentLink.LinkedEntityId))
                    mapContentDocumentIdandLink.put(documentLink.LinkedEntityId,documentLink.ContentDocumentId);
                ContentDocumentIdSet.add(documentLink.ContentDocumentId);
            }
            
            for(ContentVersion cv : [SELECT id,FileType,VersionData,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: ContentDocumentIdSet]){
                if((cv.FileType.equalsIgnoreCase('PNG') || cv.FileType.equalsIgnoreCase('JPG') || cv.FileType.equalsIgnoreCase('JPEG'))){
                    mapContentDocumentIdandVersion.put(cv.ContentDocumentId,cv);
                }
                
            }
        }
        
        integer counterNum = 1;
        for(Program_Offer__c pOffered : [SELECT id,Name,To__c,From__c,Destinations__c,Program_Website_URL__c,Tolabel(Length__c),Durations__c,Program_Type__c,Total_Cost_numeric__c,Program_Title__c,Currency__c  FROM Program_Offer__c WHERE Show_in_portal__c = true AND id IN: idSet]){
            boolean isSelected = ((appList[0].Status_App__c == System.label.AfsLbl_Application_WaitListed) ? false : (appList[0].Program_Offer__c == pOffered.id)); 
            if(mapContentDocumentIdandLink.containsKey(pOffered.id)){
                if((appList[0].Status_App__c == System.label.AfsLbl_Application_WaitListed && appList[0].Program_Offer__c != pOffered.id)
                  || appList[0].Status_App__c != System.label.AfsLbl_Application_WaitListed){
                    if(mapContentDocumentIdandVersion.containsKey(mapContentDocumentIdandLink.get(pOffered.id))){
                    	Afs_DisplayProgramSearchResultController.DisplayProgramWrapper objWrapper = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(pOffered,EncodingUtil.base64Encode(mapContentDocumentIdandVersion.get(mapContentDocumentIdandLink.get(pOffered.id)).VersionData),isSelected,counterNum,false,false);
                        objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(pOffered.id) ? true : false;
                        objWrapper.programObjDestination = (pOffered.Destinations__c != null ? valueMap.get(pOffered.Destinations__c) : '');
                        wrapperList.add(objWrapper);
                    }else{
                        Afs_DisplayProgramSearchResultController.DisplayProgramWrapper objWrapper = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(pOffered,'',isSelected,counterNum,false,false);
                        objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(pOffered.id) ? true : false;
                        objWrapper.programObjDestination = (pOffered.Destinations__c != null ? valueMap.get(pOffered.Destinations__c) : '');
                        wrapperList.add(objWrapper);
                    }
                }
            }else{
                if((appList[0].Status_App__c == System.label.AfsLbl_Application_WaitListed && appList[0].Program_Offer__c != pOffered.id)
                  || appList[0].Status_App__c != System.label.AfsLbl_Application_WaitListed){
                    Afs_DisplayProgramSearchResultController.DisplayProgramWrapper objWrapper = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(pOffered,'',isSelected,counterNum,false,false);
                    objWrapper.isScholarshipPresent = mapPrgmOffrvsIsScholarship.containsKey(pOffered.id) ? true : false;
                    objWrapper.programObjDestination = (pOffered.Destinations__c != null ? valueMap.get(pOffered.Destinations__c) : '');
                    wrapperList.add(objWrapper);  
                }
            }
            
            counterNum++;
        }  
        
        
        return wrapperList;
    }
    
    @AuraEnabled
    global static String payAndSave (String contactId, String applicationId,String programOfferId){
        try{
            String eligibility = System.label.AfsLbl_Application_Eligible;
            String currencyToDisplay = '';
            list<Application__c> appList = [SELECT id,Program_Offer__c,Status_App__c FROM Application__c WHERE id =: applicationId];
            list<Program_Offer_in_App__c> programDesired = new list<Program_Offer_in_App__c>();
            for(Program_Offer_in_App__c progrmDesired : [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c,Program_Offer__r.Currency__c FROM Program_Offer_in_App__c WHERE Application__c =: applicationId AND Contact__c =: contactId AND Is_the_applicant_Eligible__c =: eligibility]){
                if(appList[0].Status_App__c == 'Wait-Listed' && progrmDesired.Desired__c){
                    progrmDesired.Is_the_applicant_Eligible__c = 'Ineligible';
                }
                progrmDesired.Desired__c = false;
                if(progrmDesired.Program_Offer__c == programOfferId){
                    progrmDesired.Desired__c = true;
                    currencyToDisplay = progrmDesired.Program_Offer__r.Currency__c;
                }
                
                programDesired.add(progrmDesired);
            }
            update programDesired;
            
            appList[0].Program_Offer__c = programOfferId;
            appList[0].Status_App__c = 'Pre-Selected';
            appList[0].Status__c = 'Decision';
            update appList;
            
            return currencyToDisplay;
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        
    }
    
    @AuraEnabled
    global static Application__c doChangeRequest(Application__c applicationObj){
        try{
            String contactId = [SELECT id,ContactId FROM User WHERE id=: UserInfo.getUserId()].ContactId;
            Program_Offer_in_App__c prOf = [SELECT id,Desired__c FROM Program_Offer_in_App__c WHERE Contact__c =: contactId AND Program_Offer__c =: applicationObj.Program_Offer__c];
            prOf.Desired__c = false;
            update prOf;
            
            applicationObj.Program_Offer__c = null;
            applicationObj.Status_App__c = null;
            applicationObj.Status__c = 'Interest';
            update applicationObj;
            
            //DELETE TO DO ITEMS
            list<To_Do_Item__c> toDoList = [SELECT id FROM To_Do_Item__c  WHERE Application__c =: applicationObj.id];
            if(toDoList != null && !toDoList.isEmpty()){
                DELETE toDoList;
            }
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        return applicationObj;
    } 
}