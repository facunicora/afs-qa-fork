public without sharing class Afs_App_Opportunity_SyncTrigger_Helper {
    public static List<Application__c> applicationNewList = new List<Application__c>();
    public static List<Application__c> applicationOldList = new List<Application__c>();
    public static Map<Id,Application__c> applicationNewMap = new Map<Id,Application__c>();
    public static  Map<Id,Application__c> applicationOldMap = new Map<Id,Application__c>();
	public static boolean byPassTrigger = false;  
    public static boolean isTriggerRunning = false;  
    
    
    public static void  Afs_App_Opportunity_SyncTrigger_SyncOpp(){
     try{ 
      isTriggerRunning = true;
      List<Opportunity> OpportunityListToUpsert = new  List<Opportunity>();
      List<Application__c> applicationListToUpdate = new  List<Application__c>();
      set<string> relationTypeSet = new set<string>{'Mother','Father'};
      set<Id> contactIdSet = new set<Id>(); 
      set<Id> opportunityIdSet = new set<Id>(); 
      Id RecordTypeID  = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Application').getRecordTypeId();
      List<OpportunityContactRole>  OpportunityContactRoleListToInsert = new  List<OpportunityContactRole>();
      List<OpportunityContactRole>  OpportunityContactRoleListToDelete = new  List<OpportunityContactRole>();
         
      map<Id,opportunity> contactIDToOpportunityMap =  new map<Id,opportunity>();  
        
      List<Application__c> applicationList = [select id,Name,Applicant__c,Applicant__r.Name,Applicant__r.Account.Name, Applicant__r.AccountId,Program_Offer__c,Program_Offer__r.Name,
                                              Owner.Name,Status__c,Starting_Date__c,Amount__c,Additional_destination_1__c,Additional_destination_2__c,
                                              Additional_destination_3__c,Cancellation_Reason__c,Cancellation_Reason_Other__c,What_are_you_looking_for__c,
                                              Payment_Plan_Status__c,Ending_Date__c,Total_Installments_to_be_paid__c,Total_Paid_01__c,
                                              Balance__c,Option_1_Interview_Date__c,Option_1_Time_preference__c,Option_2_Interview_Date__c,Option_2_Time_preference__c,
                                              Option_3_Interview_Date__c,Option_3_Time_preference__c,Opportunity__c,SUBMIT_YOUR_HEALTH_CERTIFICATE_BY_TEXT__c,
                                              SUBMIT_YOUR_ACADEMIC_HISTORY_TEXT__c,COMPLETE_YOUR_HOME_INTERVIEW_TEXT__c,INTRODUCE_YOURSELF_BY_TEXT__c,
                                              SCHOLARSHIP_DOCUMENTATION_TEXT__c,SIGN_PARTICIPATION_AGREEMENT_BY_TEXT__c,REVIEW_YOU_PAYMENT_TEXT__c,
                                              COMPLETE_YOUR_TRAVEL_INFORMATION_BY_TEXT__c,START_YOUR_LANGUAGE_TRAINING_TEXT__c,
                                              ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION_TE__c,SUBMIT_YOUR_HEALTH_UPDATE_FORM_TEXT__c,
                                              FILL_IN_YOUR_DOMESTIC_TRAVEL_INFO_TEXT__c,FILL_IN_YOUR_DOMESTIC_RETURN_INFO_TEXT__c,
                                              PAY_YOUR_APPLICATION_FEE_BY_TEXT__c,SELECTION_CAMP_BY_TEXT__c,SUBMIT_YOUR_HEALTH_CERTIFICATE_BY_DATE__c,
                                              SUBMIT_YOUR_ACADEMIC_HISTORY_DATE__c,COMPLETE_YOUR_HOME_INTERVIEW_DATE__c,
                                              INTRODUCE_YOURSELF_BY_DATE__c,SCHOLARSHIP_DOCUMENTATION_DATE__c,SIGN_PARTICIPATION_AGREEMENT_BY_DATE__c,
                                              REVIEW_YOU_PAYMENT_DATE__c,COMPLETE_YOUR_TRAVEL_INFORMATION_BY_DATE__c,START_YOUR_LANGUAGE_TRAINING_DATE__c,
                                              ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION_DA__c,SUBMIT_YOUR_HEALTH_UPDATE_FORM_DATE__c,
                                              FILL_IN_YOUR_DOMESTIC_TRAVEL_INFO_DATE__c,FILL_IN_YOUR_DOMESTIC_RETURN_INFO_DATE__c,
                                              PAY_YOUR_APPLICATION_FEE_BY_DATE__c,SELECTION_CAMP_BY_DATE__c,Camp_link_clicked__c,
                                              Vol_Interview_Required__c,Applications_Received_To_local__c, To_do_in_preapplication_completion__c,
                                              Program_Eligibility_Check__c, Program_Eligibility_Status__c
                                              from Application__c where Id in :applicationNewMap.keySet()];
         
        
        
       	
        for(Application__c  applicationRec :  applicationList){
            Opportunity opp = new Opportunity();
            if(applicationRec.Opportunity__c !=null){
                opportunityIdSet.add(applicationRec.Opportunity__c);
                opp.Id = applicationRec.Opportunity__c;
            }
           	opp.Name = applicationRec.Name +'-'+ applicationRec.Applicant__r.Name;
            opp.RecordTypeId = RecordTypeID ; 
            if(applicationRec.Applicant__r.AccountId !=null){
                opp.Account_Name_P__c = applicationRec.Applicant__r.Account.Name; 
            }
           	if(applicationRec.Program_Offer__c !=null){
                 opp.Program_Offer_P__c = applicationRec.Program_Offer__r.Name;
           }
            opp.Opportunity_Owner_P__c = applicationRec.Owner.Name;
            opp.StageName =applicationRec.Status__c;
            if(applicationRec.Starting_Date__c !=null ){
                 opp.CloseDate =  applicationRec.Starting_Date__c; 
            }else{
                 opp.CloseDate =  Date.newInstance(2050,01, 01);
            }
            opp.Amount = applicationRec.Amount__c;
            opp.Additional_destination_1__c = applicationRec.Additional_destination_1__c;
            opp.Additional_destination_2__c = applicationRec.Additional_destination_2__c;
            opp.Additional_destination_3__c = applicationRec.Additional_destination_3__c;
            opp.Cancellation_Reason__c = applicationRec.Cancellation_Reason__c ;
            opp.Cancellation_Reason_Other__c  = applicationRec.Cancellation_Reason_Other__c;
            opp.What_are_you_looking_for__c = applicationRec.What_are_you_looking_for__c;
            opp.Payment_Plan_Status__c = applicationRec.Payment_Plan_Status__c;
            opp.Ending_Date__c = applicationRec.Ending_Date__c;
            opp.Total_Installments_to_be_paid__c = applicationRec.Total_Installments_to_be_paid__c;
            opp.Total_Paid_01__c = applicationRec.Total_Paid_01__c;
            opp.Balance__c =applicationRec.Balance__c;
            opp.Option_1_Interview_Date__c = applicationRec.Option_1_Interview_Date__c;
			opp.Option_1_Time_preference__c = applicationRec.Option_1_Time_preference__c;
            opp.Option_2_Interview_Date__c = applicationRec.Option_2_Interview_Date__c;
			opp.Option_2_Time_preference__c = applicationRec.Option_2_Time_preference__c;
            opp.Option_3_Interview_Date__c = applicationRec.Option_3_Interview_Date__c;
			opp.Option_3_Time_preference__c = applicationRec.Option_3_Time_preference__c;
            opp.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY_TEXT__c = applicationRec.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY_TEXT__c;
            opp.SUBMIT_YOUR_ACADEMIC_HISTORY_TEXT__c = applicationRec.SUBMIT_YOUR_ACADEMIC_HISTORY_TEXT__c;
            opp.COMPLETE_YOUR_HOME_INTERVIEW_TEXT__c = applicationRec.COMPLETE_YOUR_HOME_INTERVIEW_TEXT__c;
            opp.INTRODUCE_YOURSELF_BY_TEXT__c = applicationRec.INTRODUCE_YOURSELF_BY_TEXT__c;
            opp.SCHOLARSHIP_DOCUMENTATION_TEXT__c = applicationRec.SCHOLARSHIP_DOCUMENTATION_TEXT__c;
            opp.SIGN_PARTICIPATION_AGREEMENT_BY_TEXT__c = applicationRec.SIGN_PARTICIPATION_AGREEMENT_BY_TEXT__c;
            opp.REVIEW_YOU_PAYMENT_TEXT__c = applicationRec.REVIEW_YOU_PAYMENT_TEXT__c;
            opp.COMPLETE_YOUR_TRAVEL_INFORMATION_BY_TEXT__c = applicationRec.COMPLETE_YOUR_TRAVEL_INFORMATION_BY_TEXT__c;
            opp.START_YOUR_LANGUAGE_TRAINING_TEXT__c = applicationRec.START_YOUR_LANGUAGE_TRAINING_TEXT__c;
            opp.ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION_TE__c = applicationRec.ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION_TE__c;
            opp.SUBMIT_YOUR_HEALTH_UPDATE_FORM_TEXT__c = applicationRec.SUBMIT_YOUR_HEALTH_UPDATE_FORM_TEXT__c;
            opp.FILL_IN_YOUR_DOMESTIC_TRAVEL_INFO_TEXT__c = applicationRec.FILL_IN_YOUR_DOMESTIC_TRAVEL_INFO_TEXT__c;
            opp.FILL_IN_YOUR_DOMESTIC_RETURN_INFO_TEXT__c = applicationRec.FILL_IN_YOUR_DOMESTIC_RETURN_INFO_TEXT__c;
            opp.PAY_YOUR_APPLICATION_FEE_BY_TEXT__c = applicationRec.PAY_YOUR_APPLICATION_FEE_BY_TEXT__c;
            opp.SELECTION_CAMP_BY_TEXT__c = applicationRec.SELECTION_CAMP_BY_TEXT__c;
            opp.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY_DATE__c = applicationRec.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY_DATE__c;
            opp.SUBMIT_YOUR_ACADEMIC_HISTORY_DATE__c = applicationRec.SUBMIT_YOUR_ACADEMIC_HISTORY_DATE__c;
            opp.COMPLETE_YOUR_HOME_INTERVIEW_DATE__c = applicationRec.COMPLETE_YOUR_HOME_INTERVIEW_DATE__c;
            opp.INTRODUCE_YOURSELF_BY_DATE__c = applicationRec.INTRODUCE_YOURSELF_BY_DATE__c;
            opp.SCHOLARSHIP_DOCUMENTATION_DATE__c = applicationRec.SCHOLARSHIP_DOCUMENTATION_DATE__c;
            opp.COMPLETE_YOUR_TRAVEL_INFORMATION_BY_DATE__c = applicationRec.COMPLETE_YOUR_TRAVEL_INFORMATION_BY_DATE__c;
            opp.SIGN_PARTICIPATION_AGREEMENT_BY_DATE__c = applicationRec.SIGN_PARTICIPATION_AGREEMENT_BY_DATE__c;
            opp.REVIEW_YOU_PAYMENT_DATE__c = applicationRec.REVIEW_YOU_PAYMENT_DATE__c;
            opp.START_YOUR_LANGUAGE_TRAINING_DATE__c = applicationRec.START_YOUR_LANGUAGE_TRAINING_DATE__c;
            opp.ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION_DA__c = applicationRec.ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION_DA__c;
            opp.SUBMIT_YOUR_HEALTH_UPDATE_FORM_DATE__c = applicationRec.SUBMIT_YOUR_HEALTH_UPDATE_FORM_DATE__c;
            opp.FILL_IN_YOUR_DOMESTIC_TRAVEL_INFO_DATE__c = applicationRec.FILL_IN_YOUR_DOMESTIC_TRAVEL_INFO_DATE__c;
            opp.FILL_IN_YOUR_DOMESTIC_RETURN_INFO_DATE__c = applicationRec.FILL_IN_YOUR_DOMESTIC_RETURN_INFO_DATE__c;
            opp.PAY_YOUR_APPLICATION_FEE_BY_DATE__c = applicationRec.PAY_YOUR_APPLICATION_FEE_BY_DATE__c;
            opp.SELECTION_CAMP_BY_DATE__c = applicationRec.SELECTION_CAMP_BY_DATE__c;
            opp.Camp_link_clicked__c = applicationRec.Camp_link_clicked__c;
            opp.Vol_Interview_Required__c = applicationRec.Vol_Interview_Required__c;
            opp.Applications_Received_To_local__c = applicationRec.Applications_Received_To_local__c;
            opp.npsp__Primary_Contact__c = applicationRec.Applicant__c;
            opp.AccountId = applicationRec.Applicant__r.AccountId;
            opp.Application_ID__c = applicationRec.Id;
            opp.Program_Offer__c = applicationRec.Program_Offer__c;
            opp.To_do_in_preapplication_completion_Check__c = applicationRec.To_do_in_preapplication_completion__c;
            opp.Program_Eligibility_Check_Text__c = applicationRec.Program_Eligibility_Check__c;
            opp.Program_Eligibility_Status__c = applicationRec.Program_Eligibility_Status__c;
            
            contactIDToOpportunityMap.put(applicationRec.Id,opp);
            contactIdSet.add(applicationRec.Applicant__c);
            applicationListToUpdate.add(applicationRec);
        }
         map<string,map<string,string>>   contactIdToRelationMap = new map<string,map<string,string>>();						
         List<contact> contactList = [select id ,(select Id,npe4__Type__c,npe4__RelatedContact__c from npe4__Relationships__r where npe4__Type__c IN : relationTypeSet)
                                      from contact  where Id IN  : contactIdSet] ;
         for(contact conRec :  contactList){
             contactIdToRelationMap.put(conRec.Id, new map<string,string>());
             for(npe4__Relationship__c  relationRec : conRec.npe4__Relationships__r){
                 contactIdToRelationMap.get(conRec.Id).put(relationRec.npe4__Type__c,relationRec.npe4__RelatedContact__c);
             }
         }	
          map<string,map<string,OpportunityContactRole>>   oppIdToContactRolesMap = new map<string,map<string,OpportunityContactRole>>();	
         
         
         List<Opportunity> OpportunityLIst = [Select Id,(Select Role,ContactId From OpportunityContactRoles) From Opportunity where
                                      id in  : opportunityIdSet] ; 
         
          for(Opportunity oppRec :  OpportunityLIst){
             oppIdToContactRolesMap.put(oppRec.Id, new map<string,OpportunityContactRole>());
             for(OpportunityContactRole  relationRec : oppRec.OpportunityContactRoles){
                 oppIdToContactRolesMap.get(oppRec.Id).put(relationRec.Role,relationRec);
             }
         }	
          system.debug('***********' + contactIDToOpportunityMap.values());
        if(contactIDToOpportunityMap.size() > 0){
            Afs_Opportunity_Trigger_Helper.allowOppUpdate = true;
            upsert contactIDToOpportunityMap.values(); 
        }
		        
        for(Application__c  applicationRec :  applicationListToUpdate){
            applicationRec.Opportunity__c =  contactIDToOpportunityMap.get(applicationRec.id).Id;
           	map<string,string>  relationToContactMap = contactIdToRelationMap.get(applicationRec.Applicant__c);
            
            if(oppIdToContactRolesMap.containsKey(applicationRec.Opportunity__c)){
                map<string,OpportunityContactRole>  relationToContactRoleMap = oppIdToContactRolesMap.get(applicationRec.Opportunity__c);
                    for(string itemType : relationToContactMap.keySet()){
                        if(relationToContactRoleMap.containsKey(itemType)){
                            OpportunityContactRole oppContactRole  =  relationToContactRoleMap.get(itemType);
                            oppContactRole.ContactId = relationToContactMap.get(itemType);
                            OpportunityContactRoleListToInsert.add(oppContactRole);
                         }else{
                             OpportunityContactRole oppContactRole = new OpportunityContactRole(OpportunityId = applicationRec.Opportunity__c,  role = itemType , ContactId =  relationToContactMap.get(itemType));
                      		 OpportunityContactRoleListToInsert.add(oppContactRole);
                         }
                    }
            }else{
                	for(string itemType : relationToContactMap.keySet()){
                        OpportunityContactRole oppContactRole = new OpportunityContactRole(OpportunityId = applicationRec.Opportunity__c, role = itemType , ContactId =  relationToContactMap.get(itemType));
                       OpportunityContactRoleListToInsert.add(oppContactRole);
                    }                	
            }
        }
         system.debug('***********' + OpportunityContactRoleListToInsert);
         if(OpportunityContactRoleListToInsert.size() > 0) {
             upsert OpportunityContactRoleListToInsert;
         }
         
        if(applicationListToUpdate.size() > 0){
            update applicationListToUpdate;
        }
     }catch(exception ex){
         
     }
	}
    
    
    public static void Afs_App_Opportunity_SyncTrigger_SyncOpp_Del(){
        set<Id> oppIdSet = new set<Id>();
        for(application__c appRec :  applicationOldList){
                oppIdSet.add(appRec.Opportunity__c);
        }
        if(oppIdSet.size() > 0){
            Afs_Opportunity_Trigger_Helper.allowOppUpdate = true;
            delete [select id from OpportunityContactRole where OpportunityId IN : oppIdSet];
            delete [select id from opportunity where Id IN : oppIdSet ];
        }
    }
    
    
    
}