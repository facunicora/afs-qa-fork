@isTest(SeeAllData=false)
public class Afs_ScholarshipApplicationTriggerTest {
    @testSetup
    public static void setup_test(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Afs_TestDataFactory.createProfilePhoto(1);
    }
    
    public static testMethod void insertScholershipApp(){
        Test.startTest();
        Scholarship__c sch = new Scholarship__c();
        sch.name = 'Test Scholarship';
        sch.Sending_Partner__c = [SELECT id FROM Account LIMIT 1].id;
        sch.Applicant_Eligibility_Specifications__c  = 'Academic Merit Based Candidates; Diversity Merit Based Candidates; Children Grandchildren of Employees; Grandchildren of Employees; Close Relatives of Employees; Children of Members; Grandchildren of Members; Close Relatives of Members; Children of Customers; Grandchildren of Customers';
        sch.Scholarship_Application_Deadline__c  = System.now().addDays(5);
        sch.Scholarship_Amount_Available__c = 'Partial 10%';
        sch.Min_GPA__c  = '12';
        insert sch;
        
        list<Application__c> applicationList = [SELECT id FROM Application__c];
        list<Contact> contactList = [SELECT id FROM Contact];
        list<Scholarship__c> schList = [SELECT id FROM Scholarship__c];
        Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Scholarship__c = schList[0].Id;
        schApp.Applicant__c = contactList[0].Id;
        schApp.application__C = applicationList[0].id;
        
        Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
        afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c = false;
        update afsTriggerSwitch;
        
        try{
            Insert schApp;
        }catch(Exception e){
            system.assertEquals(true, e != null);
        }
        Test.stopTest(); 
        system.assertEquals(false, afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c);
    }
    
    public static testMethod void updateScholershipExcpApp(){
        Test.startTest();
        Scholarship__c sch = new Scholarship__c();
        sch.name = 'Test Scholarship';
        sch.Sending_Partner__c = [SELECT id FROM Account LIMIT 1].id;
        sch.Applicant_Eligibility_Specifications__c  = 'Academic Merit Based Candidates; Diversity Merit Based Candidates; Children Grandchildren of Employees; Grandchildren of Employees; Close Relatives of Employees; Children of Members; Grandchildren of Members; Close Relatives of Members; Children of Customers; Grandchildren of Customers';
        sch.Scholarship_Application_Deadline__c  = System.now().addDays(5);
        sch.Scholarship_Amount_Available__c = 'Partial 10%';
        sch.Min_GPA__c  = '12';
        insert sch;
        
        list<Application__c> applicationList = [SELECT id FROM Application__c];
        list<Contact> contactList = [SELECT id FROM Contact];
        list<Scholarship__c> schList = [SELECT id FROM Scholarship__c];
        Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Scholarship__c = schList[0].Id;
        schApp.Applicant__c = contactList[0].Id;
        schApp.application__C = applicationList[0].id;
        Insert schApp;    
        Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
        afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c = false;
        update afsTriggerSwitch;
        
        list<Scholarship_Application__c> saList = [SELECT id,Application_Date__c FROM Scholarship_Application__c];
        try{
            UPDATE saList;
        }catch(Exception e){
            system.assertEquals(true, e != null);
        }
        Test.stopTest(); 
        system.assertEquals(false, afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c);
    }
    
    public static testMethod void updateScholershipApp(){
        Test.startTest();
        Scholarship__c sch = new Scholarship__c();
        sch.name = 'Test Scholarship';
        sch.Sending_Partner__c = [SELECT id FROM Account LIMIT 1].id;
        sch.Applicant_Eligibility_Specifications__c  = 'Academic Merit Based Candidates; Diversity Merit Based Candidates; Children Grandchildren of Employees; Grandchildren of Employees; Close Relatives of Employees; Children of Members; Grandchildren of Members; Close Relatives of Members; Children of Customers; Grandchildren of Customers';
        sch.Scholarship_Application_Deadline__c  = System.now().addDays(5);
        sch.Scholarship_Amount_Available__c = 'Partial 10%';
        sch.Min_GPA__c  = '12';
        insert sch;
        
        list<Application__c> applicationList = [SELECT id FROM Application__c];
        list<Contact> contactList = [SELECT id FROM Contact];
        list<Scholarship__c> schList = [SELECT id FROM Scholarship__c];
        Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Scholarship__c = schList[0].Id;
        schApp.Applicant__c = contactList[0].Id;
        schApp.application__C = applicationList[0].id;
        Insert schApp;    
        Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
        afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c = false;
        update afsTriggerSwitch;
        
        list<Scholarship_Application__c> saList = [SELECT id,Application_Date__c,Stage__c FROM Scholarship_Application__c];
        saList[0].Application_Date__c = Date.today().addDays(100);
        saList[0].Stage__c = 'Applying';
        UPDATE saList;
        
        Test.stopTest(); 
        system.assertEquals(false, afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c);
    }
    
    public static testMethod void DeleteScholershipApp(){
        Test.startTest();
        Scholarship__c sch = new Scholarship__c();
        sch.name = 'Test Scholarship';
        sch.Sending_Partner__c = [SELECT id FROM Account LIMIT 1].id;
        sch.Applicant_Eligibility_Specifications__c  = 'Academic Merit Based Candidates; Diversity Merit Based Candidates; Children Grandchildren of Employees; Grandchildren of Employees; Close Relatives of Employees; Children of Members; Grandchildren of Members; Close Relatives of Members; Children of Customers; Grandchildren of Customers';
        sch.Scholarship_Application_Deadline__c  = System.now().addDays(5);
        sch.Scholarship_Amount_Available__c = 'Partial 10%';
        sch.Min_GPA__c  = '12';
        insert sch;
        list<Application__c> applicationList = [SELECT id FROM Application__c];
        list<Contact> contactList = [SELECT id FROM Contact];
        list<Scholarship__c> schList = [SELECT id FROM Scholarship__c];
        Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Scholarship__c = schList[0].Id;
        schApp.Applicant__c = contactList[0].Id;
        schApp.application__C = applicationList[0].id;
        insert schApp;
        Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
        afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c = false;
        update afsTriggerSwitch;
        
        DELETE [SELECT id FROM Scholarship_Application__c];
        Test.stopTest(); 
        system.assertEquals(false, afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c);
    }
}