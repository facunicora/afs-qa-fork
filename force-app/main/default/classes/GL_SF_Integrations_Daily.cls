/* 
	Name: GL_SF_Integrations_Daily
	CreatedBy: Facundo Nicora
	Description: Created for contains all the logic for daily integrations.
*/
global class GL_SF_Integrations_Daily implements Schedulable{    
       
	global void execute(SchedulableContext sc) {
        //Execute Get Afs_Chapters
        AfsChapter_Upsert_Queueable chapterJob = new AfsChapter_Upsert_Queueable(1);
        ID jobChapterID = System.enqueueJob(chapterJob);
    }
}