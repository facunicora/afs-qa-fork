/*
 * Created by jotategui on 09/05/2018.
 */

public without sharing class ApplicationTriggerHandler {
    
    //Method contain logic for After Insert Event
    public static void afterInsert(Map<Id,Application__c> newMap){        
        createToDos(newMap,null,false);
        ApplicationTriggersHandler.createInstallments();
    }
    
    //Method contain logic for After Update Event
    public static void afterUpdate(Map<Id,Application__c> newMap, Map<Id,Application__c> oldMap){
        updateScholarshipPreApplicationFeeByProgramOffer(newMap.values(),oldMap); 
        createToDos(newMap,oldMap,true);
        ApplicationTriggersHandler.createInstallments();
        sendAppContactToGlobalLink(newMap.values(),oldMap);
        updateApplicantProfile(newMap.values());
        getTravelDataFromGL(newMap.values(), oldMap);
    }
    
    //Method contain logic for Before Insert Event
    public static void beforeInsert(List<Application__c> newList){
        //generateNewUUID(newList);
        CopyProgramStartDate(newList);
        calculateScore(newList, null, true, false);
    }
    
    //Method contain logic for Before Update Event
    public static void beforeUpdate(Map<Id,Application__c> newMap, Map<Id,Application__c> oldMap){
        CopyProgramStartDate(newMap.Values());
        calculateScore(newMap.values(), oldMap, false, false);
         cancelationDate(newMap.values());
    }
    
    //public static void createToDos(Map<Id, Application__c> applications, boolean isUpdate) {      
    public static void createToDos(Map<Id, Application__c> mapNew, Map<Id, Application__c> mapOld ,boolean isUpdate) {
        Map<Id, Application__c> applications =  new Map<Id, Application__c>();
        for(Application__c PO : mapNew.values()){
            if(!isUpdate && PO.Program_Offer__c != null || isUpdate && mapNew.get(PO.id).Program_Offer__c != mapOld.get(PO.id).Program_Offer__c){
                applications.put(PO.id, PO);
            }
        }
        
        if(applications.size() > 0){   
            if(isUpdate)
                TodoListHelper.deleteOldToDos(applications.keySet());
    
            set<id> ProgramOffersId = new Set<Id>();
    
            for (Application__c app : applications.values()) {
                if(app.Program_Offer__c != null){
                    ProgramOffersId.add(app.Program_Offer__c);
                }
            }
            
            if(ProgramOffersId.size() > 0){
                Map<Id, List<Master_To_Do__c>> MasterTodoGrouped = new Map<Id, List<Master_To_Do__c>>();
                for (Master_To_Do__c masterToDo : [
                        SELECT Name, Days__c, Dependant_Task__c, Comments__c,Program_Offer__c, Help__c,
                                Scholarship__c, Template__c, When__c, Description__c,
                                Stage_of_portal__c, Type__c, Link__c, To_Do_Label__c, RecordType.Name
                        from Master_To_Do__c
                        WHERE Program_Offer__c IN :ProgramOffersId
                ]) {
                    Id ProgramOfferId = masterToDo.Program_Offer__c;
                    if (!MasterTodoGrouped.containsKey(ProgramOfferId))
                        MasterTodoGrouped.put(ProgramOfferId, new List<Master_To_Do__c>());
                    MasterTodoGrouped.get(ProgramOfferId).add(masterToDo);
                }
                List<To_Do_Item__c> ToDoInsert = new List<To_Do_Item__c>();
                for (Application__c app : applications.values()) {
                    List<To_Do_Item__c> innerinserttodos = new List<To_Do_Item__c>();
                    Id ScholarshipId = null;
                    innerinserttodos = TodoListHelper.CreateItemsFromMaster(MasterTodoGrouped.get(app.Program_Offer__c), app, ScholarshipId);
                    for (To_Do_Item__c todo : innerinserttodos) {
                        ToDoInsert.add(todo);
                    }
                }
                insert ToDoInsert;
            }
        }
    }

    public static void CopyProgramStartDate(List<Application__c>  applications){

        Set<Id> ProgramOfferIds = new Set<Id>();

        for(Application__c app : applications){
            ProgramOfferIds.add(app.Program_Offer__c);
        }

        Map<id,Program_Offer__c> mapDates = new Map<id,Program_Offer__c>();
        if(applications != null && applications.size() > 0){
            for(Program_Offer__c po : [SELECT From__c, To__c FROM Program_Offer__c WHERE id in :ProgramOfferIds ]){
                mapDates.put(po.id,po);
            }
            for(Application__c app : applications){
                if(app.Program_Offer__c != null && mapDates.get(app.Program_Offer__c).From__c != null)
                    app.Program_Start_Date__c   = mapDates.get(app.Program_Offer__c).From__c;
                if(app.Program_Offer__c != null && mapDates.get(app.Program_Offer__c).To__c != null)
                    app.Program_End_Date__c     = mapDates.get(app.Program_Offer__c).To__c;

            }

        }

    }
    
    public static void calculateScore(List<Application__c> newList, Map<Id,Application__c> mapOld, Boolean isInsert, Boolean forceRecalculation){
        List<Application__c> lstAppToProcess = new List<Application__c>();
        Set<Id> setApplicationIds = new Set<Id>();
        Map<Id,Contact> mapContact = new Map<Id,Contact>();
        List<Scoring_Settings__c> lstApplicationScoringSettings = ScoringHandler.getListScoringSettingByObject('Application__c');     
        
        if(!lstApplicationScoringSettings.isEmpty()){
            for(Application__c app : newList){
                Application__c oldApp;
                if(!isInsert){
                    oldApp = mapOld.get(app.Id);
                    setApplicationIds.add(app.Id);
                }
                if(isInsert || forceRecalculation || ScoringHandler.anyFieldChange(app,oldApp,'Application__c')){
                       lstAppToProcess.add(app);
                       if(app.Applicant__c != null){
                           mapContact.put(app.Applicant__c,null);
                       }
               }
            }
            
            if(lstAppToProcess.Size() > 0){
                
                List<String> lstContactFields = ScoringHandler.getFieldsOnScoringSettingByObjectName('Contact');
                List<Scoring_Settings__c> lstContactScoringSettings = ScoringHandler.getListScoringSettingByObject('Contact');                
                
                if(!mapContact.isEmpty() && !lstContactFields.isEmpty()){
                    Set<Id> setIdContacts = mapContact.KeySet();
                    String queryContact = 'SELECT ' + String.join(lstContactFields,',') + ' FROM Contact WHERE Id in :setIdContacts';
                    for(Contact con : Database.query(queryContact)){
                        mapContact.put(con.Id,con);
                    }
                }
                
                Map<Id,Application__c> mapApplicationWithRelationshipsById = new Map<Id,Application__c>();
                if(!isInsert){
                    List<String> lstApplicationFields = ScoringHandler.getFieldsOnScoringSettingByObjectName('Application__c');
                    String queryApplication = 'SELECT Id,' + String.join(lstApplicationFields,',') + ' FROM Application__c WHERE Id in :setApplicationIds';
                    for(Application__c app : Database.query(queryApplication)){
                        mapApplicationWithRelationshipsById.put(app.Id,app);
                    }                
                }
                
                //Recalculte the Score
                for(Application__c app : lstAppToProcess){
                    //Recalculate the score from App Record
                    app.Score__c = ScoringHandler.recalculateScore(app, lstApplicationScoringSettings,100,'Application__c',mapApplicationWithRelationshipsById.get(app.Id));
                    //Add score from the contact record
                    if(app.Applicant__c != null && !mapContact.isEmpty() && !lstContactFields.isEmpty()){
                        app.Score__c = ScoringHandler.recalculateScore(mapContact.get(app.Applicant__c), lstContactScoringSettings,app.Score__c,'Contact',null);
                    }
                }
            }
        }
    }

    /**
     * Metodo que se ejecuta cuando se cambia el Program Offer del Application 
     * para recontar el campo de Scholarship Pre-Application Fee de sus Scholarships
     * relacionados.
     */  
    
    public static void updateScholarshipPreApplicationFeeByProgramOffer(List<Application__c> newApplicationList, Map<Id, Application__c> mapApplicationOldValues){
        List<Id> updateApplicationListId = new List<Id>();
        try{
            for(Application__c objApplication : newApplicationList){
                if(objApplication.Program_Offer__c != mapApplicationOldValues.get(objApplication.Id).Program_Offer__c && objApplication.Program_Offer__c != null){
                    updateApplicationListId.add(objApplication.Id);
                }
            }
            
            if(updateApplicationListId.size() > 0){
                sumPreApplicationFeeByListApp(updateApplicationListId);
            }
            
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
        }
    }
    
    /*
     * 
     * 
     * 
     */
    public static void sumPreApplicationFeeByListApp(List<Id> updateApplicationListId){
                List<Application__c> applicationWithSchApp = [SELECT Id,Program_Offer__c,Scholarship_Pre_Application_Fee__c,(SELECT Scholarship__c,Name,Id,Stage__c FROM Scholarship_Applications__r WHERE Stage__c != 'Lost') FROM Application__c WHERE Id IN: updateApplicationListId];
                List<Map<String,Object>> mapApplicationWScholarship = new List<Map<String,Object>>(); 
                List<Id> globalScholarshipAppId = new List<Id>();
                List<Id> globalProgramOfferId = new List<Id>();
                List<Application__c> applicationWithoutSchApp = new List<Application__c>();
                List<Application__c> applicationMaster = new List<Application__c>();
                for(Application__c objApplication : applicationWithSchApp){
                    Map<String,Object> mapObjApp = new Map<String,Object>();
                    if(objApplication.Scholarship_Applications__r.size() > 0){
                        List<Scholarship_Application__c> listSchApp = new List<Scholarship_Application__c>();
                        mapObjApp.put('applicationId',objApplication.Id);
                        mapObjApp.put('Program_Offer__c',objApplication.Program_Offer__c);
                        globalProgramOfferId.add(objApplication.Program_Offer__c);
                        for(Scholarship_Application__c objSchApp : objApplication.Scholarship_Applications__r){
                            listSchApp.add(objSchApp);
                            globalScholarshipAppId.add(objSchApp.Id);
                        }
                        mapObjApp.put('appSchIdList',listSchApp);
                        mapApplicationWScholarship.add(mapObjApp);
                    }else{
                        objApplication.Scholarship_Pre_Application_Fee__c = 0;
                        applicationWithoutSchApp.add(objApplication);
                    }
                }
                
                if(applicationWithoutSchApp.size() > 0){
                   applicationMaster.addall(applicationWithoutSchApp); 
                }
                if(mapApplicationWScholarship.size()>0){
                    List<Scholarship__c> scholarshipList = [SELECT Id,Rre_Application_Fee_Amount__c,(SELECT Id FROM Scholarship_Applications__r),(SELECT Program_Offer__c  FROM Sch_in_Program_Offers__r) FROM Scholarship__c WHERE Id IN (SELECT Scholarship__c FROM Sch_in_Program__c WHERE Program_Offer__c IN :globalProgramOfferId) AND Id IN (SELECT Scholarship__c FROM Scholarship_Application__c WHERE Id IN :globalScholarshipAppId)];
                    
                    List<Application__c> listFinalApplicant = new List<Application__c>();
                    Decimal counter = 0;
                    for(Map<String,Object> objMap : mapApplicationWScholarship){
                        List<Scholarship_Application__c> listSchAppId = (List<Scholarship_Application__c>)objMap.get('appSchIdList');
                        Id programOfferId = (Id)objMap.get('Program_Offer__c');
                        List<Scholarship__c> newObjSch = new List<Scholarship__c>();
                        for(Scholarship_Application__c SchApp : listSchAppId){
                            for(Scholarship__c objSch : scholarshipList){
                                for(Sch_in_Program__c objPoF : objSch.Sch_in_Program_Offers__r){
                                    if(objPoF.Program_Offer__c == programOfferId){
                                        newObjSch.add(objSch);
                                        break;
                                    }
                                }
                            }
                            for(Scholarship__c objSch : newObjSch){
                                List<Id> listId = new List<Id>();
                                for(Scholarship_Application__c litleSchApp : objSch.Scholarship_Applications__r){
                                    listId.add(litleSchApp.Id);
                                }
                                if(listId.contains(SchApp.Id) == true){
                                    counter = counter + objSch.Rre_Application_Fee_Amount__c;
                                    system.debug('counter: '+counter);                                    
                                    break;
                                }
                                
                            }
                        }
                        Application__c FinalApplicant = new Application__c(Id=(Id)objMap.get('applicationId'),Scholarship_Pre_Application_Fee__c=counter);
                        System.debug('FinalApplicant.id: ' + FinalApplicant.id);
                        listFinalApplicant.add(FinalApplicant);
                    }
                    applicationMaster.addall(listFinalApplicant);
                    
                }
        
                    update applicationMaster;
    }
    
    public static void sendAppContactToGlobalLink(List<Application__c> newList, Map<Id,Application__c> mapOld){
        Set<String> setStagesForApp = new Set<String>{'Decision','Confirmation','Purchase','Onboarding','Participation'};
        Set<String> setStagesForContact = new Set<String>{'Participation Desire','Decision','Confirmation','Purchase','Onboarding','Participation'};
        if(newList.Size() == 1){
            if((newList[0].Send_To_Global_Link__c && !mapOld.get(newList[0].Id).Send_To_Global_Link__c) || (setStagesForApp.contains(newList[0].Status__c) && newList[0].Status__c != mapOld.get(newList[0].Id).Status__c)){
                RemoteSiteManager.sendApplicationToGlobalLinkFuture(newList[0].Id);
                Application__c app = new Application__c(Id=newList[0].Id,Send_To_Global_Link__c=false);
                Update app;
            }
            if(newList[0].Status__c != mapOld.get(newList[0].Id).Status__c && setStagesForContact.contains(newList[0].Status__c)){
                RemoteSiteManager.sendPersonToGlobalLinkFuture(newList[0].Applicant__c);
            }
        }
    }
    
    //Method to retrieve from GL Travel Data of the application
    public static void getTravelDataFromGL(List<Application__c> newList, Map<Id,Application__c> mapOld){
       Set<String> setStagesForApp = new Set<String>{'Onboarding','Participation'};
       List<Application__c> lstAppsToUpdate = new List<Application__c>();
       
       //Filter Apps To Process
       for(Application__c app : newList){
            if((app.Get_Travel_Data_From_Global_Link__c && !mapOld.get(app.Id).Get_Travel_Data_From_Global_Link__c) || (setStagesForApp.contains(app.Status__c) && app.Status__c != mapOld.get(app.Id).Status__c)){
            	lstAppsToUpdate.add(app);
            }
       } 
       
       //enqueueJob to ger Data
       if(!lstAppsToUpdate.isEmpty()){
         	GetTravelData_Queueable que = new GetTravelData_Queueable(lstAppsToUpdate);
       		System.enqueueJob(que);   
       }
       
       
    }
    
    public static void generateNewUUID(List<Application__c> newList){    
        for(Application__c app : newList){            
            app.Global_Link_Participant_App_ID__c = RemoteSiteParser.generateNewUUID();
        }    
    }
    
    /**
     * Desc : Method to update applicant profile based on Status Field 
     *        Stamp Cancel Date on Application object
     * @param lstApplications : List of updated application 
     */
    public static void cancelationDate(List<Application__c> lstApplications){    
        for(Application__c objApp : lstApplications){            
            if(objApp.Status__c.equals('Cancelled')){
                objApp.Cancelled_Date__c = Date.today();
            }
        }    
    }
    
    /**
     * Desc : Method to update applicant profile based on Status Field 
     *        Stamp Cancel Date on Application object
     * @param lstApplications : List of updated application 
     */
    public static void updateApplicantProfile(list<Application__c> lstApplications){
        set<id> stApplicantId = new set<id> ();
        // Map to decide whether applicant
        // needs to be a member or just login
        map<id,Boolean> mapApplicantIdVsisCommunityMember = new map<id,Boolean> ();
        map<id,list<id>> mapProgramOfferIdvsApplicantIdLst = new map<id,list<id>> ();
        for(Application__c objApp : lstApplications){
            if(objApp.Status__c.equals('Decision')){
                mapApplicantIdVsisCommunityMember.put(objApp.Applicant__c,true);
            }else if(objApp.Status__c.equals('Cancelled')){
                mapApplicantIdVsisCommunityMember.put(objApp.Applicant__c,false);
            }
            // Map to store program offered
            // and Application ids 
            // used for logic regarding from__c date
            // condition
            if(String.isNotBlank(objApp.Program_Offer__c)){
                if(mapProgramOfferIdvsApplicantIdLst.containsKey(objApp.Program_Offer__c)){
                    list<id> applicantIds = mapProgramOfferIdvsApplicantIdLst.get(objApp.Program_Offer__c);
                    applicantIds.add(objApp.Applicant__c);
                    mapProgramOfferIdvsApplicantIdLst.put(objApp.Program_Offer__c,applicantIds);
                }else{
                    list<id> applicantIds = new list<id> ();
                    applicantIds.add(objApp.Applicant__c);
                    mapProgramOfferIdvsApplicantIdLst.put(objApp.Program_Offer__c,applicantIds);
                }
            }
        }
        
        // get program offered 
        if(!mapProgramOfferIdvsApplicantIdLst.isEmpty()){
            for(Program_Offer__c objPO : [SELECT id,From__c FROM Program_Offer__c 
                                          WHERE id IN: mapProgramOfferIdvsApplicantIdLst.keySet()]){
                if(objPO.From__c == Date.Today() && mapProgramOfferIdvsApplicantIdLst.containsKey(objPO.id)){
                    for(id applicantId : mapProgramOfferIdvsApplicantIdLst.get(objPO.id)){
                        mapApplicantIdVsisCommunityMember.put(applicantId,false); 
                    }
                }
            }
        }
        
        // Method called to update user profile
        if(System.IsBatch() == false && System.isFuture() == false){
        	ApplicationTriggerHandler.updateUserOnFuture(mapApplicantIdVsisCommunityMember);
        }
        else{
            ApplicationTriggerHandler.updateUser(mapApplicantIdVsisCommunityMember);
        }
    }
    
    /**
     * Desc : Future method to update user 
     *        used to update profile based on Application Status
     */
    
    public static void updateUser(map<id,Boolean> mapApplicantIdVsisCommunityMember){
        list<User> lstUser = new list<User> ();
        map<String,id> mapProfileNamevsId = new map<String,id>();
        for(Profile pr : [SELECT id,Name FROM Profile 
                          WHERE Name = 'Applicant Community Member Profile'
                          OR Name = 'Applicant Community Profile Login']){
            mapProfileNamevsId.put(pr.Name,pr.id);
        }
        for(User objUser : [SELECT id,ProfileId,ContactId FROM User WHERE ContactId IN: mapApplicantIdVsisCommunityMember.keySet()]){
            if(mapApplicantIdVsisCommunityMember.get(objUser.ContactId)){
                objUser.ProfileId = mapProfileNamevsId.get('Applicant Community Member Profile');
            }else{
                objUser.ProfileId = mapProfileNamevsId.get('Applicant Community Profile Login');
            }
            lstUser.add(objUser);
        }
        
        if(!lstUser.isEmpty()){
           update lstUser;
        }
    }
    @future
    public static void updateUserOnFuture(map<id,Boolean> mapApplicantIdVsisCommunityMember){
        ApplicationTriggerHandler.updateUser(mapApplicantIdVsisCommunityMember);
    }
}