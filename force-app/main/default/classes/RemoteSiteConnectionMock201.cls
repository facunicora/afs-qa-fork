@IsTest
global class RemoteSiteConnectionMock201 implements HttpCalloutMock {
	global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"testResponses": ["test 1", "test 2", "test 3", "test 4", "test 5"]}');
        response.setStatusCode(201);
        return response; 
    }
}