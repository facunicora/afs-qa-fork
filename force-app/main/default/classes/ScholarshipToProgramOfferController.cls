/* @Autor             Facundo Nicora  
   @Dia de Creacion   07/08/2018 
   @Descripcion       Controller  del componente ScholarshipToProgramOffer*/
public class ScholarshipToProgramOfferController {
    public class wrapperProgToOffer{
        @AuraEnabled
        public boolean checked;
        @AuraEnabled
        public String name;
        @AuraEnabled
        public Id ident;
        @AuraEnabled
        public String hostingPartner;
        @AuraEnabled
        public String programType;
        @AuraEnabled
        public String recordType;
        
        wrapperProgToOffer(Program_Offer__c po){
            this.checked = false;
            this.name = po.Name;
            this.ident = po.id;
            this.hostingPartner = po.Hosting_Program__r.Host_Partner__r.Name;
            this.programType = po.Program_Type__c;
            this.recordType = po.RecordType.Name;
        }
    }
    
    //Metodo que recupera los program offers a mostrar como elegibles
	@AuraEnabled
    public static List <wrapperProgToOffer> getProgram_Offers(Id recordId) {
        List<wrapperProgToOffer> listWrapper = new List<wrapperProgToOffer>();
        Set<Id> setIdProgramOffer = new Set<Id>();
        //Recupero Info del User
        Id userId = UserInfo.getUserId();
        User usr = [SELECT IOC_Code__c FROM User WHERE Id = :userId];
        
        //Recupero scholarship actual
        Scholarship__c sch = [SELECT Sending_Partner__c FROM Scholarship__c WHERE id = :recordId];
        
        //Set que contiene los id de program offers ya asociados a un scholarship
        for(Sch_in_Program__c sip : [SELECT Program_Offer__c FROM Sch_in_Program__c WHERE Scholarship__c = :recordId]){
            setIdProgramOffer.add(sip.Program_Offer__c);
        }
        
        //Recupero los program offers y si aún no estan asociados los agrego a las lista a mostrar
        for(Program_Offer__c po : [SELECT Id, Name, Hosting_Program__r.Host_Partner__r.Name, Program_Type__c, RecordType.Name 
                                   FROM Program_Offer__c WHERE Sending_Partner__c = :sch.Sending_Partner__c
                                  						 AND Applications_Received_To_local__c > TODAY
                                  						 AND Id NOT IN :setIdProgramOffer
                                  						 AND Host_IOC__c = :usr.IOC_Code__c]){
        	wrapperProgToOffer wrapper = new wrapperProgToOffer(po);
            listWrapper.add(wrapper);
        }
        
        return listWrapper;
    }
    
    //Metodo que crea los registros del junction object
    @AuraEnabled
    public static List<Sch_in_Program__c> createRecords(String strJSONIds, Id recordId) {
        List<Sch_in_Program__c> listJunction = new List<Sch_in_Program__c>();
        List<Id> setIdProgramOffer = (List<Id>)System.JSON.deserialize(strJSONIds,List<Id>.class);

        //Creo los registros del junction object
        for(Id idProgOffer: setIdProgramOffer){
        		Sch_in_Program__c newJunctionRec = new Sch_in_Program__c();
                newJunctionRec.Scholarship__c = recordId;
                newJunctionRec.Program_Offer__c = idProgOffer;
                listJunction.add(newJunctionRec);
        }
        
        if(!listJunction.isEmpty()){
            try{
                Insert listJunction;
            }catch(Exception ex){
            }                
        }
        
        return listJunction;
    }
}