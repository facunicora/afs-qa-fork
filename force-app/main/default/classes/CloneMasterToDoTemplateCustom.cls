public class CloneMasterToDoTemplateCustom {
	@AuraEnabled
    public static Master_To_Do_Template__c newMasterToDoTemplate(String recordName, Id recordId) {
        Boolean response = true;
        Master_To_Do_Template__c parent;
        List<Master_To_Do__c> childRecords = new List<Master_To_Do__c>();
        
        try{
        
        List<Master_To_Do_Template__c> parentRecords;
            
        String resultQuery = getInfoRecords('Master_To_Do_Template__c','id=\''+recordId+'\'');
        parentRecords = Database.query(resultQuery);
        parent = parentRecords[0].clone(false, true, false, false);
        parent.Name = recordName;
        
        insert parent;
 
        String resultChilds = getInfoRecords('Master_To_Do__c','Template__c=\''+recordId+'\'');
        for (Master_To_Do__c child:(List<Master_To_Do__c>)Database.query(resultChilds)) {
                childRecords.add(child.clone(false,true,false,false));
            }
            
        // If there isn't any children ends the process and return success
        if (!childRecords.isEmpty()) {
            // Set the parent's Id
           for (Master_To_Do__c child : childRecords) {
               child.Template__c = parent.Id;
           }     
           insert childRecords;
        }
            
        }catch(Exception ex) {
            response = false;
            system.debug('CloneMasterToDoTemplateCustom.newMasterToDoTemplate -->'+ex);
            throw ex;
        }

           return parent; 
        
    }
    
    @AuraEnabled
    public static String getInfoRecords(String objectName, String condition) {
        try{
            Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
            String selects = '';
            list<string> selectFields = new list<string>();

            if (fMap != null){
                for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                    Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                    if (fd.isCreateable()){ // field is creatable
                        selectFields.add(fd.getName());
                    }
                }
            }
            
            if (!selectFields.isEmpty()){
                for (string s:selectFields){
                    selects += s + ',';
                }
                if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
            }
            string resultQuery =  'SELECT ' + selects + ' FROM '+ objectName +' WHERE '+condition;
            
            return resultQuery;
            
        }catch(Exception ex) {
            system.debug('CloneMasterToDoTemplateCustom.cloneRelatedRecords -->'+ex);
            throw ex;
        }
    }
}