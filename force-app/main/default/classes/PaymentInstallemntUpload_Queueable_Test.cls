@isTest
public class PaymentInstallemntUpload_Queueable_Test {
    static testmethod void testQueueable() {
  		AFS_Triggers_Switch__c swi = Util_test.create_AFS_Triggers_Switch('Afs_Trigger',false,true);
        Account acc = new Account(name= 'acc');
        acc.IOC_Code__c = 'Test';
        insert acc;
        
        Contact con = Util_Test.createContact('Test con',acc);
        insert con;
        
        Hosting_Program__c  hp = Util_Test.create_Hosting_Program(acc);
        
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordType = [Select Id From RecordType Where DeveloperName = 'Minors']; //flagship
        po.Name = 'Test';
		insert po;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Test opp';
        opp.StageName = 'test';
        opp.CloseDate = date.today();
        insert opp;
              
        Application__c app = Util_Test.createApplication(con, po);
        app.Opportunity__c = opp.id;        
        insert app;
        
        Payment_Installment__c pins = new Payment_Installment__c();
        pins.Due_Date__c = date.today();
        pins.Number__c = 4000;      
        pins.Amount__c = 4000;
        pins.Application__c = app.id;
        insert pins;
        
		PaymentInstallemntUpload_Queueable Upload = new PaymentInstallemntUpload_Queueable();
        Test.startTest();        
        System.enqueueJob(Upload);
        Test.stopTest();
        
        List<Opportunity> opp2 = [SELECT Due_Date_Payment_Installment__c, Number_of_Payment_Installment__c, Ammount_of_Payment_Installment__c FROM Opportunity];
        system.assertNotEquals(opp.Due_Date_Payment_Installment__c, opp2[0].Due_Date_Payment_Installment__c, 'Error in update the field Due_Date_Payment_Installment__c ');
        system.assertNotEquals(opp.Number_of_Payment_Installment__c, opp2[0].Number_of_Payment_Installment__c,'Error in update the field Number_of_Payment_Installment__c ');
        system.assertNotEquals(opp.Ammount_of_Payment_Installment__c, opp2[0].Ammount_of_Payment_Installment__c,'Error in update the field Ammount_of_Payment_Installment__c ');
        //List<AsyncApexJob> asyncJobList = [select id from AsyncApexJob];
		//System.assertEquals(1,asyncJobList.size(),'One Job should have been enqueued'); 
    }
}