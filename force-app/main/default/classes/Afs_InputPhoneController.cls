global without sharing class Afs_InputPhoneController {
	@AuraEnabled
	global static List<String> getselectOptions() {
	 AFS_Community_Configuration__c objObject = new AFS_Community_Configuration__c();
	 system.debug(objObject);
	 List<String> allOpts = new list<String>();
	 // Get the object type of the SObject.
	 Schema.SObjectType objType = objObject.getSObjectType();
	 system.debug(objType);	
	  // Describe the SObject using its object type.
	 Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
	 
	 // Get a map of fields for the SObject
	 map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
	 
	 // Get the list of picklist values for this field.
	 list <Schema.PicklistEntry> values =
	  fieldMap.get('Default_Phone_Country_Code__c').getDescribe().getPickListValues();
	 
	  // Add these values to the selectoption list.
	 for (Schema.PicklistEntry a: values) {
	   allOpts.add(a.getValue());
	  }
	  system.debug('allOpts ---->' + allOpts);
	  allOpts.sort();
	  
	  return allOpts;
	 }   
}