public class ContactTriggerHandler {
    //Method contains logic for afterUpdate Event
    public static void afterUpdate(Map<Id,Contact> mapOld,Map<Id,Contact> mapNew){
    	calculateScore(mapNew.values(),mapOld);
    }    
   
    public static void calculateScore(List<Contact> newList, Map<Id,Contact> mapOld){
        List<Contact> lstConToProcess = new List<Contact>();
        Map<Id,List<Application__c>> mapListApplicationByConId = new Map<Id,List<Application__c>>();
        List<Scoring_Settings__c> lstContactScoringSettings = ScoringHandler.getListScoringSettingByObject('Contact');     
        
        if(!lstContactScoringSettings.isEmpty()){
        for(Contact con : newList){
            Contact oldCon = mapOld.get(con.Id);
               if(ScoringHandler.anyFieldChange(con,oldCon,'Contact')){
                       lstConToProcess.add(con);
                       mapListApplicationByConId.put(con.Id,new List<Application__c>());
               }
            }
            
            if(lstConToProcess.Size() > 0){
                
                List<Application__c> lstApplicationsToUpdate = new List<Application__c>();
                List<String> lstApplicationFields = ScoringHandler.getFieldsOnScoringSettingByObjectName('Application__c');
                List<Scoring_Settings__c> lstApplicationScoringSettings = ScoringHandler.getListScoringSettingByObject('Application__c'); 
                
                if(!lstApplicationFields.isEmpty()){
                    Set<Id> setContactIds = mapListApplicationByConId.keySet();
					String queryApplications = 'SELECT Applicant__c, ' + String.join(lstApplicationFields,',') + ' FROM Application__c WHERE Applicant__c in :setContactIds';
                    for(Application__c app : DataBase.query(queryApplications)){
                        mapListApplicationByConId.get(app.Applicant__c).add(app);
                        lstApplicationsToUpdate.add(app);
                    }
                }
                
                //Recalculte the Score
                for(Contact con : lstConToProcess){
                    if(!mapListApplicationByConId.get(con.Id).isEmpty()){
                        //Recalculate the score from Con Record
                        Decimal contactScore = ScoringHandler.recalculateScore(con, lstContactScoringSettings,100,'Contact',null);
                        //Add score from the Application record
                        for(Application__c app : mapListApplicationByConId.get(con.Id)){
                            app.Score__c = ScoringHandler.recalculateScore(app, lstApplicationScoringSettings,contactScore,'Application__c',null);
                        }
                    }
                }
                
                if(!lstApplicationsToUpdate.isEmpty()){
                    Database.Update(lstApplicationsToUpdate);
                }
            }
        }
    }  
}