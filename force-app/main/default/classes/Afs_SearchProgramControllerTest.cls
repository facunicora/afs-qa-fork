@isTest
public class Afs_SearchProgramControllerTest {
    
    @isTest
    static void getselectOptionsTest() {  
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        list<Program_Offer__c> prList = new list<Program_Offer__c>();
        date dt = Date.today().addDays(40);
        date dt1 = Date.today().addDays(-40);
        for(Program_Offer__c prObj : [Select id,Applications_Received_To_local__c,Applications_Received_From_local__c, Length__c from Program_Offer__c]){
            prObj.Applications_Received_To_local__c = dt;
            prObj.Applications_Received_From_local__c = dt1;
            prObj.Length__c = '3 weeks';
            prList.add(prObj);
        }
        prList[1].Length__c= '1 month';
        prList[2].Length__c= '2 weeks';
        

        update prList;
        system.assertEquals(true,Afs_SearchProgramController.getselectOptions().get('Destinations__c').Size() > 0);
        Test.stopTest();
    }
}