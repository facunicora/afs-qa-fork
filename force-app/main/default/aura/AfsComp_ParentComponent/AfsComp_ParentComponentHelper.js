({
	onInit : function(component, event) {
		component.set("v.Spinner","true");
		var action = component.get("c.getConfiguration");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.configWrapper",response.getReturnValue());
                var configObj = response.getReturnValue();
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Name__c,component);
                component.set("v.Name",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_FirstName__c,component);
                component.set("v.FirstName",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_LastName__c,component);
                component.set("v.LastName",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Email__c,component);
                component.set("v.Email",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_EmailPlaceHolder__c,component);
                component.set("v.EmailPlaceHolder",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_DOB__c,component);
                component.set("v.DOB",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_DOBHelpTxt__c,component);
                component.set("v.DOBHelpTxt",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_AlreadyApplied__c,component);
                component.set("v.AlreadyApplied",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Login__c,component);
                component.set("v.LogIn",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_ZipCode__c,component);
                component.set("v.ZipCode",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_ZipCodeHelpTxt__c,component);
                component.set("v.ZipCodeHelpTxt",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Mobile__c,component);
                component.set("v.Mobile",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_MobileHelpTxt__c,component);
                component.set("v.MobileHelpTxt",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_PromoCode__c,component);
                component.set("v.PromoCode",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_PromoCodeHelpTxt__c,component);
                component.set("v.PromoCodeHelpTxt",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_HearAboutUs__c,component);
                component.set("v.HearAboutUs",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Btn_SignUp__c,component);
                component.set("v.SignMeUp",component.get("v.LabelTempVal"));
                // Changes made to add language selector
                this.doGetLanguageList(component,event);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
	
	onLanguageChange : function(component, event) {
            var action = component.get("c.updateUserLanguage");
            var languageSelected = component.get("v.languageSelected");
            
            action.setParams({
                "selectedLanguage" : languageSelected
            });
            
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    location.reload(true);
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    var messageTemplate = response.getError()[0].message;
                    toastEvent.setParams({ 
                        type : "error",
                        duration : 5000,
                        message: messageTemplate
                    });
                    component.set("v.Spinner",false);
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
		
	},
	
	doGetLanguageList : function(component, event) {
		var action = component.get("c.getLanguageList");
        var configObj = component.get("v.configWrapper");
       	
        action.setParams({
            "selectedLanguageCode" : configObj.Afs_CommunityLanguages__c
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
            	if(response.getReturnValue() != undefined && response.getReturnValue() != []){
            		component.set("v.isLanguagesAvailable", false);
            	}
                component.set("v.languageList",response.getReturnValue());
                component.set("v.Spinner",false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                component.set("v.Spinner",false);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getProfilePic : function(component, event) {
		var action = component.get("c.getDP");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.imageUrl",response.getReturnValue());
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getNavigateWrapper : function(component, event) {
		var action = component.get("c.getNavigateWrapper");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                
                var con = component.get("v.ContactInfo");
                var applicationWrapper = component.get("v.applicationWrapper");
                if(con){
                    if(con.Terms_Service_Agreement__c){
                        response.getReturnValue().isSelectProgram = true;
                        response.getReturnValue().isSelectProgramSideBar = true;
                        
                        if(applicationWrapper){
                            if(applicationWrapper.applicationObj.Status__c == "Interest"){
                                response.getReturnValue().isSelectProgram = true;
                                response.getReturnValue().isSelectProgramSideBar = true;
                                response.getReturnValue().isProgramDetail = false;
                                response.getReturnValue().isProgramDetailSideBar = false;
                                response.getReturnValue().isAboutYou = false;
                                response.getReturnValue().isAboutYouSideBar = false;
                                response.getReturnValue().isPortalSubmissionConfirmation =false;
                                response.getReturnValue().isPortalSubmissionConfirmationSideBar = false;
                                response.getReturnValue().isPreSelected = false;
                                response.getReturnValue().isPreSelectedSideBar = false;
                                response.getReturnValue().isWaitlisted = false;
                            }else if(applicationWrapper.applicationObj.Status__c == "Product Desire"){
                                response.getReturnValue().isSelectProgram = true;
                                response.getReturnValue().isSelectProgramSideBar = true;
                                response.getReturnValue().isProgramDetail = false;
                                response.getReturnValue().isProgramDetailSideBar = false;
                                response.getReturnValue().isAboutYou = false;
                                response.getReturnValue().isAboutYouSideBar = false;
                                response.getReturnValue().isPortalSubmissionConfirmation =false;
                                response.getReturnValue().isPortalSubmissionConfirmationSideBar = false;
                                response.getReturnValue().isPreSelected = false;
                                response.getReturnValue().isPreSelectedSideBar = false;
                                response.getReturnValue().isWaitlisted = false;
                            }else if(applicationWrapper.applicationObj.Status__c == "Participation Desire"){
                                response.getReturnValue().isSelectProgram = false;
                                response.getReturnValue().isSelectProgramSideBar = true;
                                response.getReturnValue().isProgramDetail = false;
                                response.getReturnValue().isProgramDetailSideBar = true;
                                response.getReturnValue().isAboutYou = false;
                                response.getReturnValue().isAboutYouSideBar = true;
                                response.getReturnValue().isPortalSubmissionConfirmation = true;
                                response.getReturnValue().isPortalSubmissionConfirmationSideBar = true;
                                response.getReturnValue().isPreSelected = false;
                                response.getReturnValue().isPreSelectedSideBar = false;
                                response.getReturnValue().isWaitlisted = false;
                            }else if(applicationWrapper.applicationObj.Status__c == "Decision" || applicationWrapper.applicationObj.Status__c == "Confirmation"){
                                response.getReturnValue().isSelectProgram = false;
                                response.getReturnValue().isSelectProgramSideBar = true;
                                response.getReturnValue().isProgramDetail = false;
                                response.getReturnValue().isProgramDetailSideBar = true;
                                response.getReturnValue().isAboutYou = false;
                                response.getReturnValue().isAboutYouSideBar = true;
                                response.getReturnValue().isPortalSubmissionConfirmation =false;
                                response.getReturnValue().isPortalSubmissionConfirmationSideBar = true;
                                response.getReturnValue().isPreSelected = true;
                                response.getReturnValue().isPreSelectedSideBar = true;
                                response.getReturnValue().isWaitlisted = false;
                                if(applicationWrapper.applicationObj.Status_App__c == $A.get("$Label.c.AfsLbl_Application_WaitListed")){
                                    response.getReturnValue().isWaitlisted = true;
                                }
                                
                                
                            }else if(applicationWrapper.applicationObj.Status__c == "Purchase"){
                                response.getReturnValue().isSelectProgram = false;
                                response.getReturnValue().isSelectProgramSideBar = true;
                                response.getReturnValue().isProgramDetail = false;
                                response.getReturnValue().isProgramDetailSideBar = true;
                                response.getReturnValue().isAboutYou = false;
                                response.getReturnValue().isAboutYouSideBar = true;
                                response.getReturnValue().isPortalSubmissionConfirmation =false;
                                response.getReturnValue().isPortalSubmissionConfirmationSideBar = true;
                                response.getReturnValue().isPreSelected = false;
                                response.getReturnValue().isPreSelectedSideBar = true;
                                response.getReturnValue().isWaitlisted = false;
                                response.getReturnValue().isAccepted = true;
                                response.getReturnValue().isAcceptedSideBar = true;
                            }else if(applicationWrapper.applicationObj.Status__c == "Onboarding"){
                                response.getReturnValue().isSelectProgram = false;
                                response.getReturnValue().isSelectProgramSideBar = true;
                                response.getReturnValue().isProgramDetail = false;
                                response.getReturnValue().isProgramDetailSideBar = true;
                                response.getReturnValue().isAboutYou = false;
                                response.getReturnValue().isAboutYouSideBar = true;
                                response.getReturnValue().isPortalSubmissionConfirmation =false;
                                response.getReturnValue().isPortalSubmissionConfirmationSideBar = true;
                                response.getReturnValue().isPreSelected = false;
                                response.getReturnValue().isPreSelectedSideBar = true;
                                response.getReturnValue().isWaitlisted = false;
                                response.getReturnValue().isAccepted = true;
                                response.getReturnValue().isAcceptedSideBar = true;
                            }
                        }
                    }
                }
                
                
                component.set("v.navigateWrapper",response.getReturnValue());
                component.set("v.Spinner","false");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getContact : function(component, event) {
		var action = component.get("c.getContactRecord");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.ContactInfo",response.getReturnValue());
                if(response.getReturnValue().Terms_Service_Agreement__c){
                    component.set("v.isSessionStarted","true");
                }
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getApplication : function(component, event) {
		var action = component.get("c.getApplicationRecord");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.applicationWrapper",response.getReturnValue());
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getLoginWrapper : function(component, event) {
		var action = component.get("c.getNewLoginWrapper");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
            	var loginWrapper = response.getReturnValue();
            	loginWrapper.AgreeTerms = true;
                component.set("v.loginWrapper", loginWrapper);
                component.set("v.initialLoad",true);
                component.set("v.languageSelected",response.getReturnValue().languageSelected);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    onUpdateUserContact : function(component, event) {
		var action = component.get("c.updateUserAndContact");
        var contactObj = component.get("v.ContactInfo");
        var wrapperObj = component.get("v.loginWrapper");
        var urlString = "https://" + window.location.hostname;
        contactObj.FirstName = wrapperObj.FirstName;
        contactObj.LastName = wrapperObj.LastName;
        contactObj.Email  = wrapperObj.Email;
        contactObj.Birthdate = wrapperObj.DOB;
        contactObj.MiddleName = wrapperObj.MiddleName;
        contactObj.MailingPostalCode = wrapperObj.ZipCode;
        contactObj.MobilePhone = wrapperObj.MobileNumber;
        contactObj.How_did_you_hear_about_AFS__c = wrapperObj.HearAboutUs;
        contactObj.Terms_Service_Agreement__c = wrapperObj.AgreeTerms;
        contactObj.Keep_me_informed_Opt_in__c = wrapperObj.KeepMeInformed;
        action.setParams({
            "conObj" : contactObj,
            "languageSelected" : wrapperObj.languageSelected
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                location.reload(true);
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                component.set("v.Spinner","false");
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    }
})