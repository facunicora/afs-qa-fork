({
	getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    fetchPickListRadioVal: function(component, fieldName) {
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "objObject": "Health_Information__c",
            "fld": fieldName,
            "isRadioList" : true
        });
        var opts=[];
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var allValues = response.getReturnValue();
                component.set("v.Allergies", allValues['Allergies__c']);
                component.set("v.Allergies_Affects", allValues['Allergies_Affects__c']);
                component.set("v.Allergies_EpiPen", allValues['Allergies_EpiPen__c']);
                component.set("v.Allergic_hairless_or_hypoallergenic_Pets", allValues['Allergic_hairless_or_hypoallergenic_Pets__c']);
                component.set("v.Allergies_Pets", allValues['Allergies_Pets__c']);
                component.set("v.Allergies_medications", allValues['Allergies_medications__c']);
                component.set("v.Allergies_Pet_free_room", allValues['Allergies_Pet_free_room__c']);
                component.set("v.Allergies_special_considerations", allValues['Allergies_special_considerations__c']);
                component.set("v.Asthma", allValues['Asthma__c']);
                component.set("v.Asthma_Affect_Daily_Life", allValues['Asthma_Affect_Daily_Life__c']);
                component.set("v.Asthma_Medications", allValues['Asthma_Medications__c']);
                component.set("v.Asthma_Special_Considerations", allValues['Asthma_Special_Considerations__c']);
                component.set("v.Celiac_Disease", allValues['Celiac_Disease__c']);
                component.set("v.Celiac_Consume_Food_From_Same_Plate_Ut", allValues['Celiac_Consume_Food_From_Same_Plate_Ut__c']);
                component.set("v.Celiac_Consume_Food_From_Same_Pot", allValues['Celiac_Consume_Food_From_Same_Pot__c']);
                component.set("v.Celiac_Hospitalization", allValues['Celiac_Hospitalization__c']);
				component.set("v.Celiac_Hospitalization_History", allValues['Celiac_Hospitalization_History__c']);
                component.set("v.Celiac_Limit_Physical_Activities", allValues['Celiac_Limit_Physical_Activities__c']);
                component.set("v.Celiac_Medications", allValues['Celiac_Medications__c']);
                component.set("v.Celiac_Visit_Doctor", allValues['Celiac_Visit_Doctor__c']);
                component.set("v.Celiac_Reaction", allValues['Celiac_Reaction__c']);   
                component.set("v.Dental_care_on_program", allValues['Dental_care_on_program__c']);
                component.set("v.Diabetes", allValues['Diabetes__c']);
                component.set("v.Diabetes_Need_to_visit_doctor", allValues['Diabetes_Need_to_visit_doctor__c']);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false); 
            }  
            
        });
        $A.enqueueAction(action);
    } ,
    
    fetchPickListVal: function(component, fieldName) {
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "objObject": "Health_Information__c",
            "fld": fieldName,
            "isRadioList" : false
        });
        var opts=[];
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var allValues = response.getReturnValue();
                component.set("v.Allergies_Seasonality", allValues['Allergies_Seasonality__c']);
                component.set("v.Allergies_Severity", allValues['Allergies_Severity__c']);
                component.set("v.Asthma_Live_With_Pets", allValues['Asthma_Live_With_Pets__c']);
                component.set("v.Asthma_Pet_Free_Room", allValues['Asthma_Pet_Free_Room__c']);
                component.set("v.Asthma_Severity", allValues['Asthma_Severity__c']);
                component.set("v.Asthma_Symptoms", allValues['Asthma_Symptoms__c']);
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false); 
            }  
            
        });
        $A.enqueueAction(action);
    } ,
    
    fetchHealthInforRecord : function(component, fieldName) {
        var action = component.get("c.getHealthInfoRecord");
        var ContactInfo = component.get("v.ContactInfo");
        action.setParams({
            "strContactId": ContactInfo.Id
        });
        var opts=[];
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.healthInfoObj",response.getReturnValue());
                component.set("v.Spinner",false); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    onSave : function(component, event, isDraft) {
        var action = component.get("c.saveRecord");
        var healthInfoObj = component.get("v.healthInfoObj");
        var recId = component.get("v.record.item.Id");
        //"objHealthStr": JSON.stringify(healthInfoObj),
        action.setParams({
            "objHealth": healthInfoObj,
            "recId": recId,
            "isDraft": isDraft
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.Spinner",false);
                component.set("v.parentToggle",false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    onValidateForm : function(component, event) {
        var isValid = false;
        var healthInfoObj = component.get("v.healthInfoObj");
        var configWrapper = component.get("v.configWrapper");
        var messageErr = "";
        if(configWrapper.YourHealthInfo_Fld_Allergies__c == 'Required' && healthInfoObj.Allergies__c === undefined){
            messageErr += "Please Fill:" + $A.get("$Label.c.AfsLbl_YourHealthInformation_DoYouHaveAllergies") +"\n";
        }
        
        if(healthInfoObj.Allergies__c == 'Yes'){
            if(configWrapper.YourHealthInfo_Fld_Allergies_List__c == 'Required' && healthInfoObj.Allergies_List__c === undefined || healthInfoObj.Allergies_List__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Whatareyouallergicto") +"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_Allergies_Diagnosis__c == 'Required' && healthInfoObj.Allergies_Diagnosis__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFillAllergies") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DiagnosisDate") +"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_Allergies_Severity__c == 'Required' && (healthInfoObj.Allergies_Severity__c === undefined || healthInfoObj.Allergies_Severity__c == '')){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Howdoyoudescribetheseverityofyourallergies") +"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_Allergies_Seasonality__c == 'Required' && (healthInfoObj.Allergies_Seasonality__c === undefined || healthInfoObj.Allergies_Seasonality__c == '')){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Whendoyoutypicallyexperienceyourallergies")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_Allergies_Symtomps__c == 'Required' && healthInfoObj.Allergies_Symtomps__c === undefined || healthInfoObj.Allergies_Symtomps__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Whatsymptomsdoyouexperience")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_Allergies_Triggers__c == 'Required' && healthInfoObj.Allergies_Triggers__c === undefined || healthInfoObj.Allergies_Triggers__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Whattriggersyourallergyies")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllergiesMedications__c == 'Required' && healthInfoObj.Allergies_medications__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Doyouevertakemedicationforyourallergies")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllergiesMedDetails__c == 'Required' && healthInfoObj.YourHealthInfo_Fld_AllergiesMedications__c == 'Yes' && healthInfoObj.Allergies_medication_details__c === undefined || healthInfoObj.Allergies_medication_details__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_whatisthenameofthemedicationwhatisthedosageandhowof")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllergiesEpiPen__c == 'Required' && healthInfoObj.Allergies_EpiPen__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DoyouhaveallergieswhichrequireyoutocarryanEpiPen")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllergiesEpiPenUse__c == 'Required' && healthInfoObj.Allergies_EpiPen__c == 'Yes' && healthInfoObj.Allergies_EpiPen_Use__c === undefined || healthInfoObj.Allergies_EpiPen_Use__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Howoftendoyouusetheeippen")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllergiesAffects__c == 'Required' && healthInfoObj.Allergies_Affects__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Doallergiessignificantlyaffectyourdailylifeorlimity")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllergiesAffectsHow__c == 'Required' && healthInfoObj.Allergies_Affects__c == 'Yes' && healthInfoObj.Allergies_How_Affect__c === undefined || healthInfoObj.Allergies_How_Affect__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_Howisyourlifeaffectedoractivitieslimited")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllerSplConsider__c == 'Required' && healthInfoObj.Allergies_special_considerations__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiers")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllerSplConsiderDes__c == 'Required' && healthInfoObj.Allergies_special_considerations__c == 'Yes' && healthInfoObj.Allergic_special_consideration_detail__c === undefined || healthInfoObj.Allergic_special_consideration_detail__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiersDescribe")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllergiesPets__c == 'Required' && healthInfoObj.Allergies_Pets__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AlergiesPets")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllerPetFreeRoom__c == 'Required' && healthInfoObj.Allergies_Pet_free_room__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AlergiesPetsFreeRoom")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_AllerHairHypoPet__c == 'Required' && healthInfoObj.Allergic_hairless_or_hypoallergenic_Pets__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AlergiesPetsHairFree")+"\n";
            }
        }
            
        if(configWrapper.YourHealthInfo_Fld_Asthma__c == 'Required' && healthInfoObj.Asthma__c === undefined){
            messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DoYouHaveAsthma")+"\n";
        }
        
        if(healthInfoObj.Asthma__c == 'Yes'){
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaSeverity__c == 'Required' && (healthInfoObj.Asthma_Severity__c === undefined || healthInfoObj.Asthma_Severity__c == '')){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaSeverity")+"\n";
           } 
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaDiagnosis__c == 'Required' && healthInfoObj.Asthma_Diagnosis__c === undefined){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFillAsthma") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DiagnosisDate")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaSymptoms__c == 'Required' && (healthInfoObj.Asthma_Symptoms__c === undefined || healthInfoObj.Asthma_Symptoms__c == '')){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaSymptoms")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaTriggers__c == 'Required' && healthInfoObj.Asthma_Triggers__c === undefined){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaTrigger")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaMedications__c == 'Required' && healthInfoObj.Asthma_Medications__c === undefined){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaMedication")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaMedDetails__c == 'Required' && healthInfoObj.Asthma_Medications__c == 'Yes' && healthInfoObj.Asthma_Med_details__c === undefined || healthInfoObj.Asthma_Med_details__c === ""){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaMedicationDescribe")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaAffectDaily__c == 'Required' && healthInfoObj.Asthma_Affect_Daily_Life__c === undefined){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaDailyLifeAffect")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AsthmaAffectHow__c == 'Required' && healthInfoObj.Asthma_Affect_Daily_Life__c == 'Yes' && healthInfoObj.Asthma_Affect_How__c === undefined || healthInfoObj.Asthma_Affect_How__c === ""){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaDailyLifeAffectHow")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AstmaSplConsider__c == 'Required' && healthInfoObj.Asthma_Special_Considerations__c === undefined){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaSpecialConsideration")+"\n";
           }
            
           if(configWrapper.YourHealthInfo_Fld_AstmaConsiderDetail__c == 'Required' && healthInfoObj.Asthma_Special_Considerations__c == 'Yes' && healthInfoObj.Asthma_Considerations_Details__c === undefined || healthInfoObj.Asthma_Considerations_Details__c === ""){
               messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_AsthmaSpecialConsiderationDesc")+"\n";
           }
        }
        
        if(configWrapper.YourHealthInfo_Fld_CeliacDisease__c == 'Required' && healthInfoObj.Celiac_Disease__c === undefined){
            messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DoyouCeliacdisease")+"\n";
        }
        
        if(healthInfoObj.Celiac_Disease__c == 'Yes'){
            if(configWrapper.YourHealthInfo_Fld_CeliacAffectDaily__c == 'Required' && healthInfoObj.Celiac_Affect_daily_Life__c === undefined || healthInfoObj.Celiac_Affect_daily_Life__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacDailyAffect")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacDiagnosis__c == 'Required' && healthInfoObj.Celiac_Diagnosis__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFillCeliac") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DiagnosisDate")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacSymptoms__c == 'Required' && healthInfoObj.Celiac_Symptoms__c === undefined || healthInfoObj.Celiac_Symptoms__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacSymptoms")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacReactions__c == 'Required' && healthInfoObj.Celiac_Reaction__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacReactions")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacHospitalization__c == 'Required' && healthInfoObj.Celiac_Hospitalization__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacHospitalization")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacHospitalHistory__c == 'Required' && healthInfoObj.Celiac_Hospitalization_History__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacHospitalizationHistory")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacConsumeSamePot__c == 'Required' && healthInfoObj.Celiac_Consume_Food_From_Same_Pot__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacFoodSamePot")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacConsumeSamePlat__c == 'Required' && healthInfoObj.Celiac_Consume_Food_From_Same_Plate_Ut__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacFoodSamePlate")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacLimitPhysActiv__c == 'Required' && healthInfoObj.Celiac_Limit_Physical_Activities__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacLimitPhysicalActivity")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacVisitDoc__c == 'Required' && healthInfoObj.Celiac_Visit_Doctor__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacVisitDoctor")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacMedications__c == 'Required' && healthInfoObj.Celiac_Medications__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacMedications")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacMedDetails__c == 'Required' && healthInfoObj.Celiac_Medications__c == 'Yes' && healthInfoObj.Celiac_Med_Details__c === undefined || healthInfoObj.Celiac_Med_Details__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacMedicationsDetails")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacMedAdmin__c == 'Required' && healthInfoObj.Celiac_Medications__c == 'Yes' && healthInfoObj.Celiac_Med_Administration__c === undefined || healthInfoObj.Celiac_Med_Administration__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacMedicationsAdmin")+"\n";
            }
            
            if(configWrapper.YourHealthInfo_Fld_CeliacDietManage__c == 'Required' && healthInfoObj.Celiac_Diet_Management__c === undefined || healthInfoObj.Celiac_Diet_Management__c === ""){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_CeliacDietManagement")+"\n";
            }
        }
        
        if(configWrapper.YourHealthInfo_Fld_Diabetes__c == 'Required' && healthInfoObj.Diabetes__c === undefined){
            messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DoyouDiabetes")+"\n";
        }
        
        if(healthInfoObj.Diabetes__c == 'Yes'){
            if(configWrapper.YourHealthInfo_Fld_DiabetiesNeedsDoc__c == 'Required' && healthInfoObj.Diabetes_Need_to_visit_doctor__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DiabetiesNeedsDoc")+"\n";
            }
        }
        
        if(configWrapper.YourHealthInfo_Fld_DentalCareOnPrg__c == 'Required' && healthInfoObj.Dental_care_on_program__c === undefined){
           messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DentalCareProgram")+"\n";
        }
        
        if(healthInfoObj.Dental_care_on_program__c == 'Yes'){
            if(configWrapper.YourHealthInfo_Fld_DentalDiagnosis__c == 'Required' && healthInfoObj.Dental_Diagnosis__c === undefined){
                messageErr += $A.get("$Label.c.AfsLbl_YourHealthInformation_PleaseFill") + $A.get("$Label.c.AfsLbl_YourHealthInformation_DiagnosisDate")+"\n";
            }
        }
        
        if(messageErr != ""){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({ 
                type : "error",
                duration : 5000,
                message: messageErr
            });
            toastEvent.fire();
            component.set("v.Spinner",false);
        }else{
            isValid = true;
        }
        return isValid;
    }
})