({
	doInit: function (component, event, helper) {
		const schoolApp = component.get("v.SchoolAppLoaded");
        const school = component.get("v.School");
        //this should only run when the component initiate, so this restriction won't run after that
        if(!school.Name){
            helper.getSchoolInfo(component, schoolApp);
        }
		
	},
    handleChange: function(component, event, helper) {
        component.set("v.zoomLevel", 0);
        const inputText = event.getParam("value");
        if (inputText.length > 3) {
            helper.getPossibleLocations(component, inputText, "");
        }
        if (inputText.length === 0) {
            component.set("v.predictions", []);
        }


        helper.startTimeOut(component, inputText);
    },
    
    
    handleChangeManualSchoolName: function (component, event, helper){
		component.set("v.schoolNameManualInput", event.getParam("value"));

        
    },
    
    selectOpcion: function(component, event, helper) {
    	
        const name = event.currentTarget.dataset.record;
        const locationSelected = event.currentTarget.dataset.localval;
        const placeId = event.currentTarget.dataset.placeid;
        
      
        helper.selectedOption(component, name, placeId)
    }, 
    handleCantFindSchool: function(component, event, helper){
    component.set("v.schoolManualInput", true)
     component.set("v.mapMarkers", []) // to hide the map
     component.set("v.locationSelected", '') // to clear the input 
     component.set("v.predictions", [] ) // to hide the predictions
     component.set("v.showCantFindSchoolBtn" , false) // to hide the cant find school button 
     
     
     
    
    }


})