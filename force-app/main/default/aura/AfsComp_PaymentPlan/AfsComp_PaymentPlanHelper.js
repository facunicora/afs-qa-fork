({
	getPaymentPlan : function (component,helper){
        component.set('v.Spinner',true);
        var actionPaymentInstallment =  component.get("c.getPaymentInstallmentList");
        var actionPaymentEntry =  component.get("c.getPaymentEntryList");
       
        var  applicationObj = component.get("v.applicationWrapper.applicationObj");
      
        actionPaymentInstallment.setParams({
            "aplicationID" : applicationObj.Id
        });
        actionPaymentInstallment.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {   
                var value = response.getReturnValue();   
                component.set('v.paymentInstallmentList',value);
                 $A.enqueueAction(actionPaymentEntry);
            }else{ 
                component.set('v.Spinner',false);
            }
        });
		
        actionPaymentEntry.setParams({
            "aplicationID" : applicationObj.Id
        });
        actionPaymentEntry.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {   
                var value = response.getReturnValue();  
                 component.set('v.paymentEntryList',value);
                component.set('v.Spinner',false);
            }else{
                component.set('v.Spinner',false);
            }
        }); 
        $A.enqueueAction(actionPaymentInstallment);
	},
    updateTask :function (component,helper){
   		 component.set('v.Spinner',true);
        var action =  component.get("c.updateTask");
        var taskID = component.get("v.taskID");
       	action.setParams({
            "taskID" : taskID
        });
    	action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {   
                var value = response.getReturnValue();   
               	component.set('v.parentToggle',false);
                var cmpEvent = component.getEvent("afscompeventPaymentPlan");
        		cmpEvent.setParam("param",value);
                cmpEvent.fire();
                
            }else{ 
                component.set('v.Spinner',false);
            }
        });
        
         $A.enqueueAction(action);
    }
})