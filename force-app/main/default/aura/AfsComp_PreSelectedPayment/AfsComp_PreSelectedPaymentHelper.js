({
	onSaveRecord : function(component, event) {
    	var action = component.get("c.updateParent");
        action.setParams({ recId : component.get("v.record.item.Id") });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.parentToggle",false);        
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action); 
	},
    
    getDynamicCustomLabelVal :  function (labelStr,component){
        if(labelStr != undefined && labelStr != "" ){
            var labelReference = $A.getReference("$Label.c." + labelStr);
        	component.set("v.LabelTempVal", labelReference);
        }else{
            component.set("v.LabelTempVal", "");
        }
    },
    
    onInit : function(component,event){
        var action = component.get("c.getApplicationFee");
        action.setParams({
            "applicationId": component.get("v.applicationWrapper").applicationObj.Id
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.applicationFee",response.getReturnValue());                 
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    }
})