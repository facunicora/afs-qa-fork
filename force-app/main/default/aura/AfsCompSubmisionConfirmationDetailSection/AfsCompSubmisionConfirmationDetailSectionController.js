({
	doInit : function(component, event, helper) {
        component.set("v.Spinner","true"); 
        window.scrollTo(0, 0);
        helper.isAppUnderProcess(component,event);
        var configObj = component.get("v.configWrapper");
        var applicationWrapper = component.get("v.applicationWrapper");
        if(configObj.Scholarship_Available_Stages__c != undefined && configObj.Scholarship_Available_Stages__c != ''){
            if(configObj.Scholarship_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                component.set("v.isScholarshipDisbaled",false);
            }else{
                component.set("v.isScholarshipDisbaled",true);
            }
        }else{
            component.set("v.isScholarshipDisbaled",true);
        }
        
        
        if(configObj.GCC_Available_Stages__c != undefined && configObj.GCC_Available_Stages__c != ''){
            if(configObj.GCC_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                component.set("v.isGCCDisbaled",false);
            }else{
                component.set("v.isGCCDisbaled",true);
            }
        }else{
            component.set("v.isGCCDisbaled",true);
        }
	},
    
    navigateView : function(component, event, helper) {
        console.log('txetx')
		helper.routeConfig(component,event);  
	},
    getInput :function(component, event, helper) {
		 console.log(event.getsource());
	},
    linkMoveTo : function(component, event, helper) {
    	helper.routeConfig(component,event);  
        window.scrollTo(0, 0);
    },
   openLinkFaceBook :function(component, event, helper) {
      	var configObj = component.get("v.configWrapper"); 
        var shareurl  = configObj.Submission_Url_Facebook_Share__c;
        if(shareurl != undefined ){
				window.open(shareurl,'AFS Community','height=320', 'width=300');            
        }else{
            console.log('no url');
        }
   		
    },
    openTwitterLink :function(component,event,helper){
        debugger;
        var configObj = component.get("v.configWrapper"); 
        var shareurl  = configObj.Submission_Url_Twitter_Share__c;
        console.log(shareurl);  
        if(shareurl != undefined ){
        	window.open(shareurl, 'twitter-popup', 'height=350,width=600');
        }
    },
     handleEventNavigation :function(component, event, helper) {
   			var navigateTo = event.getParam("navigateTo");
            //var view ="2"
       		component.set("v.detailNavigationWrapper", navigateTo);
         	window.scrollTo(0, 0);

    }
    
})