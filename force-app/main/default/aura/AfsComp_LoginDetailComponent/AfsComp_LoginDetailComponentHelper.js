({
	getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    doGetLanguageList : function(component, event) {
		var action = component.get("c.getLanguageList");
        var configObj = component.get("v.configWrapper");
       	
        action.setParams({
            "selectedLanguageCode" : configObj.Afs_CommunityLanguages__c
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
            	if(response.getReturnValue() != undefined && response.getReturnValue() != [] && response.getReturnValue().length == 1){
            		component.set("v.isLanguagesAvailable", false);
                }else{
                    component.set("v.isLanguagesAvailable", true);
                }
                // if only one language is selected  in
                // configuration object assign that language as default 
                // without giving user option to select language
                if(response.getReturnValue().length == 1){
                   var wrapperObj = component.get("v.loginWrapper"); 
                   wrapperObj.languageSelected = response.getReturnValue()[0];
                }
                component.set("v.languageList",response.getReturnValue());
                component.set("v.Spinner",false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                component.set("v.Spinner",false);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    createUserContact : function(component, event) {
		var action = component.get("c.generateSession");
        var wrapperObj = component.get("v.loginWrapper");
       	var sPageURL = window.location.href.split('?'); //You get the whole decoded URL of the page.
        var sParameterName = "";
        if(sPageURL.length > 1 && (sPageURL[1] != undefined || sPageURL[1] != "")){
           	var sURLVariables = sPageURL[1].split('&'); //Split by & so that you get the key value pairs separately in a list
            
            for (var i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                
                if (sParameterName[0] === 'prId') { //lets say you are looking for param name - firstName
                    sParameterName[1] === undefined ? '' : sParameterName[1];
                }
            }
        }
        
        action.setParams({
            "wrapObj" : JSON.stringify(wrapperObj),
            "prId" : sParameterName[1]
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                //TODO
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                component.set("v.Spinner",false);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	}
})