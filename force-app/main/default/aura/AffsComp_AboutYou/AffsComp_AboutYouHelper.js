({
	getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    doSubmitApplication : function (component,wrapperObj){
        var action = component.get("c.doSubmittApplication");
        action.setParams({
            "appObj": wrapperObj.applicationObj,   
            "fromWhere":'AboutYou', 
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var wrapperObjNew = component.get("v.applicationWrapper");
                wrapperObjNew.applicationObj.Status__c = 'Participation Desire';
                component.set("v.applicationWrapper",wrapperObjNew);
                var navigateWrapper = component.get("v.navigateWrapper");
                navigateWrapper.isProgramDetail = false;
                navigateWrapper.isProgramDetailSideBar = true;
                navigateWrapper.isAboutYou = false;
                navigateWrapper.isAboutYouSideBar = true;
                navigateWrapper.isSelectProgram = false;
                navigateWrapper.isSelectProgramSideBar = true;
               	navigateWrapper.isPortalSubmissionConfirmation = true;
                navigateWrapper.isPortalSubmissionConfirmationSideBar = true;
                if(response.getReturnValue() == 'Processed'){
                    navigateWrapper.isPortalSubmissionConfirmation =false;
                    navigateWrapper.isPreSelected = true;
                    navigateWrapper.isPreSelectedSideBar = true;
                }
                component.set("v.navigateWrapper",navigateWrapper);
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false");
            }
        });
        $A.enqueueAction(action);
    },
    
    validatedParentComp : function (component ,event){
        
        var messageTemplateStr ='';
        var parentList = component.get('v.ParentGuardianWrapper');
        var emergencyContact =  false;
        var parentName =''
        for(var  i= 0 ;i < parentList.length ; i++ ){
            if(parentList[i].relation.This_is_your_emergency_contact__c == true){
                emergencyContact = true; 
            }
            
            if(parentList[i].parentSelected  == ''){
                var num =i+1;
                parentName = 'Parent detail no ' + num ;
            }else{
                if(parentList[i].parentSelected != 'Other' ){
                    parentName = 'Your '+ parentList[i].parentSelected + '\'s';
                }else{
                    parentName = 'Your '+ parentList[i].relation.npe4__Type__c + '\'s';
                }
                
            }
              
            if($A.util.isEmpty(parentList[i].parentSelected)){
                messageTemplateStr += parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianRequire") +"\n";
            }else{
                if(parentList[i].parentSelected == 'Other' && $A.util.isEmpty(parentList[i].relation.npe4__Type__c)){
                    messageTemplateStr += parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianRequire") +"\n";
                }
            }
            if($A.util.isEmpty(parentList[i].relatedContact.FirstName)){
                messageTemplateStr +=    parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianName") +"\n"; 
            }
            if($A.util.isEmpty(parentList[i].relatedContact.LastName)){
                messageTemplateStr +=   parentName +' '+  $A.get("$Label.c.AfsLbl_Error_ParentGuardianLastName") +"\n"; 
            }
            //---------------------------------------------------------------------------
            if(!$A.util.isEmpty(parentList[i].relatedContact.Phone)){
                if(parentList[i].relatedContact.Phone === "invalid"){
                    messageTemplateStr +=   parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianPhoneNoValid") +"\n"; 
                }	
            }else if(component.get("v.configWrapper.Submission_Fld_ParentGuardian_Phone__c") =='Required'){
                messageTemplateStr +=   parentName +' '+  $A.get("$Label.c.AfsLbl_Error_PhoneRequire") +"\n"; 
            }
            
            if(parentList[i].relatedContact.Phone === 'invalid') {
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
                }
            }
            
            if(!$A.util.isEmpty(parentList[i].relatedContact.Email)){
                var validEmail = this.validateEmail(parentList[i].relatedContact.Email);
                if(validEmail != true){
                    messageTemplateStr +=  parentName +' '+  $A.get("$Label.c.AfsLbl_Error_ParentGuardianEmailNoValid") +"\n"; 
                }	
            }else if(component.get("v.configWrapper.Submission_Fld_ParentGuardian_Email__c") =='Required'){
                messageTemplateStr +=  parentName +' '+   $A.get("$Label.c.AfsLbl_Error_ParentGuardianEmailRequired") +"\n"; 
            } 
            
        }
        if(component.get("v.configWrapper.Submission_Fld_Parent_Emergency_contact__c") == 'Required' && emergencyContact == false){
            messageTemplateStr +=  $A.get("$Label.c.AfsLbl_Error_This_is_your_emergency_contact") +"\n"; 
        }
        
        return messageTemplateStr;
        
    },
    
    validateEmail : function (email) {
    	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(String(email).toLowerCase());
    },
    
    validatePhone : function (phone) {
    	var re = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    	return re.test(String(phone));
    }
    
    
})