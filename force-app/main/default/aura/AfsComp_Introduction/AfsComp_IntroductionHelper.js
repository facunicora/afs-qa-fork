({
    getAttachmentList : function(component) {
       var configObj =component.get("v.configWrapper");
      
       var action = component.get("c.getAttachment");
       var Id =  component.get('v.applicationWrapper.applicationObj.Id');
        action.setParams({
            "Id" : Id
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
              	component.set("v.Files",response.getReturnValue());
               
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }
        });
        $A.enqueueAction(action);
        
    },
    validate :  function(component){
        var configObj = component.get("v.configWrapper");
        var applicationObj = component.get("v.applicationWrapper.applicationObj");
        var errorMessage ='';
        if(configObj.Introduction_Fid_FiveThings__c == 'Required' && $A.util.isEmpty(applicationObj.Things_to_share__c)){
            errorMessage += $A.get("$Label.c.AfsLbl_Introduction_FiveThings_Error") + '\n';
        }
        if(configObj.Introduction_Fid_ThingsCompleteYourDay__c == 'Required' && $A.util.isEmpty(applicationObj.Describe_a_normal_day_in_your_life__c)){
            errorMessage += $A.get("$Label.c.AfsLbl_Introduction_ThingsComplete_Error")+ '\n';
        }
        if(configObj.Introduction_Fid_ExcitingAboutAFS__c == 'Required' && $A.util.isEmpty(applicationObj.What_you_bring_to_your_AFS_experience__c)){
            errorMessage += $A.get("$Label.c.AfsLbl_Introduction_ExcitingAFS_Error")+ '\n';
        }
        if(configObj.Introduction_Fid_FoodYouLike__c == 'Required' && $A.util.isEmpty(applicationObj.Food_thoughts__c)){
            errorMessage += $A.get("$Label.c.AfsLbl_Introduction_FoodYouLike_Error")+ '\n';
        }
        if(configObj.Introduction_Fid_FavoriteItem__c == 'Required' && $A.util.isEmpty(applicationObj.Favorite_travel_destinations_books__c)){
            errorMessage += $A.get("$Label.c.AfsLbl_Introduction_FavoriteItem_Error")+ '\n';
        }
        debugger;
        if(configObj.Introduction_Fid_Video__c == 'Required' && (($A.util.isEmpty(applicationObj.Video_for_Host_Family__c)) && (component.get("v.Files") == undefined || component.get("v.Files").length <= 0))){
            errorMessage += $A.get("$Label.c.AfsLbl_Introduction_Video_Error")+ '\n';
        }
        return errorMessage;
    },
    
    deleteFile :function(component,event){
        
        var fId = event.currentTarget.getAttribute('data-value');
        //console.log(fId);
        var action = component.get("c.deleteAttachment"); 
        var Id =  component.get('v.applicationWrapper.applicationObj.Id');
        action.setParams({
            "AttchmentId" : fId,
            "Id" : Id
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var lstFiles = [];
                var uploadedFiles = component.get("v.Files");
                for(var i  = 0 ; i< uploadedFiles.length ; i ++){
                    if(uploadedFiles[i].ContentDocumentId != fId){
                        lstFiles.push(uploadedFiles[i]);
                    }
                }
                
                component.set("v.Files",lstFiles);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "File deleted."
                });
                toastEvent.fire();    
                 component.set("v.Spinner", false);
            }else{                
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                 component.set("v.Spinner", false);
            }           
        });
        $A.enqueueAction(action);
    
	},
	saveIntroduction : function(component) {
      
        var errorMessage = this.validate(component);
        if(errorMessage !=''){
             var toastEvent = $A.get("e.force:showToast");
             	toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: errorMessage
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            return;
        }
        
       var action = component.get("c.saveApplicantIntroduction");
       var application =  component.get('v.applicationWrapper.applicationObj');
        
		var toDoObj =  component.get('v.record.item');
        if(toDoObj.Stage_of_portal__c == "1. Stage: Pre-application"){
            application.Status__c = "Decision";
            application.Status_App__c = "Pre-Selected";
        }else if(toDoObj.Stage_of_portal__c == "2. Stage: Pre-selected"){
            application.Status__c = "Confirmation";
            application.Status_App__c = "Pre-Selected";
        }
        
        action.setParams({
            "application" : application,
            "recId" : toDoObj.Id
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                 component.set("v.Spinner","false"); 
                component.set("v.parentToggle",false);
                var applicationWrapper =  component.get('v.applicationWrapper');
                applicationWrapper.applicationObj = application;
                component.set('v.applicationWrapper',applicationWrapper);
               // var cmpEvent = component.getEvent("afscompeventIntroduction");
        		//cmpEvent.setParam("goto","main");
               // cmpEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                 component.set("v.Spinner","false"); 
            }
        });
        $A.enqueueAction(action);
	},
    getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    }
    
})