({
	createCloneHelper : function(component,event, recordName, recordId) {
		var action = component.get("c.newMasterToDoTemplate");
        action.setParams({
            'recordName': recordName,
            'recordId': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert('Se clono con exito!');
                var recId = response.getReturnValue();
               var address = '/'+recId.Id;
               var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": address,
                  "isredirect" :false
                });
               urlEvent.fire();
            }else{
                alert('Something went wrong..');
            }
 
        });
        $A.enqueueAction(action);
	}
})