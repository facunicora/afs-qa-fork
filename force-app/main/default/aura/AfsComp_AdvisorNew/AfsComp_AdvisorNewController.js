({
	doInit : function(component, event, helper) {
        component.set("v.Spinner",true);
		helper.OnInit(component,event);
	},
    
    doInsertFeed : function(component, event, helper) {
        component.set("v.Spinner",true);
		helper.OnInsertFeed(component,event);
	},
    
    doInsertComment : function(component, event, helper) {
        component.set("v.Spinner",true);
        var feedId = event.currentTarget.id;
        var feedWrapperList = component.get("v.feedWrapperList");
		helper.OnInsertComment(component,event,feedWrapperList[event.currentTarget.id].feedObj.Id,feedWrapperList[event.currentTarget.id].commentBody);
	},
    
    toggleComment : function(component, event, helper) {
        var itemIndex = event.currentTarget.id;
        var feedWrapperList = component.get("v.feedWrapperList");
        
        if(feedWrapperList[itemIndex].showCommentSection){
            feedWrapperList[itemIndex].showCommentSection = false;
            component.set("v.feedWrapperList",feedWrapperList);
        }else{
            feedWrapperList[itemIndex].showCommentSection = true;
            component.set("v.feedWrapperList",feedWrapperList);
        }
	},
    
    doLikeFeed : function(component, event, helper) {
        component.set("v.Spinner",true);
        var feedWrapperList = component.get("v.feedWrapperList");
		helper.OnLikeFeed(component,event,feedWrapperList[event.currentTarget.id].feedObj.Id);
	}
})