({
	doInit : function(component, event, helper) {
        component.set("v.Spinner",true);
		helper.onInit(component, event);
        
        var configObj = component.get("v.configWrapper");
        helper.getDynamicCustomLabelVal(configObj.ScholarshipApply_Txt_ElegibilityHelpTxt__c ,component);
        component.set("v.ScholarshipApply_Txt_ElegibilityHelpTxt",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.ScholarshipApply_Txt_DesiredAmountHelpTx__c ,component);
        component.set("v.ScholarshipApply_Txt_DesiredAmountHelpTx",component.get("v.LabelTempVal")); 
	},
    
    selectScholarShip : function(component, event, helper) {
		var ScholarWrapperList = component.get("v.ScholarWrapperList");
        var index = event.currentTarget.id;
        if(ScholarWrapperList[index].isCreated == false){
            ScholarWrapperList[index].isSelected = (ScholarWrapperList[index].isSelected == false ? true : false);
        	component.set("v.ScholarWrapperList",ScholarWrapperList);
        }
	}, 
    
    onSaveData : function(component, event, helper) {
        component.set("v.Spinner",true);
        // Check for validations
        var ScholarWrapperList = component.get("v.ScholarWrapperList");
        var messageTemplateStr;
        var flag = false;
        for(var i = 0; i < ScholarWrapperList.length ; i++){
            if(ScholarWrapperList[i].isSelected != undefined && ScholarWrapperList[i].isSelected != false){
                flag = true;
                if(ScholarWrapperList[i].scholarShipObj.Applicant_Eligibility_Specifications__c === undefined ||
                   ScholarWrapperList[i].scholarShipObj.Applicant_Eligibility_Specifications__c == "" || 
                   ScholarWrapperList[i].scholarShipObj.Applicant_Eligibility_Specifications__c == "Indicate eligibility"){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += ScholarWrapperList[i].scholarShipObj.Name + " : " + $A.get("$Label.c.AfsLbl_Error_EligibilityRequire");
                    }else{
                        messageTemplateStr = ScholarWrapperList[i].scholarShipObj.Name + " : " + $A.get("$Label.c.AfsLbl_Error_EligibilityRequire");
                    }
                }
                
                if(ScholarWrapperList[i].scholarShipObj.Scholarship_Amount_Available__c === undefined ||
                   ScholarWrapperList[i].scholarShipObj.Scholarship_Amount_Available__c == "" ||
                   ScholarWrapperList[i].scholarShipObj.Scholarship_Amount_Available__c == "Indicate amount"){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += ScholarWrapperList[i].scholarShipObj.Name + " : " +  $A.get("$Label.c.AfsLbl_Error_DesiredAmountRequire");
                    }else{
                        messageTemplateStr = ScholarWrapperList[i].scholarShipObj.Name + " : " +  $A.get("$Label.c.AfsLbl_Error_DesiredAmountRequire");
                    }
                }
            }
            
        }
        
		if(messageTemplateStr != undefined){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            component.set("v.Spinner",false);
            return;
        }
        
        if(flag != false){
            helper.doSaveData(component, event);
        }
		
	}
})