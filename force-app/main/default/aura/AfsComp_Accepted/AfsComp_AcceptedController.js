({
    doInit : function (component, event, helper){
        component.set("v.Spinner","true"); 
        helper.getProgramApplication(component,event);
		window.scrollTo(0, 0);
        var configObj = component.get("v.configWrapper");
        var applicationWrapper = component.get("v.applicationWrapper");
        if(configObj.Scholarship_Available_Stages__c != undefined && configObj.Scholarship_Available_Stages__c != ''){
            if(configObj.Scholarship_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                component.set("v.isScholarshipDisbaled",false);
            }else{
                component.set("v.isScholarshipDisbaled",true);
            }
        }else{
            component.set("v.isScholarshipDisbaled",true);
        }
        
        
        if(configObj.GCC_Available_Stages__c != undefined && configObj.GCC_Available_Stages__c != ''){
            if(configObj.GCC_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                component.set("v.isGCCDisbaled",false);
            }else{
                component.set("v.isGCCDisbaled",true);
            }
        }else{
            component.set("v.isGCCDisbaled",true);
        }
        //var configObj = component.get("v.configWrapper");
        
        // Disable Your Profile if
        // Application stage is not in available stages
        if(configObj.Your_Profile_Available_Stages__c != undefined && configObj.Your_Profile_Available_Stages__c != ''){
            if(configObj.Your_Profile_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                component.set("v.isYourProfileDisbaled",false);
            }else{
                component.set("v.isYourProfileDisbaled",true);
            }
        }else{
            component.set("v.isYourProfileDisbaled",true);
        }
        
        
    },
    
    navigateComponent : function(component, event, helper) {
        var idNavigate = event.currentTarget.id;
        var config = component.get("v.configWrapper");
        var applicationWrapper = component.get("v.applicationWrapper");
        if(idNavigate == "@0@"){
            component.set("v.detailNavigationWrapper", '0');
         	window.scrollTo(0, 0);
        }else if(idNavigate == "@1@"){
           if(config.Your_Profile_Available_Stages__c != undefined && config.Your_Profile_Available_Stages__c != ''){
                if(config.Your_Profile_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                    component.set("v.detailNavigationWrapper", '1');
         			window.scrollTo(0, 0);
                }
            }
        }else if(idNavigate == "@2@"){
            if(config.Scholarship_Available_Stages__c != undefined && config.Scholarship_Available_Stages__c != ''){
                if(config.Scholarship_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                    component.set("v.detailNavigationWrapper", '2');
                    window.scrollTo(0, 0);
                }
            }
        }else if(idNavigate == "@3@"){
            if(config.GCC_Available_Stages__c != undefined && config.GCC_Available_Stages__c != ''){
                if(config.GCC_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                    component.set("v.detailNavigationWrapper", '3');
                    window.scrollTo(0, 0);
                }
            }
        }else if(idNavigate == "@4@"){
            component.set("v.detailNavigationWrapper", '4');
         	window.scrollTo(0, 0);
        }
	},
    
    handleEventNavigation :function(component, event, helper) {
   			var navigateTo = event.getParam("navigateTo");
       		component.set("v.detailNavigationWrapper", navigateTo);
         	window.scrollTo(0, 0);
    }
    
    
})