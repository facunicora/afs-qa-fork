({
	getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    getProgramApplication : function (component,event){
        
        var action = component.get("c.getProgramOfferedAccepted");
        action.setParams({
            "prId": component.get("v.applicationWrapper").applicationObj.Program_Offer__c
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                
                component.set("v.programOffrd",response.getReturnValue()); 
                component.set("v.Spinner","false");
                
                var programOffrd = response.getReturnValue();
                var applicationWrapper = component.get("v.applicationWrapper");                
        		var totalCostString = component.get("v.totalCostString");
        
                if(programOffrd.programObj.Total_Cost_numeric__c != 0 && programOffrd.programObj.Total_Cost_numeric__c !== undefined){
                    totalCostString =  applicationWrapper.currencyStr + ' ' + this.formatMoney(programOffrd.programObj.Total_Cost_numeric__c, 0, " ", " ");
                }
                else{
                    totalCostString = "-";
                }
				
                console.log('totalCostString: ' + totalCostString);
        		component.set("v.totalCostString", totalCostString);
                
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
	formatMoney : function(n, c, d, t) {
        console.log('entra function');
		  var c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                j = (j = i.length) > 3 ? j % 3 : 0;

  		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}
})