({
	onInit : function(component, event, helper) {
		 helper.fetchPickListVal(component, 'Country_of_Legal_Residence__c,Passport_Country_of_Issue__c,Dual_citizenship__c,SecondCitizenship__c,Tshirt__c'); 
         window.scrollTo(0, 0);
	},
     changeDualCitizenShip :function(component, event, helper) {
        var item = event.currentTarget.id;  
        if(item == 'No'){
           var SecondaryCitizen =  component.get('v.contactInfo.SecondCitizenship__c');
            if(SecondaryCitizen != undefined){
                 component.set('v.contactInfo.SecondCitizenship__c','');
            }
        }
        component.set('v.contactInfo.Dual_citizenship__c',item);
    },
    saveTravelInfo : function(component, event, helper) {
    	helper.saveTravelDetails(component,helper)
    },
    cancelPage: function(component, event, helper) {
		component.set('v.parentToggle',false);
    },
    handleUploadFinished :function(component, event, helper){
        
        //get file list
    	 component.set("v.Spinner", true);
         var uploadedFiles = event.getParam("files"); 
         var files = component.get("v.Files");
         //var files = [];
         for(var i  = 0 ; i< uploadedFiles.length ; i ++){
            var filObj = new Object();
            filObj.ContentDocumentId = uploadedFiles[i].documentId;
            filObj.Title = uploadedFiles[i].name;
            files.push(filObj);
         }
        if(files.length == 0){
            component.set("v.isFileAdded",false);                
        }else{
            component.set("v.isFileAdded",true);  //file present
        }
        component.set("v.Files",files);
        //helper.getFiles(component, event);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Files Uploaded successfully."
        });
        toastEvent.fire();
        component.set("v.Spinner", false);
    },
    
    deleteFile :function(component, event, helper){
        component.set("v.Spinner","true"); 
    	helper.deleteFile(component, event);
    }
})