({
	fetchPickListVal: function(component, fieldName) {
        debugger;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "objObject": 'Contact',
            "fld": fieldName
        });
    	 var opts=[];
       	 action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {   
                var allValues = response.getReturnValue();
                component.set("v.LegalResidenceList", allValues['Country_of_Legal_Residence__c']);
                component.set("v.CountryOfIssueList", allValues['Passport_Country_of_Issue__c']);
                var  dual = allValues['Dual_citizenship__c'];
                dual.splice(0,1);
                component.set("v.DualCitizenShipPicklist", dual);
                component.set("v.NatinalityPicklist", allValues['SecondCitizenship__c']);
                component.set("v.TshirtList", allValues['Tshirt__c'])
                
            }else{
                
            }
        }); 
	
		var programId =	component.get("v.applicationWrapper.applicationObj.Program_Offer__c");
		var actionGetProgram = component.get("c.getProgramOfferDetails");
        if(programId == undefined){
            programId = null;
        }
        actionGetProgram.setParams({
            "programId" : programId
        });
		actionGetProgram.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {  
               	var result = response.getReturnValue();
                if( result != null ){
                     component.set("v.programOffer", result);  
                }
            }else{
                	var result = response.getReturnValue();
            }
        });
		        
        $A.enqueueAction(action);
        if(programId !=null ){
            $A.enqueueAction(actionGetProgram); 
        }
     },
     isValid : function(component, helper) {
         var contactInfo = component.get("v.contactInfo");
         var messageTemplateStr ='';
         debugger;
     	 if($A.util.isEmpty(contactInfo.Passport_First_Name__c)){
               messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Travel_FirstName") +"\n";
         }
         if($A.util.isEmpty(contactInfo.Passport_Last_Name__c)){
               messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Travel_LastName") +"\n";
         }
         if($A.util.isEmpty(contactInfo.Passport_Number__c)){
               messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Travel_PassPortNumber_Req") +"\n";
         }
          if($A.util.isEmpty(contactInfo.Passport_Expiration_Date__c)){
               messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Expiration_Date_Req") +"\n";
          }else {
              var dt = new Date(contactInfo.Passport_Expiration_Date__c);
              if(dt =='Invalid Date'){
                   messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Expiration_Date_Invalid") +"\n";
              }
          } 
         
         if($A.util.isEmpty(contactInfo.Passport_Country_of_Issue__c) || contactInfo.Passport_Country_of_Issue__c == '-None-'){
          	messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Passport_Country_of_Issue") +"\n";
         }
         if($A.util.isEmpty(contactInfo.Country_of_Legal_Residence__c) || contactInfo.Country_of_Legal_Residence__c == '-None-'){
          	messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Country_of_Legal_Residence") +"\n";
         }
         if($A.util.isEmpty(contactInfo.Dual_citizenship__c)){
          	messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Dual_citizenship") +"\n";
         }else{
             if(contactInfo.Dual_citizenship__c == 'Yes'){
                 if($A.util.isEmpty(contactInfo.SecondCitizenship__c) || contactInfo.SecondCitizenship__c == '-None-'){
                    messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Travel_SecondCitizenship") +"\n";
                 }
             }
         }
         
          if($A.util.isEmpty(contactInfo.Tshirt__c) || contactInfo.Tshirt__c == '-None-'){
           		messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_Travel_Tshirt") +"\n";
          }
         return  messageTemplateStr;
     },
     saveTravelDetails : function(component, helper) {
        
        var cmpEvent = component.getEvent("afscomptravelinfoevent");
        cmpEvent.setParam("param","save");         
       	 var vaidationResult = this.isValid(component ,helper);
         if(vaidationResult !=''){
             var toastEvent = $A.get("e.force:showToast");
             var messageTemplate = vaidationResult;
             toastEvent.setParams({ 
                 type : "Error",
                 duration : 5000,
                 message: messageTemplate
             });
             toastEvent.fire();
             return;
         }
        
   		component.set("v.Spinner","true"); 
        var action =  component.get("c.saveTravelInfomation");
        var  contactInfo = component.get("v.contactInfo");
         if(contactInfo.Dual_citizenship__c == 'No'){
             contactInfo.SecondCitizenship__c =''
         }
        var taskID = component.get("v.taskID");
        
        action.setParams({
            "contactInfo": contactInfo,
            "taskID": taskID,
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {   
                var allValues = response.getReturnValue();
                component.set('v.parentToggle',false);
                cmpEvent.fire();
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError();
                toastEvent.setParams({ 
                    type : "Error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }
        });
        $A.enqueueAction(action);
    },
    
    deleteFile :function(component,event){
        
        var fId = event.currentTarget.getAttribute('data-value');
        //console.log(fId);
        var action = component.get("c.deleteAttachment"); 
        action.setParams({
            "AttchmentId" : fId
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var lstFiles = [];
                var uploadedFiles = component.get("v.Files");
                for(var i  = 0 ; i< uploadedFiles.length ; i ++){
                    if(uploadedFiles[i].ContentDocumentId != fId){
                        lstFiles.push(uploadedFiles[i]);
                    }
                }
                
                component.set("v.Files",lstFiles);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "File deleted."
                });
                toastEvent.fire();    
                 component.set("v.Spinner", false);
            }else{                
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                 component.set("v.Spinner", false);
            }           
        });
        $A.enqueueAction(action);
    
	}
})