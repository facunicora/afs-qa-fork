({
    doInit : function (cmp, event, helper) {
        cmp.set("v.Spinner", "true");
        var applicationWrapper = cmp.get("v.applicationWrapper");
        if(applicationWrapper.applicationObj.Status__c == "Participation Desire"){
             cmp.set("v.photoProgressClass", "c100 p20");
        }else if(applicationWrapper.applicationObj.Status__c == "Decision"){
             cmp.set("v.photoProgressClass", "c100 p30");
        }else if(applicationWrapper.applicationObj.Status__c == "Confirmation"){
             cmp.set("v.photoProgressClass", "c100 p50");
        }else if(applicationWrapper.applicationObj.Status__c == "Purchase"){
             cmp.set("v.photoProgressClass", "c100 p80");
        }else if(applicationWrapper.applicationObj.Status__c == "Onboarding"){
            if(cmp.get("v.photoProgressClass") != undefined && cmp.get("v.photoProgressClass") != ""){
            }else{
                cmp.set("v.photoProgressClass", "c100 p90");
            }
        }else if(applicationWrapper.applicationObj.Status__c == "Participation"){
             cmp.set("v.photoProgressClass", "c100 p100");
        }
        helper.onInit(cmp,event);
    },
    
    handleUploadFinished : function (cmp, event, helper) {
        cmp.set("v.Spinner", "true");
        cmp.set("v.isOpen","false");
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        cmp.set("v.CDLId", uploadedFiles[0].documentId);
        helper.getBase64String(cmp, event,uploadedFiles[0].documentId);
    },
    
    closeModel: function(cmp, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        cmp.set("v.isOpen", "false");
    },
    
    openUploadModal: function(cmp, event, helper) {
        var showDP = cmp.get("v.showDP");
        var isDisabled = cmp.get("v.isDisabled");
        if((showDP == "false" || showDP == false) && isDisabled == false){
            // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        	cmp.set("v.isOpen", "true");
        }else if(isDisabled == true){
            cmp.set("v.detailNavigationWrapper", "1");
        }
        
    },
    
    changeProfilePhoto: function(cmp, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle" 
         
        var CDLId = cmp.get("v.CDLId");
        if(CDLId != undefined && CDLId != ""){
            cmp.set("v.Spinner", "true");
            helper.removeProfilePhoto(cmp, event);
        }
        
    },
    
    changeProgress : function(cmp, event, helper) {
        var applicationWrapper = cmp.get("v.applicationWrapper");
        if(applicationWrapper.applicationObj.Status__c == "Participation Desire"){
             cmp.set("v.photoProgressClass", "c100 p20");
        }else if(applicationWrapper.applicationObj.Status__c == "Decision"){
             cmp.set("v.photoProgressClass", "c100 p30");
        }else if(applicationWrapper.applicationObj.Status__c == "Confirmation"){
             cmp.set("v.photoProgressClass", "c100 p50");
        }else if(applicationWrapper.applicationObj.Status__c == "Purchase"){
             cmp.set("v.photoProgressClass", "c100 p80");
        }else if(applicationWrapper.applicationObj.Status__c == "Onboarding"){
            if(cmp.get("v.photoProgressClass") != undefined && cmp.get("v.photoProgressClass") != ""){
            }else{
                cmp.set("v.photoProgressClass", "c100 p90");
            }
        }else if(applicationWrapper.applicationObj.Status__c == "Participation"){
             cmp.set("v.photoProgressClass", "c100 p100");
        }
        
    }
    
    
})