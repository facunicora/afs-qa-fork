({
	doInit : function(component, event, helper) {
        helper.onInit(component, event);
	},

    showDetail : function(component, event, helper) {
        helper.getDetails(component,event);
	},
    
    toggleMenu : function(component, event, helper) {
        var flag = component.get("v.toggleMenuIcon");
        if(flag){
            component.set("v.toggleMenuIcon",false);
            component.set("v.toggleMenuClass",true); 
        }else{
            component.set("v.toggleMenuIcon",true);
            component.set("v.toggleMenuClass",false);
        }
	},
	
	
	checkAgreeTerms : function(component, event, helper) {
		var AgreeSocialTerms = component.get("v.AgreeSocialTerms");
        if(AgreeSocialTerms){
            AgreeSocialTerms = false;
        }else{
            AgreeSocialTerms = true;
        }
        component.set("v.AgreeSocialTerms",AgreeSocialTerms);
	},
	
	signUpCommunityFromFBButton  : function(component, event, helper) {
		
        helper.navigateToSocial(component, component.get("v.authFBURL"));
	},
	
	signUpCommunityFromGmailButton  : function(component, event, helper) {
		
        helper.navigateToSocial(component, component.get("v.authGoogleURL"));
	}
	
})