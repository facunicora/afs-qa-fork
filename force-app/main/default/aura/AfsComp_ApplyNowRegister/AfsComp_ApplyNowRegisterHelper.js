({
	onInit : function(component, event) {
		component.set("v.Spinner","true");
		var action = component.get("c.getConfiguration");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.configWrapper",response.getReturnValue());
                component.set("v.Spinner","false");
                var configObj = response.getReturnValue();
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_SignUp__c,component);
                component.set("v.SignUpWith",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_SignUpHelpTxt__c,component);
                component.set("v.SignUpWithHelpTxt",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Btn_Facebook__c,component);
                component.set("v.Facebook",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Btn_Gmail__c,component);
                component.set("v.Gmail",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Btn_SignUpWithGmail__c ,component);
                component.set("v.SignUpWithEmail",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Or__c,component);
                component.set("v.Or",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_AlreadyApplied__c,component);
                component.set("v.AlreadyApplied",component.get("v.LabelTempVal"));
                this.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Login__c,component);
                component.set("v.LogIn",component.get("v.LabelTempVal"));
                
                //prepare authURLs
                var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
                var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
                var sParameterName;
                for (var i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('='); //to split the key from the value.
        
                    if (sParameterName[0] === 'prId') { //lets say you are looking for param name - firstName
                        sParameterName[1] === undefined ? '' : sParameterName[1];
                    }
                }
                var comHomeURL = component.get("v.configWrapper.Community_URL__c");
                var startURL = '/s/?prId=' + sParameterName[1] + '&';
                var authFBURL = comHomeURL + '/services/auth/sso/Facebook?community=' + comHomeURL + '&startURL=' + startURL + 'from=facebook';
                var authGoogleURL = comHomeURL + '/services/auth/sso/Google?community=' + comHomeURL + '&startURL=' + startURL + 'from=google';
                
                //console.log(authFBURL);
                //console.log(authGoogleURL);
                
                component.set("v.authFBURL", authFBURL);
                component.set("v.authGoogleURL",authGoogleURL);
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getDetails :  function (component,event){
        var action = component.get("c.getLoginWrapper");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.loginWrapper",response.getReturnValue());
                component.set("v.showDetailSection", "true");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
        
    },
    
    getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    navigateToSocial:  function (component, url){

    	var showDetailSection = component.get("v.showDetailSection");
    	if(showDetailSection == false){
    		var AgreeSocialTerms = component.get("v.AgreeSocialTerms");
    		
    		if(AgreeSocialTerms == false){
				var messageTemplate = $A.get("$Label.c.AfsLbl_Login_ErrMsgCheckbox");
				
				var toastEvent = $A.get("e.force:showToast");
	            
	            toastEvent.setParams({ 
	                type : "Error",
	                duration : 5000,
	                message: messageTemplate
	            });
	            
	            toastEvent.fire();
	            component.set("v.Spinner",false);
	            return;
	        }
	        
	        //window.open(url,'_top')
	        window.location.href = url;
    	}
        
    }
})