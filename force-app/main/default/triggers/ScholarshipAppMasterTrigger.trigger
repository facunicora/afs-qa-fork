/**
 * Created by jotategui on 10/05/2018.
 */

trigger ScholarshipAppMasterTrigger on Scholarship_Application__c (before insert, before update, before delete, after insert, after update, after delete) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){
            system.debug('Trigger.isInsert.ScholarshipAppTriggerHandler.whenSchAppIsCreateSumPreAppfee');
            //ScholarshipAppTriggerHandler.createToDos(Trigger.newMap);
            ScholarshipAppTriggerHandler.createToDosIntoAppWhenSchAppChangedToApplying(Trigger.new,Trigger.oldMap,Trigger.isUpdate);
            ScholarshipAppTriggerHandler.canceledToDosAppWhenSchAppChangedToLost(Trigger.new,Trigger.oldMap,Trigger.isUpdate);
            ScholarshipAppTriggerHandler.whenSchAppIsCreateSumPreAppfee(Trigger.new);
        }else if(Trigger.isUpdate){
            /*Map<id,Scholarship_Application__c> AppsUpdates = new Map<id,Scholarship_Application__c>();
            for(Scholarship_Application__c PO : Trigger.new){
                if(trigger.newMap.get(PO.id).Scholarship__c != null &&
                        (trigger.newMap.get(PO.id).Scholarship__c != trigger.oldMap.get(PO.id).Scholarship__c) ){
                    AppsUpdates.put(PO.id, PO);
                }
            }
            if(AppsUpdates.size() > 0){
                ScholarshipAppTriggerHandler.createToDos(AppsUpdates);
            }*/
            ScholarshipAppTriggerHandler.createToDosIntoAppWhenSchAppChangedToApplying(Trigger.new,Trigger.oldMap,Trigger.isUpdate);
            ScholarshipAppTriggerHandler.canceledToDosAppWhenSchAppChangedToLost(Trigger.new,Trigger.oldMap,Trigger.isUpdate);
            ScholarshipAppTriggerHandler.whenSchAppIsUpdateSumPreAppfee(Trigger.new,Trigger.oldMap);
        }else if(Trigger.isDelete){
            system.debug('Trigger.isDelete.ScholarshipAppTriggerHandler.whenSchAppIsDeleteSumPreAppfee');
            ScholarshipAppTriggerHandler.whenSchAppIsDeleteSumPreAppfee(Trigger.old);
        }

    }


}