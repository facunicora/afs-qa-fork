trigger Afs_ContentDocumentLink on ContentDocumentLink (before insert) {
    if(trigger.isBefore && trigger.isInsert){
        Afs_ContentDocumentLinkTriggerHelper.beforeInsert(Trigger.new);
    }
}