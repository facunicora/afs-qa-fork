trigger Afs_Opportunity_trigger on Opportunity (before insert,before update,before delete) {
   Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
    if(!afsTriggerSwitch.By_Pass_Opportunity_Trigger__c){
        Afs_Opportunity_Trigger_Helper.opportunityNewList = trigger.new;
        Afs_Opportunity_Trigger_Helper.opportunityOldList = trigger.old;
        Afs_Opportunity_Trigger_Helper.opportunityNewMap = trigger.newMap;
        Afs_Opportunity_Trigger_Helper.opportunityOldMap = trigger.oldMap;
        if(!Afs_Opportunity_Trigger_Helper.allowOppUpdate){
            if(trigger.isbefore){
                if( trigger.isInsert || trigger.isUpdate){
                    Afs_Opportunity_Trigger_Helper.allowOpportunityUpdate();
                }else if(trigger.isDelete){
                      Afs_Opportunity_Trigger_Helper.allowOpportunityDelete();
                } 
            }
          
    	}else{
            Afs_UtilityApex.logger('Trigger is already running');
        }
    }else{
         Afs_UtilityApex.logger('B=Trigger is by passesd by custom setting');
   	}
    
}