declare module "@salesforce/apex/Afs_PaymentPlanCtrl.getPaymentInstallmentList" {
  export default function getPaymentInstallmentList(param: {aplicationID: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PaymentPlanCtrl.getPaymentEntryList" {
  export default function getPaymentEntryList(param: {aplicationID: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PaymentPlanCtrl.updateTask" {
  export default function updateTask(param: {taskID: any}): Promise<any>;
}
