declare module "@salesforce/apex/Afs_ParentComponentController.getConfiguration" {
  export default function getConfiguration(): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.getLoginWrapper" {
  export default function getLoginWrapper(): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.updateUserLanguage" {
  export default function updateUserLanguage(param: {selectedLanguage: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.getLanguageList" {
  export default function getLanguageList(param: {selectedLanguageCode: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.userLogin" {
  export default function userLogin(param: {userName: any, password: any, prId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.getNewLoginWrapper" {
  export default function getNewLoginWrapper(): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.updateUserAndContact" {
  export default function updateUserAndContact(param: {conObj: any, languageSelected: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.generateSession" {
  export default function generateSession(param: {wrapObj: any, prId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.getNavigateWrapper" {
  export default function getNavigateWrapper(): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.resetPassword" {
  export default function resetPassword(param: {userNameStr: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.getContactRecord" {
  export default function getContactRecord(): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.getApplicationRecord" {
  export default function getApplicationRecord(): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentComponentController.getDP" {
  export default function getDP(): Promise<any>;
}
