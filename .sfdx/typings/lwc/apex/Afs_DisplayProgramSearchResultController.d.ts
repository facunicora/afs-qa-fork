declare module "@salesforce/apex/Afs_DisplayProgramSearchResultController.getDestinations" {
  export default function getDestinations(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DisplayProgramSearchResultController.getCurrentUserName" {
  export default function getCurrentUserName(): Promise<any>;
}
declare module "@salesforce/apex/Afs_DisplayProgramSearchResultController.getProgramWrapper" {
  export default function getProgramWrapper(param: {mapStringyfied: any, preSelectedProgram: any, isSearched: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DisplayProgramSearchResultController.saveAndContinueProgram" {
  export default function saveAndContinueProgram(param: {offerIdsToDeleteInterest: any, offerIdsToSave: any, additionalVal: any}): Promise<any>;
}
